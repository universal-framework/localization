<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 16.5.2018 г.
 * Time: 21:19 ч.
 */

use Localization\Entity\Template;
use Localization\Utils\PDOWrapper;
use Localization\Utils\TemplateRenderer;
use Localization\Utils\Logger;
use Localization\Entity\Field;
use Localization\Utils\Error;
use Localization\Utils\Token;
use Localization\Template\Field2Template;

if ($_GET['update'] >= 0) {
    $updateID = intval($_GET['update']);
} else {
    header('Location: templates.php?list');
}

$PDO = PDOWrapper::getInstance();
$logger = Logger::getInstance();
$error = Error::getInstance();
$success = false;
$successMessage = "";

$errors = null;

if (empty($_SESSION['field_form_token'])) {
    $_SESSION['field_form_token'] = Token::generateToken();
}

$tempInfo = null;

$token = $_SESSION['field_form_token'];


if (isset($_POST) && count($_POST) > 0) {
    $isUpdated = updateTemplate($updateID, $_POST, $error, $_SESSION['field_form_token'], $PDO, $logger);

    if ($isUpdated === true) {
        $success = true;
        $successMessage = "Sucessfully updated the template";
    } else {
        if ($error->isErrorSome()) {
            $errors = $error->fetchAll();
        } else {
            $errors = "Unknow error occured. Please contact the admin.";
        }

        $_SESSION['field_form_token'] = Token::generateToken();
    }
}

function updateTemplate($TID, $data, Error $errorObj, $token, \PDO $PDO, Logger $logger)
{
    if (empty($data['CSRF_TOKEN'])) {
        $errorObj->add("Could not save the form. Please try again!");
        $errorObj->alert("CSRF token is empty");
        return false;
    }

    if (false === hash_equals($token, $data['CSRF_TOKEN'])) {
        $errorObj->add("Could not save the form. Please try again!");
        $errorObj->alert("CSRF token does not match as expected!");
        return false;
    }

    $label = trim($data['label']);
    $desc = trim($data['desc']);
    $fields = isset($data['fields'])? $data['fields'] : [];
    $tType = trim($data['tempType']);

    try {
        $template = new Template(PDOWrapper::getInstance());
        $updated = $template->update($TID, ['name' => $label, 'desc' => $desc, 'tType' => $tType]);

        if ($updated) {

            $f2Template = new Field2Template($PDO);

            $f2Template->delete($TID);

            if (is_array($fields) && count($fields) > 0) {

                $field = new Field($PDO);

                foreach ($fields as $FID) {
                    $field->load($FID);
                    try {
                        $f2Template->save($field, $template);
                    } catch (\Exception $e){
                        $logger->alert($e->getMessage());
                        return false;
                    }
                }
            }
        }

        return $updated;
    } catch (\Exception $e){
        $errorObj->add($e->getMessage());
        return false;
    }
}

try {
    $field = new Field($PDO);
    $fields = $field->getAll();

    $template = new Template($PDO);
    if ($template->load($updateID)) {
        $tempInfo = $template->getData();
        $tempInfo['fields'] = [];

        // Get the fields associated with the template

        $field2Template = new Field2Template($PDO);
        $fieldIDS = $field2Template->load($tempInfo['id']);
        if (count ($fieldIDS) > 0) {
            foreach ($fieldIDS as $fieldID) {
                if ($field->load($fieldID)) {
                    $tempFieldData = $field->getData();
                    $tempInfo['fields'][] = $tempFieldData['id'];
                } else {
                    $logger->warning(sprintf("Field with ID %d not found in the database", $fieldID));
                }
            }
        }

    } else {
        $error->add(sprintf('Could not find a template with ID %d', $updateID));
    }

} catch (\Exception $e){
    $logger->alert($e->getMessage());
    $error->add(Error::$standardMessage);
}

if ($error->isErrorSome()) {
    $errors = $error->fetchAll();
}

try {
    $templateRenderer = TemplateRenderer::getInstance();
    $templateRenderer->render('header', ['title' => sprintf('Update template %s', $tempInfo['label']),
        'includeCSS' => [URL_VENDORS.'select2/css/select2.min.css'], 'errors' => $errors, 'success' => $success,
        'successMessage' => $successMessage]
    );
    $templateRenderer->render('leftMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('topMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('createTemplate', ['update' => true,'fields' => $fields, 'tempInfo' => $tempInfo, 'token' => $token]);
    $templateRenderer->render('footer', ['includeJS' => [URL_VENDORS.'select2/js/select2.min.js']]);
} catch (\Exception $e){
    $logger->warning($e->getMessage());
}