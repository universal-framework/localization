<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 31.5.2018 г.
 * Time: 23:52 ч.
 */

include_once '_main.php';

use Localization\Automation\Bing;
use Localization\Utils\QueryStringBuilder;
use Localization\Settings;
use Localization\Entity\Language;
use Localization\Utils\PDOWrapper;
use Localization\Entity\TranslatorService;
use Localization\Utils\Logger;
use Localization\Translator\Translator;
use Localization\Entity\Field;
use Localization\Page\PageTranslator;
use Localization\Entity\PageField;
use Localization\Page\Page;

$PDO = PDOWrapper::getInstance();

$settingsObj = new Settings($PDO);
$languageObj = new Language($PDO);
$translatorObj = new Translator($PDO);
$transService = new TranslatorService($PDO);
$fieldObj = new Field($PDO);
$pageTranslator = new PageTranslator($PDO);
$pageField = new PageField($PDO);
$page = new Page($PDO);

try {

    if (isset($_POST['LID'])) {
        $LID = intval($_POST['LID']);

        $settings = $settingsObj->getSettings();

        if (empty($settings['transType'])) {
            throw new \Exception('No translation type chosen for the mass translate. Visit settings for more info');
        }

        if (!$languageObj->load($LID)) {
            throw new \Exception(sprintf('There is no such language with id %d', $LID));
        }

        if (empty($settings['mainLangID'])) {
            throw new \Exception('No base language set up for translation. Visit settings for more info');
        }

        $SID = $settings['chosenSID'];

        $transData = $transService->getServiceByID($SID);

        if (count($transData) == 0) {
            throw new \Exception(sprintf("There is no such service with ID %d", $SID));
        }

        if ($transData['sName'] == 'bing') {
            $transAPI = new Bing($transData['sAPIKey'], new QueryStringBuilder());
        }


        $mainLangObj = new Language($PDO);
        $mainLangObj->load($settings['mainLangID']);

        if ($settings['transType'] == 'fillempty' || $settings['transType'] == 'overwrite') {
            $fieldTranslations = $translatorObj->getAllEmptyTranslations($LID, $settings['mainLangID'], $settings['transType']);
            $pageTranslations = $pageTranslator->getAllEmptyTranslations($languageObj->getData()['abbv'],$mainLangObj->getData()['abbv'],
                $settings['transType']);

            $translations = array_merge($fieldTranslations, $pageTranslations);

            if (count ($translations) == 0) {
                echo json_encode(['success' => true]);
                die();
            }

            $from = [$mainLangObj->getData()['code']];
            $to = [$languageObj->getData()['code']];

            foreach ($translations as $transData) {

                if ($transData['transType'] == 'field') {
                    $fieldObj->load($transData['FID']);
                } else {
                    $pageField->load($transData['pFieldID'], ['page_id' => $transData['PID']]);
                    $page->load($transData['PID']);
                }


                $transAPI->makeTranslationRequest($from, $to, $transData['translation']);

                $reqResult = $transAPI->getResult();

                if (isset($reqResult['error'])) {
                    $logger->alert(sprintf(
                        "Translation failed.Code: %d. Message: %s",
                        $reqResult['error']['code'],
                        $reqResult['error']['message']
                    ));

                    throw new \Exception("Automated translation response indicated there is an error. Please contact the admin.");
                }

                foreach ($reqResult[0]['translations'] as $transResult) {
                    $text = trim($transResult['text']);
                    $languageObj->load(null, ['code' => $transResult['to']]);
                    $transService->save($SID, $languageObj, $text, $settings['mainLangID'], $transData['translation']);
                    if ($transData['transType'] == 'field') {
                        $translatorObj->save($fieldObj, $languageObj, $text, true);
                    } else {
                        $pageTranslator->save($page, $languageObj, $pageField, $text);
                    }
                }
            }

        } else if ($settingsObj['transType'] == 'overwrite') {

        } else {
            echo $settingsObj['transType'];
            die();
        }

        echo json_encode(['success' => true]);
        die();
    }
} catch (\Exception $e) {
    echo json_encode(['success' => false, 'message' => $e->getMessage()]);
    die();
}

?>