<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 21.5.2018 г.
 * Time: 21:46 ч.
 */

use Localization\Entity\PageField;
use Localization\Utils\PDOWrapper;
use Localization\Utils\Logger;
use Localization\Utils\Error;
use Localization\Page\Page;

if ($_GET['delete'] >= 0 || $_GET['delete'] == 'all') {
    $deleteID = intval($_GET['delete']);
} else {
    header('Location: pages.php?list');
}

$error = Error::getInstance();
$logger = Logger::getInstance();
$PDO = PDOWrapper::getInstance();
$page = new Page($PDO);
$pageField = new PageField($PDO);

try {

    if ($_GET['delete'] == 'all') {
        $pageField->deleteAll();
        $page->deleteAll();
    }

    if ($pageField->delete($deleteID)) {
        $page->delete($deleteID);
        header('Location: pages.php?list&deleteSuccess='.$deleteID);
    } else {
        header('Location: pages.php?list&deleteError='.$deleteID);
    }
} catch (\Exception $e) {
    $logger->alert($e->getMessage());
    header('Location: pages.php?list&deleteError='.$deleteID);
}