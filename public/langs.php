<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 20.5.2018 г.
 * Time: 03:01 ч.
 */

require_once('_main.php');

if (isset($_GET['list'])) {
    include_once ('listLangs.php');
} else if (isset($_GET['add'])) {
    include_once ('addLang.php');
} else if (isset($_GET['update'])) {
    include_once ('updateLang.php');
} else if (isset($_GET['delete'])) {
    include_once ('deleteLang.php');
} else if (isset($_GET['overview'])) {
    include_once ('overviewLang.php');
} else {
    header('Location: dashboard.php');
}