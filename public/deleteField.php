<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 19.5.2018 г.
 * Time: 23:03 ч.
 */

use Localization\Entity\Field;
use Localization\Utils\PDOWrapper;
use Localization\Utils\Logger;
use Localization\Utils\Error;
use Localization\Translator\Translator;

if ($_GET['delete'] >= 0 || $_GET['delete'] == 'all') {
    $deleteID = intval($_GET['delete']);
} else {
    header('Location: fields.php?list');
}

$error = Error::getInstance();
$logger = Logger::getInstance();
$PDO = PDOWrapper::getInstance();
$field = new Field($PDO);
$translator = new Translator($PDO);

try {

    if ($_GET['delete'] == 'all') {
        $translator->deleteAll();
        $field->deleteAll();
    }

    if ($translator->delete($deleteID)) {
        $field->delete($deleteID);
        header('Location: fields.php?list&deleteSuccess='.$deleteID);
    } else {
        header('Location: fields.php?list&deleteError='.$deleteID);
    }
} catch (\Exception $e) {
    $logger->alert($e->getMessage());
    header('Location: fields.php?list&deleteError='.$deleteID);
}