<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 20.5.2018 г.
 * Time: 03:02 ч.
 */

use Localization\Entity\Language;
use Localization\Utils\PDOWrapper;
use Localization\Utils\TemplateRenderer;
use Localization\Utils\Logger;


$PDO = PDOWrapper::getInstance();
$logger = Logger::getInstance();

$success = null;
$successMessage = "";
$errors = null;


if (isset($_GET['deleteSuccess'])) {
    $success = true;
    $successMessage = sprintf("Successfully deleted language with ID %d", intval($_GET['deleteSuccess']));
}

if (isset($_GET['deleteError'])) {
    $errors = sprintf("There was an error while deleting language with ID %d",$_GET['deleteError']);
}

try {
    $langObj = new Language($PDO);
    $langsData = $langObj->getAll();
    $langsCount = $langObj->getCount();
} catch (\Exception $e){
    $logger->alert($e->getMessage());
}

try {
    $templateRenderer = TemplateRenderer::getInstance();
    $templateRenderer->render('header', ['title' => 'List Languages',
        'success' => $success, 'successMessage' => $successMessage, 'errors' => $errors]);
    $templateRenderer->render('leftMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('topMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('listLangs', ['langsData' => $langsData, 'langsCount' => $langsCount]);
    $templateRenderer->render('footer');
} catch (\Exception $e){
    $logger->warning($e->getMessage());
}