<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 19.5.2018 г.
 * Time: 22:13 ч.
 */

use Localization\Entity\Field;
use Localization\Utils\PDOWrapper;
use Localization\Utils\TemplateRenderer;
use Localization\Utils\Logger;
use Localization\Utils\Error;
use Localization\Utils\Token;
use Localization\Entity\Language;
use Localization\Translator\Translator;
use Localization\Settings;
use Localization\Entity\TranslatorService;

$PDO = PDOWrapper::getInstance();
$logger = Logger::getInstance();
$error = Error::getInstance();

$errors = null;


if ($_GET['update'] >= 0) {
    $updateID = intval($_GET['update']);
} else {
    header('Location: fields.php?list');
}

if (empty($_SESSION['field_form_token'])) {
    $_SESSION['field_form_token'] = Token::generateToken();
}

if (isset($_POST) && count($_POST) > 0) {

    $langObj = new Language($PDO);
    $langs = $langObj->getAll();

    $updated = updateField($updateID, $_POST, $error, $_SESSION['field_form_token'], $PDO, $langs);

    if ($updated) {
        header('Location: fields.php?update='.$updateID);
    } else {
        if ($error->isErrorSome()) {
            $errors = $error->fetchAll();
        } else {
            $errors = "Unknow error occured. Please contact the admin.";
        }

        $_SESSION['field_form_token'] = Token::generateToken();
    }
}

function updateField($updateID, $data, Error $errorObj, $token, \PDO $PDO, $possibleLangs = [])
{
    $langsToInsert = [];

    if (empty($data['CSRF_TOKEN'])) {
        $errorObj->add("Could not save the form. Please try again!");
        $errorObj->alert("CSRF token is empty");
        return false;
    }

    if (false === hash_equals($token, $data['CSRF_TOKEN'])) {
        $errorObj->add("Could not save the form. Please try again!");
        $errorObj->alert("CSRF token does not match as expected!");
        return false;
    }

    $label = strip_tags(trim($data['label']));

    if( count($possibleLangs) > 0 ) {
        foreach ($possibleLangs as $langData) {
            if (isset($data['lang_'.intval($langData['LID'])])) {
                $langsToInsert[$langData['lLangAbbv']] = strip_tags(trim($data['lang_'.intval($langData['LID'])]));
            } else {
                $langsToInsert[$langData['lLangAbbv']] = "";
            }
        }
    }

    try {
        $field = new Field($PDO);
        $language = new Language($PDO);
        $fieldTranslations = new Translator($PDO);

        $updated = $field->update($updateID, ['label' => $label]);

        if ($updated > 0 && count($langsToInsert) > 0) {
            foreach($langsToInsert as $lang => $translation){
                if ($language->load(null, ['lang' => $lang])) {
                    $fieldTranslations->save($field, $language, $translation, true);
                }
            }
        }

        return true;
    } catch (\Exception $e){
        $errorObj->add($e->getMessage());
        return false;
    }
}

$token = $_SESSION['field_form_token'];

try {
    $langObj = new Language($PDO);
    $langs = $langObj->getAll();

    $field = new Field($PDO);
    $field->load($updateID);
    $fieldData = $field->getData();

    $translator = new Translator($PDO);
    $translations = $translator->getTranslations($field);

    $settings = new Settings($PDO);

    $settings = $settings->getSettings();
    if (count($settings) > 0) {
        $chosenLang = $settings['mainLangID'];
    } else {
        $chosenLang = 0;
    }

    $transService = new TranslatorService($PDO);
    if ($settings['chosenSID'] > 0) {
        $serviceTrans = $transService->getAllTrans($settings['chosenSID'], $chosenLang, $translations[$chosenLang]);
    } else {
        $serviceTrans = [];
    }

} catch (\Exception $e){
    $logger->alert($e->getMessage());
    $error->add("Could not fetch adequately!");
}

if ($error->isErrorSome()) {
   $errors = $error->fetchAll();
}

try {
    $templateRenderer = TemplateRenderer::getInstance();
    $templateRenderer->render('header', ['title' => sprintf('Update field %s', $fieldData['label']), 'errors' => $errors,
        'includeCSS' => [URL_VENDORS.'flags/css/flag-icon.min.css']]);
    $templateRenderer->render('leftMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('topMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('createField', ['update' => true, 'fieldData' => $fieldData,
        'langs' => $langs, 'token' => $token, 'translations' => $translations, 'chosenLang' => $chosenLang,
        'updateID' => $updateID, 'serviceTrans' => $serviceTrans]);
    $templateRenderer->render('footer', ['includeJS' => [URL_VENDORS.'select2/js/select2.min.js']]);
} catch (\Exception $e){
    $logger->warning($e->getMessage());
}