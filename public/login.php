<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 12.5.2018 г.
 * Time: 17:31 ч.
 */

include_once('../mainloader.php');

use Localization\Utils\Error;
use Localization\Entity\AdminUser;
use Localization\Utils\PDOWrapper;
use Localization\Utils\TemplateRenderer;
use Localization\Utils\Logger;
use Localization\Utils\Token;

$error = Error::getInstance();
$logger = Logger::getInstance();
$errors = null;

if (isset($_SESSION['UID']) && is_numeric($_SESSION['UID'])) {
    header('Location: dashboard.php');
}

if (empty($_SESSION['token'])) {
    if (function_exists('mcrypt_create_iv')) {
        $_SESSION['token'] = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
    } else {
        $_SESSION['token'] = bin2hex(openssl_random_pseudo_bytes(32));
    }
}

if (isset($_POST) && count($_POST) > 0) {
    $userData = login($_POST, $error, $_SESSION['token']);

    if (false === $userData) {
        if ($error->isErrorSome()) {
            $errors = $error->fetchAll();
        } else {
            $errors = "Unknow error occured. Please contact the admin.";
        }

        $_SESSION['token'] = Token::generateToken();

    } else {
        $_SESSION['UID'] = $userData['id'];
        $_SESSION['username'] = $userData['username'];
        header('Location: dashboard.php');
    }
}

/**
 * @param $data
 * @param Error $errorObj
 * @param $token
 * @return bool
 * @throws Exception
 */

function login($data, $errorObj, $token)
{
    if (empty($data['CSRF_TOKEN'])) {
        $errorObj->add("Could not log in. Please try again!");
        $errorObj->alert("CSRF token is empty");
        return false;
    }

    if (false === hash_equals($token, $data['CSRF_TOKEN'])) {
        $errorObj->add("Could not log in. Please try again!");
        $errorObj->alert("CSRF token does not match as expected!");
        return false;
    }

    $userName = trim($data['username']);
    $password = trim($data['password']);

    try {
        $adminUser = new AdminUser(PDOWrapper::getInstance());
        $loaded = $adminUser->load(null, ['user' => $userName, 'pass' => $password]);
    } catch (\Exception $e){
        $errorObj->add($e->getMessage());
        return false;
    }

    if ($loaded === false) {
        $errorObj->add("Wrong credentials. Please try again");
        return false;
    } else {
        return ['id' => $adminUser->getId(), 'username' => $adminUser->getUsername()];
    }
}

$token = $_SESSION['token'];

try {
    $templateRenderer = TemplateRenderer::getInstance();

    $templateRenderer->render('login', ['errors' => $errors, 'token' => $token]);
} catch (\Exception $e){
    $logger->warning($e->getMessage());
}

?>