<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 14.5.2018 г.
 * Time: 21:31 ч.
 */

require_once('_main.php');

if (isset($_GET['list'])) {
    include_once ('listTemplates.php');
} else if (isset($_GET['create'])) {
    include_once ('createTemplate.php');
} else if (isset($_GET['update'])) {
    include_once ('updateTemplate.php');
} else if (isset($_GET['delete'])) {
    include_once ('deleteTemplate.php');
} else if (isset($_GET['overview'])) {
    include_once ('overviewTemplate.php');
} else {
    header('Location: dashboard.php');
}