<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 21.5.2018 г.
 * Time: 21:54 ч.
 */

use Localization\Entity\Template;
use Localization\Utils\PDOWrapper;
use Localization\Utils\TemplateRenderer;
use Localization\Utils\Logger;
use Localization\Utils\Error;
use Localization\Page\Page;
use Localization\Entity\PageField;
use Localization\Page\PageTranslator;
use Localization\Translator\Translator;
use Localization\Entity\Field;
use Localization\Template\Field2Template;
use Localization\Entity\Language;

$PDO = PDOWrapper::getInstance();
$logger = Logger::getInstance();
$error = Error::getInstance();
$pageData = [];
$templates = [];
$extraFields = [];

$errors = null;

if ($_GET['overview'] >= 0) {
    $overviewID = intval($_GET['overview']);
} else {
    header('Location: pages.php?list');
}

try {

    $page = new Page($PDO);
    if ($page->load($overviewID)) {
        $pageData = $page->getData();

        $template = new Template($PDO);
        $templates = $template->getTemplatesByTempTypes();

        $language = new Language($PDO);
        $languages = $language->getAll();

        $translator = new Translator($PDO);
        $pageTranslator = new PageTranslator($PDO);

        $fieldObj = new Field($PDO);

        $field2Temp = new Field2Template($PDO);

        //Acquire template name
        if (isset($pageData['hTempID']) && isset($templates['h']) && count($templates['h']) > 0) {
            $fields = $field2Temp->load($pageData['hTempID']);
            if (count($fields) > 0) {
                foreach($fields as $FID) {
                    if($fieldObj->load($FID)) {
                        $pageData['hTempTranslations'][$fieldObj->getData()['label']] = $translator->getTranslations($fieldObj);
                    }
                }
            }

            foreach ($templates['h'] as $headerTemps) {
                if ($pageData['hTempID'] == $headerTemps['TID']) {
                    $pageData['hTempName'] = $headerTemps['tUniqueLabel'];
                }
            }
        }

        if (isset($pageData['mTempID']) && isset($templates['s']) && count($templates['s']) > 0) {
            $fields = $field2Temp->load($pageData['mTempID']);
            if (count($fields) > 0) {
                foreach($fields as $FID) {
                    if($fieldObj->load($FID)) {
                        $pageData['mTempTranslations'][$fieldObj->getData()['label']] = $translator->getTranslations($fieldObj);
                    }
                }
            }
            foreach ($templates['s'] as $standardTemps) {
                if ($pageData['mTempID'] == $standardTemps['TID']) {
                    $pageData['mTempName'] = $standardTemps['tUniqueLabel'];
                }
            }
        }

        if (isset($pageData['fTempID']) && isset($templates['f']) && count($templates['f']) > 0) {
            $fields = $field2Temp->load($pageData['fTempID']);
            if (count($fields) > 0) {
                foreach($fields as $FID) {
                    if($fieldObj->load($FID)) {
                        $pageData['fTempTranslations'][$fieldObj->getData()['label']] = $translator->getTranslations($fieldObj);
                    }
                }
            }
            foreach ($templates['f'] as $footerTemp) {
                if ($pageData['fTempID'] == $footerTemp['TID']) {
                    $pageData['fTempName'] = $footerTemp['tUniqueLabel'];
                }
            }
        }

        $pageField = new PageField($PDO);
        $extraFields = $pageField->loadAllFields($overviewID);



        if (count($extraFields) > 0) {
            $extraFieldIDS = array_column($extraFields,'FID');
            foreach($extraFieldIDS as $FID) {
                if($pageField->load($FID, ['page_id' => $overviewID])) {
                    $pageData['extraTranslations'][$pageField->getData()['label']] = $pageTranslator->getTranslations($page, $pageField);
                }
            }
        }

} else {
    $error->add(sprintf('Could not find a page with ID %d', $overviewID));
}


} catch (\Exception $e){
    $logger->alert($e->getMessage());
    $error->add(Error::$standardMessage);
}

if ($error->isErrorSome()) {
    $errors = $error->fetchAll();
}

try {
    $templateRenderer = TemplateRenderer::getInstance();
    $templateRenderer->render('header', ['title' => (isset($pageData['label'])) ?
            sprintf('Overview page %s', $pageData['label']) : "Overview of page with no ID", 'errors' => $errors,
            'includeCSS' => [URL_VENDORS.'flags/css/flag-icon.min.css']]
    );
    $templateRenderer->render('leftMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('topMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('overviewPage', ['pageData' => $pageData, 'templates' => $templates,
        'langs' => $languages]);
    $templateRenderer->render('footer', []);
} catch (\Exception $e){
    $logger->warning($e->getMessage());
}