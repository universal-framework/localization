<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 19.5.2018 г.
 * Time: 22:38 ч.
 */


use Localization\Entity\Field;
use Localization\Utils\PDOWrapper;
use Localization\Utils\TemplateRenderer;
use Localization\Utils\Logger;
use Localization\Utils\Error;
use Localization\Entity\Language;
use Localization\Translator\Translator;

$PDO = PDOWrapper::getInstance();
$logger = Logger::getInstance();
$error = Error::getInstance();

$errors = null;

if ($_GET['overview'] >= 0) {
    $overviewID = intval($_GET['overview']);
} else {
    header('Location: fields.php?list');
}

try {
    $langObj = new Language($PDO);
    $langs = $langObj->getAll();

    $field = new Field($PDO);
    if (false === $field->load($overviewID)) {
        $error->add("No such field exists!");
    }
    $fieldData = $field->getData();

    $translator = new Translator($PDO);
    $translations = $translator->getTranslations($field);
} catch (\Exception $e){
    $logger->alert($e->getMessage());
}

if ($error->isErrorSome()) {
    $errors = $error->fetchAll();
}

try {
    $templateRenderer = TemplateRenderer::getInstance();
    $templateRenderer->render('header', ['title' => sprintf('Overview field %s', $fieldData['label']), 'errors' => $errors,
        'includeCSS' => [URL_VENDORS.'flags/css/flag-icon.min.css']]);
    $templateRenderer->render('leftMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('topMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('overviewField', ['fieldData' => $fieldData,
        'langs' => $langs, 'translations' => $translations]);
    $templateRenderer->render('footer', []);
} catch (\Exception $e){
    $logger->warning($e->getMessage());
}