<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 13.5.2018 г.
 * Time: 18:44 ч.
 */

include_once ('../mainloader.php');

if (isset($_SESSION['UID']) && is_numeric($_SESSION['UID'])) {
    session_destroy();
}

header('Location: login.php');

