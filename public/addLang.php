<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 20.5.2018 г.
 * Time: 03:14 ч.
 */

use Localization\Utils\PDOWrapper;
use Localization\Utils\TemplateRenderer;
use Localization\Utils\Logger;
use Localization\Utils\Error;
use Localization\Utils\Token;
use Localization\Entity\Language;

$PDO = PDOWrapper::getInstance();
$logger = Logger::getInstance();
$error = Error::getInstance();

$errors = null;

if (empty($_SESSION['field_form_token'])) {
    $_SESSION['field_form_token'] = Token::generateToken();
}

if (isset($_POST) && count($_POST) > 0) {

    $langID = addLanguage($_POST, $error, $_SESSION['field_form_token'], $PDO);

    if ($langID > 0) {
        header('Location: langs.php?overview='.$langID);
    } else {
        if ($error->isErrorSome()) {
            $errors = $error->fetchAll();
        } else {
            $errors = "Unknow error occured. Please contact the admin.";
        }

        $_SESSION['field_form_token'] = Token::generateToken();
    }
}

function addLanguage ($data, Error $error, $token, \PDO $PDO) {

    if (empty($data['CSRF_TOKEN'])) {
        $error->add("Could not save the form. Please try again!");
        $error->alert("CSRF token is empty");
        return false;
    }

    if (false === hash_equals($token, $data['CSRF_TOKEN'])) {
        $error->add("Could not save the form. Please try again!");
        $error->alert("CSRF token does not match as expected!");
        return false;
    }

    $abbv = strip_tags(trim($data['abbv']));
    $desc = strip_tags(trim($data['desc']));
    $code = strip_tags(trim($data['code']));

    try {
        $language = new Language($PDO);

        $langID = $language->save(['abbv' => $abbv, 'desc' => $desc, 'code' => $code]);

        return $langID;
    } catch (\Exception $e){
        $error->add($e->getMessage());
        return false;
    }
}

$token = $_SESSION['field_form_token'];

try {
    $templateRenderer = TemplateRenderer::getInstance();
    $templateRenderer->render('header', ['title' => 'Add new language', 'errors' => $errors,
        'includeCSS' => [URL_VENDORS.'flags/css/flag-icon.min.css']]);
    $templateRenderer->render('leftMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('topMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('addLang', ['token' => $token]);
    $templateRenderer->render('footer', ['includeJS' => [URL_VENDORS.'select2/js/select2.min.js']]);
} catch (\Exception $e){
    $logger->warning($e->getMessage());
}