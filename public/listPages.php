<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 20.5.2018 г.
 * Time: 13:47 ч.
 */

use Localization\Page\Page;
use Localization\Utils\PDOWrapper;
use Localization\Utils\TemplateRenderer;
use Localization\Utils\Logger;
use Localization\Page\PageTranslator;
use Localization\Entity\Template;

$PDO = PDOWrapper::getInstance();
$logger = Logger::getInstance();

$success = null;
$successMessage = "";
$errors = null;

if (isset($_GET['deleteSuccess'])) {
    $success = true;
    $successMessage = sprintf("Successfully deleted page with ID %d", intval($_GET['deleteSuccess']));
}

if (isset($_GET['deleteError'])) {
    $errors = sprintf("There was an error while deleting page with ID %d",$_GET['deleteError']);
}

try {
    $page = new Page($PDO);
    $pageTranslator = new PageTranslator($PDO);
    $pages = $page->getAll();

    if (count($pages) > 0) {

        $template = new Template($PDO);

        foreach ($pages as &$pageData) {

            if (!empty($pageData['pTempHeaderID'])) {
                if ($template->load($pageData['pTempHeaderID'])) {
                    $pageData['pTempHeaderName'] = $template->getData()['label'];
                }
            } else {
                $pageData['pTempHeaderName'] = "";
            }

            if (!empty($pageData['pTempMainID'])) {
                if ($template->load($pageData['pTempMainID'])) {
                    $pageData['pTempMainName'] = $template->getData()['label'];
                }
            } else{
                $pageData['pTempMainName'] = "";
            }

            if (!empty($pageData['pTempFooterID'])) {
                if ($template->load($pageData['pTempFooterID'])) {
                    $pageData['pTempFooterName'] = $template->getData()['label'];
                }
            } else {
                $pageData['pTempFooterName'] = "";
            }
        }
    }

    $pagesCount= $page->getCount();
    if ($pagesCount > 0) {
        foreach ($pages as &$pageData) {
            $pageData['pTransCount'] = $pageTranslator->getCustomTranslationsCount($pageData['PID']);
        }
    }
} catch (\Exception $e){
    $logger->alert($e->getMessage());
}

try {
    $templateRenderer = TemplateRenderer::getInstance();
    $templateRenderer->render('header', ['title' => 'List Pages',
        'success' => $success, 'successMessage' => $successMessage, 'errors' => $errors]);
    $templateRenderer->render('leftMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('topMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('listPages', ['pageData' => $pages, 'pagesCount' => $pagesCount]);
    $templateRenderer->render('footer');
} catch (\Exception $e){
    $logger->warning($e->getMessage());
}