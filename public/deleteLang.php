<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 20.5.2018 г.
 * Time: 13:16 ч.
 */

use Localization\Entity\Language;
use Localization\Utils\PDOWrapper;
use Localization\Utils\Logger;
use Localization\Utils\Error;
use Localization\Translator\Translator;

if ($_GET['delete'] >= 0 || $_GET['delete'] == 'all') {
    $deleteID = intval($_GET['delete']);
} else {
    header('Location: langs.php?list');
}

$error = Error::getInstance();
$logger = Logger::getInstance();
$PDO = PDOWrapper::getInstance();
$language = new Language($PDO);
$translator = new Translator($PDO);

try {

    if ($_GET['delete'] == 'all') {
        $language->deleteAll();
        $translator->deleteAll();
    }

    if ($translator->deleteByLID($deleteID)) {
        $language->delete($deleteID);
        header('Location: langs.php?list&deleteSuccess='.$deleteID);
    } else {
        header('Location: langs.php?list&deleteError='.$deleteID);
    }
} catch (\Exception $e) {
    $logger->alert($e->getMessage());
    header('Location: langs.php?list&deleteError='.$deleteID);
}