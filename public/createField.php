<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 18.5.2018 г.
 * Time: 00:06 ч.
 */

use Localization\Entity\Field;
use Localization\Utils\PDOWrapper;
use Localization\Utils\TemplateRenderer;
use Localization\Utils\Logger;
use Localization\Utils\Error;
use Localization\Utils\Token;
use Localization\Entity\Language;
use Localization\Translator\Translator;
use Localization\Settings;

$PDO = PDOWrapper::getInstance();
$logger = Logger::getInstance();
$error = Error::getInstance();

$errors = null;

if (empty($_SESSION['field_form_token'])) {
    $_SESSION['field_form_token'] = Token::generateToken();
}

if (isset($_POST) && count($_POST) > 0) {

    $langObj = new Language($PDO);
    $langs = $langObj->getAll();

    $fieldID = createField($_POST, $error, $_SESSION['field_form_token'], $PDO, $langs);

    if ($fieldID > 0) {
        header('Location: fields.php?overview='.$fieldID);
    } else {
        if ($error->isErrorSome()) {
            $errors = $error->fetchAll();
        } else {
            $errors = "Unknow error occured. Please contact the admin.";
        }

        $_SESSION['field_form_token'] = Token::generateToken();
    }
}

function createField($data, Error $errorObj, $token, \PDO $PDO, $possibleLangs = [])
{
    $langsToInsert = [];

    if (empty($data['CSRF_TOKEN'])) {
        $errorObj->add("Could not save the form. Please try again!");
        $errorObj->alert("CSRF token is empty");
        return false;
    }

    if (false === hash_equals($token, $data['CSRF_TOKEN'])) {
        $errorObj->add("Could not save the form. Please try again!");
        $errorObj->alert("CSRF token does not match as expected!");
        return false;
    }

    $label = strip_tags(trim($data['label']));

    if( count($possibleLangs) > 0 ) {
        foreach ($possibleLangs as $langData) {
            if (isset($data['lang_'.intval($langData['LID'])])) {
                $langsToInsert[$langData['lLangAbbv']] = strip_tags(trim($data['lang_'.intval($langData['LID'])]));
            }
        }
    }



    try {
        $field = new Field($PDO);
        $language = new Language($PDO);
        $fieldTranslations = new Translator($PDO);

        $newID = $field->save(['label' => $label]);

        if ($newID > 0 && count($langsToInsert) > 0) {
            foreach($langsToInsert as $lang => $translation){
                if ($language->load(null, ['lang' => $lang])) {
                    $fieldTranslations->save($field, $language, $translation);
                }
            }
        }

        return $field->getId();
    } catch (\Exception $e){
        $errorObj->add($e->getMessage());
        return false;
    }
}

$token = $_SESSION['field_form_token'];

try {
    $langObj = new Language($PDO);
    $langs = $langObj->getAll();

    $settings = new Settings($PDO);

    $settings = $settings->getSettings();
    if ($settings !== false ) {
        $chosenLang = $settings['mainLangID'];
    } else {
        $chosenLang = 0;
    }

} catch (\Exception $e){
    $logger->alert($e->getMessage());
}

try {
    $templateRenderer = TemplateRenderer::getInstance();
    $templateRenderer->render('header', ['title' => 'Create new field', 'errors' => $errors,
        'includeCSS' => [URL_VENDORS.'flags/css/flag-icon.min.css']]);
    $templateRenderer->render('leftMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('topMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('createField', ['langs' => $langs, 'token' => $token, 'chosenLang' => $chosenLang]);
    $templateRenderer->render('footer', ['includeJS' => [URL_VENDORS.'select2/js/select2.min.js']]);
} catch (\Exception $e){
    $logger->warning($e->getMessage());
}