<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 14.5.2018 г.
 * Time: 21:34 ч.
 */

use Localization\Entity\Template;
use Localization\Utils\PDOWrapper;
use Localization\Utils\TemplateRenderer;
use Localization\Utils\Logger;

$PDO = PDOWrapper::getInstance();
$logger = Logger::getInstance();

$success = null;
$successMessage = "";
$errors = null;

if (isset($_GET['deleteSuccess'])) {
    $success = true;
    $successMessage = sprintf("Successfully deleted template with ID %d", intval($_GET['deleteSuccess']));
}

if (isset($_GET['deleteError'])) {
    $errors = sprintf("There was an error while deleting template with ID %d",$_GET['deleteError']);
}

try {
    $template = new Template($PDO);
    $templatesData = $template->listAll();

    $templatesCount = $template->getCount();
} catch (\Exception $e){
    $logger->alert($e->getMessage());
}

try {
    $templateRenderer = TemplateRenderer::getInstance();
    $templateRenderer->render('header', ['title' => 'List templates',
        'success' => $success, 'successMessage' => $successMessage, 'errors' => $errors]);
    $templateRenderer->render('leftMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('topMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('listTemplates', ['templatesData' => $templatesData, 'templatesCount' => $templatesCount]);
    $templateRenderer->render('footer');
} catch (\Exception $e){
    $logger->warning($e->getMessage());
}

