<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 12.5.2018 г.
 * Time: 19:14 ч.
 */

require_once('_main.php');

use Localization\Utils\TemplateRenderer;
use Localization\Utils\Logger;
use Localization\Page\Page;
use Localization\Utils\PDOWrapper;
use Localization\Entity\Template;
use Localization\Entity\Field;
use Localization\Entity\Language;
use Localization\Translator\Translator;
use Localization\Page\PageTranslator;

$logger = Logger::getInstance();

$PDO = PDOWrapper::getInstance();

$page = new Page($PDO);
$template = new Template($PDO);
$field = new Field($PDO);
$language = new Language($PDO);
$translator = new Translator($PDO);
$pageTranslator = new PageTranslator($PDO);

try {
    $countData['pagesCount'] = $page->getCount();
    $countData['templatesCount'] = $template->getCount();
    $countData['fieldsCount'] = $field->getCount();
    $countData['languagesCount'] = $language->getCount();
    $countData['translationsCount'] = $translator->getCount();
    $countData['pageTransCount'] = $pageTranslator->getCount();
} catch (\Exception $e){
    $logger->alert($e->getMessage());
}

try {
    $templateRenderer = TemplateRenderer::getInstance();
    $templateRenderer->render('header', ['title' => 'Dashboard']);
    $templateRenderer->render('leftMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('topMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('dashboard', $countData);
    $templateRenderer->render('footer');
} catch (\Exception $e){
    $logger->warning($e->getMessage());
}
