<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 26.5.2018 г.
 * Time: 21:48 ч.
 */

include_once '_main.php';

use Localization\Automation\Bing;
use Localization\Utils\QueryStringBuilder;
use Localization\Settings;
use Localization\Entity\Language;
use Localization\Utils\PDOWrapper;
use Localization\Entity\TranslatorService;
use Localization\Utils\Logger;

$PDO = PDOWrapper::getInstance();

$settingsObj = new Settings($PDO);
$languageObj = new Language($PDO);
$transService = new TranslatorService($PDO);
$logger = new Logger($PDO);


try {

    if (isset($_POST) && count($_POST) > 0) {

        $LID = $_POST['LID'];
        $mainTransTxt = strip_tags(trim($_POST['text']));
        $returnArr = [];

        $settings = $settingsObj->getSettings();

        if (count($settings) == 0 || empty($settings['mainLangID'])) {
            throw new \Exception ("There is no main language chosen for translations. Please visit settings and set accordingly");
        }

        if ($languageObj->load($settings['mainLangID'])) {
            $fromLangCode = $languageObj->getData()['code'];
        } else {
            throw new \Exception("From language not recognized!");
        }

        $from = [$fromLangCode];

        if ($LID !== "all") {
            $LID = intval($LID);
            if (!$languageObj->load($LID)) {
                throw new \Exception (sprintf("There is no such language with ID %d", $LID));
            }
            $to = [$languageObj->getData()['code']];
        } else {
            $allLangs = $languageObj->getAll();
            $to = array_filter(array_map(function($val){
                return strtolower($val);
            },array_column($allLangs,'lBingRef')), function($val){
                return !empty($val);
            });
        }

        if (empty($settings['chosenSID'])) {
            throw new \Exception ("There is no chosen service! Please visit settings and set accordingly.");
        }

        $transCache = $transService->getTranslationFromCache($settings['chosenSID'], $LID, $settings['mainLangID'], $mainTransTxt);

        if (count($transCache) > 0) {
            $retArr = $transCache;

            if($LID == "all") {
                $allLIDS = array_column($allLangs, 'LID');
                $returnLIDS = array_keys($transCache);

                $nonCachedLIDS = array_diff($allLIDS, $returnLIDS);

                if (count($nonCachedLIDS) == 0) {
                    $retArr['success'] = true;
                    echo json_encode($retArr);
                    die();
                }

                $to = [];

                foreach ($nonCachedLIDS as $nLID) {
                    $indexInLangs = array_search($nLID, $allLIDS);
                    if (!empty($allLangs[$indexInLangs]['lBingRef'])) {
                        $to[] = $allLangs[$indexInLangs]['lBingRef'];
                    }
                }

                if (count($to) == 0) {
                    $retArr['success'] = true;
                    echo json_encode($retArr);
                    die();
                }
            }
        }

        $SID = $settings['chosenSID'];

        $transData = $transService->getServiceByID($SID);

        if (count($transData) == 0) {
            throw new \Exception(sprintf("There is no such service with ID %d", $SID));
        }

        if ($transData['sName'] == 'bing') {
            $transAPI = new Bing($transData['sAPIKey'], new QueryStringBuilder());
        }

        $transAPI->makeTranslationRequest($from, $to, $mainTransTxt);

        $reqResult = $transAPI->getResult();

        if (isset($reqResult['error'])) {
            $logger->alert(sprintf(
                "Translation failed.Code: %d. Message: %s",
                $reqResult['error']['code'],
                $reqResult['error']['message']
            ));

            throw new \Exception("Automated translation response indicated there is an error. Please contact the admin.");
        }

        foreach ($reqResult[0]['translations'] as $transResult) {
            $text = trim($transResult['text']);
            $languageObj->load(null, ['code' => $transResult['to']]);
            $transService->save($SID, $languageObj, $text, $settings['mainLangID'], $mainTransTxt);
            $returnArr[$languageObj->getId()] = $text;
        }

        if (count($returnArr) == 0) {
            $returnArr = ['success' => false, 'message' => "No translations processed"];
        } else {
            $returnArr['success'] = true;
        }

        echo json_encode($returnArr);
        die();

    }

} catch (\Exception $e){
    echo json_encode(['success' => false, 'message' => $e]);
    die();
}