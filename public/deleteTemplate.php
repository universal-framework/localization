<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 16.5.2018 г.
 * Time: 22:53 ч.
 */

use Localization\Entity\Template;
use Localization\Utils\PDOWrapper;
use Localization\Utils\Logger;
use Localization\Utils\Error;
use Localization\Template\Field2Template;

if ($_GET['delete'] >= 0 || $_GET['delete'] == 'all') {
    $deleteID = intval($_GET['delete']);
} else {
    header('Location: templates.php?list');
}

$error = Error::getInstance();
$logger = Logger::getInstance();
$PDO = PDOWrapper::getInstance();
$template = new Template($PDO);
$field2Template = new Field2Template($PDO);

try {

    if ($_GET['delete'] == 'all') {
        $field2Template->deleteAll();
        $template->deleteAll();
    }

    if ($field2Template->delete($deleteID)) {
        $template->delete($deleteID);
        header('Location: templates.php?list&deleteSuccess='.$deleteID);
    } else {
        header('Location: templates.php?list&deleteError='.$deleteID);
    }
} catch (\Exception $e) {
    $logger->alert($e->getMessage());
    header('Location: listTemplates.php?list&deleteError');
}