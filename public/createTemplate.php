<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 14.5.2018 г.
 * Time: 21:34 ч.
 */

use Localization\Entity\Template;
use Localization\Utils\PDOWrapper;
use Localization\Utils\TemplateRenderer;
use Localization\Utils\Logger;
use Localization\Entity\Field;
use Localization\Utils\Error;
use Localization\Utils\Token;
use Localization\Template\Field2Template;

$PDO = PDOWrapper::getInstance();
$logger = Logger::getInstance();
$error = Error::getInstance();

$errors = null;

if (empty($_SESSION['field_form_token'])) {
    $_SESSION['field_form_token'] = Token::generateToken();
}

if (isset($_POST) && count($_POST) > 0) {
    $templateID = createTemplate($_POST, $error, $_SESSION['field_form_token'], $PDO);

    if ($templateID > 0) {
        header('Location: templates.php?overview='.$templateID);
    } else {
        if ($error->isErrorSome()) {
            $errors = $error->fetchAll();
        } else {
            $errors = "Unknow error occured. Please contact the admin.";
        }

        $_SESSION['field_form_token'] = Token::generateToken();
    }
}

function createTemplate($data, Error $errorObj, $token, \PDO $PDO)
{
    if (empty($data['CSRF_TOKEN'])) {
        $errorObj->add("Could not save the form. Please try again!");
        $errorObj->alert("CSRF token is empty");
        return false;
    }

    if (false === hash_equals($token, $data['CSRF_TOKEN'])) {
        $errorObj->add("Could not save the form. Please try again!");
        $errorObj->alert("CSRF token does not match as expected!");
        return false;
    }

    $label = trim($data['label']);
    $desc = trim($data['desc']);
    $tType = trim($data['tempType']);
    $fields = isset($data['fields'])? $data['fields'] : [];

    try {
        $template = new Template(PDOWrapper::getInstance());
        $newID = $template->save(['name' => $label, 'desc' => $desc, 'tType' => $tType]);

        if (is_array($fields) && count($fields) > 0) {

            $field = new Field($PDO);
            $f2Template = new Field2Template($PDO);

            foreach ($fields as $FID) {
                $field->load($FID);
                $f2Template->save($field, $template);
            }
        }

        return $newID;
    } catch (\Exception $e){
        $errorObj->add($e->getMessage());
        return false;
    }
}

$token = $_SESSION['field_form_token'];

try {
    $field = new Field($PDO);
    $fields = $field->getAll();
} catch (\Exception $e){
    $logger->alert($e->getMessage());
}

try {
    $templateRenderer = TemplateRenderer::getInstance();
    $templateRenderer->render('header', ['title' => 'Create new template',
        'includeCSS' => [URL_VENDORS.'select2/css/select2.min.css'], 'errors' => $errors]);
    $templateRenderer->render('leftMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('topMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('createTemplate', ['fields' => $fields, 'token' => $token]);
    $templateRenderer->render('footer', ['includeJS' => [URL_VENDORS.'select2/js/select2.min.js']]);
} catch (\Exception $e){
    $logger->warning($e->getMessage());
}