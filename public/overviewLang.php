<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 20.5.2018 г.
 * Time: 13:03 ч.
 */

use Localization\Utils\PDOWrapper;
use Localization\Utils\TemplateRenderer;
use Localization\Utils\Logger;
use Localization\Utils\Error;
use Localization\Entity\Language;

$PDO = PDOWrapper::getInstance();
$logger = Logger::getInstance();
$error = Error::getInstance();

$errors = null;

if ($_GET['overview'] >= 0) {
    $overviewID = intval($_GET['overview']);
} else {
    header('Location: langs.php?list');
}

try {

    $language = new Language($PDO);
    if (false === $language->load($overviewID)) {
        $error->add("No such field exists!");
    }

    $langData = $language->getData();
} catch (\Exception $e){
    $logger->alert($e->getMessage());
    $error->add("Unknown error occured. Please contact admin!");
}

if ($error->isErrorSome()) {
    $errors = $error->fetchAll();
}

try {
    $templateRenderer = TemplateRenderer::getInstance();
    $templateRenderer->render('header', ['title' => (isset($langData['abbv']))
        ? sprintf('Overview lang %s', $langData['abbv']) : 'Overview no lang...', 'errors' => $errors,
        'includeCSS' => [URL_VENDORS.'flags/css/flag-icon.min.css']]);
    $templateRenderer->render('leftMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('topMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('overviewLang', ['langData' => $langData]);
    $templateRenderer->render('footer', []);
} catch (\Exception $e){
    $logger->warning($e->getMessage());
}