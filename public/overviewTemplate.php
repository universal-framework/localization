<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 16.5.2018 г.
 * Time: 23:24 ч.
 */

use Localization\Entity\Template;
use Localization\Utils\PDOWrapper;
use Localization\Utils\TemplateRenderer;
use Localization\Utils\Logger;
use Localization\Entity\Field;
use Localization\Utils\Error;
use Localization\Template\Field2Template;

if ($_GET['overview'] >= 0) {
    $updateID = intval($_GET['overview']);
} else {
    header('Location: templates.php?list');
}

$PDO = PDOWrapper::getInstance();
$logger = Logger::getInstance();
$error = Error::getInstance();
$success = false;
$successMessage = "";
$errors = null;
$tempInfo = [];


try {
    $field = new Field($PDO);

    $template = new Template($PDO);
    if ($template->load($updateID)) {
        $tempInfo = $template->getData();
        $tempInfo['fields'] = [];

        // Get the fields associated with the template

        $field2Template = new Field2Template($PDO);
        $fieldIDS = $field2Template->load($tempInfo['id']);
        if (count ($fieldIDS) > 0) {
            foreach ($fieldIDS as $fieldID) {
                if ($field->load($fieldID)) {
                    $tempFieldData = $field->getData();
                    $tempFieldData['fCount'] = $field->getTranslationsCount();
                    $tempInfo['fields'][] = $tempFieldData;
                } else {
                    $logger->warning(sprintf("Field with ID %d not found in the database", $fieldID));
                }
            }
        }

    } else {
        $error->add(sprintf('Could not find a template with ID %d', $updateID));
    }

} catch (\Exception $e){
    $logger->alert($e->getMessage());
    $error->add(Error::$standardMessage);
}

if ($error->isErrorSome()) {
    $errors = $error->fetchAll();
}

try {
    $templateRenderer = TemplateRenderer::getInstance();
    $templateRenderer->render('header', [
            'title' => (isset($tempInfo['label'])) ? sprintf('Overview template %s', $tempInfo['label']) : 'Not found',
            'errors' => $errors, 'success' => $success,
            'successMessage' => $successMessage]
    );
    $templateRenderer->render('leftMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('topMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('overviewTemplate', ['tempInfo' => $tempInfo]);
    $templateRenderer->render('footer', []);
} catch (\Exception $e){
    $logger->warning($e->getMessage());
}