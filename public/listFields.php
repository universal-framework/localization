<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 17.5.2018 г.
 * Time: 23:46 ч.
 */


use Localization\Entity\Field;
use Localization\Utils\PDOWrapper;
use Localization\Utils\TemplateRenderer;
use Localization\Utils\Logger;

$PDO = PDOWrapper::getInstance();
$logger = Logger::getInstance();

$success = null;
$successMessage = "";
$errors = null;

if (isset($_GET['deleteSuccess'])) {
    $success = true;
    $successMessage = sprintf("Successfully deleted field with ID %d", intval($_GET['deleteSuccess']));
}

if (isset($_GET['deleteError'])) {
    $errors = sprintf("There was an error while deleting field with ID %d",$_GET['deleteError']);
}

try {
    $fieldObj = new Field($PDO);
    $fieldsData = $fieldObj->getAll();
    $fieldsCount = $fieldObj->getCount();
    if (count($fieldsData) > 0) {
        foreach ($fieldsData as &$field) {
            $field['tCount'] = $fieldObj->getTranslationsCount($field['FID']);
        }
    }
} catch (\Exception $e){
    $logger->alert($e->getMessage());
}

try {
    $templateRenderer = TemplateRenderer::getInstance();
    $templateRenderer->render('header', ['title' => 'List Fields',
        'success' => $success, 'successMessage' => $successMessage, 'errors' => $errors]);
    $templateRenderer->render('leftMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('topMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('listFields', ['fieldsData' => $fieldsData, 'fieldsCount' => $fieldsCount]);
    $templateRenderer->render('footer');
} catch (\Exception $e){
    $logger->warning($e->getMessage());
}