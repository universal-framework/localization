<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 20.5.2018 г.
 * Time: 13:45 ч.
 */


require_once('_main.php');

if (isset($_GET['list'])) {
    include_once ('listPages.php');
} else if (isset($_GET['create'])) {
    include_once ('createPage.php');
} else if (isset($_GET['update'])) {
    include_once ('updatePage.php');
} else if (isset($_GET['delete'])) {
    include_once ('deletePage.php');
} else if (isset($_GET['overview'])) {
    include_once ('overviewPage.php');
} else {
    header('Location: dashboard.php');
}

?>