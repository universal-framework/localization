<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 17.5.2018 г.
 * Time: 23:23 ч.
 */

require_once('_main.php');

if (isset($_GET['list'])) {
    include_once ('listFields.php');
} else if (isset($_GET['create'])) {
    include_once ('createField.php');
} else if (isset($_GET['update'])) {
    include_once ('updateField.php');
} else if (isset($_GET['delete'])) {
    include_once ('deleteField.php');
} else if (isset($_GET['overview'])) {
    include_once ('overviewField.php');
} else {
    header('Location: dashboard.php');
}