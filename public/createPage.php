<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 20.5.2018 г.
 * Time: 14:34 ч.
 */

use Localization\Page\Page;
use Localization\Utils\PDOWrapper;
use Localization\Utils\TemplateRenderer;
use Localization\Utils\Logger;
use Localization\Utils\Error;
use Localization\Utils\Token;
use Localization\Page\PageTranslator;
use Localization\Entity\Template;
use Localization\Entity\PageField;
use Localization\Entity\Language;
use Localization\Settings;

$PDO = PDOWrapper::getInstance();
$logger = Logger::getInstance();
$error = Error::getInstance();

$errors = null;

if (empty($_SESSION['field_form_token'])) {
    $_SESSION['field_form_token'] = Token::generateToken();
}

if (isset($_POST) && count($_POST) > 0) {


    $language = new Language($PDO);

    $pageID = createPage($_POST, $error, $_SESSION['field_form_token'], $PDO, $language);

    if ($pageID > 0) {
        header('Location: pages.php?overview='.$pageID);
    } else {
        if ($error->isErrorSome()) {
            $errors = $error->fetchAll();
        } else {
            $errors = "Unknow error occured. Please contact the admin.";
        }

        $_SESSION['field_form_token'] = Token::generateToken();
    }
}

function createPage($data, Error $errorObj, $token, \PDO $PDO, Language $langObj)
{
    $fields = [];

    $languages = $langObj->getAll();
    $translations = [];

    if (empty($data['CSRF_TOKEN'])) {
        $errorObj->add("Could not save the form. Please try again!");
        $errorObj->alert("CSRF token is empty");
        return false;
    }

    if (false === hash_equals($token, $data['CSRF_TOKEN'])) {
        $errorObj->add("Could not save the form. Please try again!");
        $errorObj->alert("CSRF token does not match as expected!");
        return false;
    }

    $label = strip_tags(trim($data['label']));
    $desc = strip_tags(trim($data['desc']));
    $hTemplate = intval($data['headerTemp']);
    $mTemplate = intval($data['standardTemp']);
    $fTemplate = intval($data['footerTemp']);

    if (is_array($data['fields']) && count($data['fields']) > 0) {
        foreach ($data['fields'] as $id => $field) {
            if (empty ($field)) {
                continue;
            }
            foreach($languages as $langData) {
                $translations[$id][$langData['LID']] = $data['lang_'.$id.'_'.$langData['LID']];
            }

            $fields[$id] = strip_tags(trim($field));
        }
    }

    try {
        $page = new Page($PDO);
        if ($hTemplate > 0) {
            $hTemplateObj = new Template($PDO);
            $hTemplateObj->load($hTemplate);
        } else {
            $hTemplateObj = null;
        }

        if ($mTemplate > 0) {
            $mTemplateObj = new Template($PDO);
            $mTemplateObj->load($mTemplate);
        } else {
            $mTemplateObj = null;
        }

        if ($fTemplate > 0) {
            $fTemplateObj = new Template($PDO);
            $fTemplateObj->load($fTemplate);
        } else {
            $fTemplateObj = null;
        }

        $newID = $page->save($label,$desc,$hTemplateObj,$mTemplateObj,$fTemplateObj);

        if ($newID > 0 && count ($fields) > 0) {
            $pageFields = new PageField($PDO);
            $pageTrans = new PageTranslator($PDO);
            foreach ($fields as $key => $field) {
                if (!$pageFields->save(['page_id' => $newID, 'field' => $field])) {
                    throw new \Exception("Could not add the pagefield");
                }

                if (!isset($translations[$key]) || count ($translations[$key]) == 0) {
                    continue;
                }

                $langToTranslation = $translations[$key];


                foreach ($langToTranslation as $lang => $translation) {

                    $translation = strip_tags(trim($translation));

                    if (!empty($translation)) {
                        if ($langObj->load($lang)) {
                            $pageTrans->save($page,$langObj, $pageFields, $translation);
                        }
                    }
                }

            }
        }

        return $newID;
    } catch (\Exception $e){
        $errorObj->add($e->getMessage());
        return false;
    }
}

$token = $_SESSION['field_form_token'];

try {
    $template = new Template($PDO);
    $templates = $template->getTemplatesByTempTypes();

    $language = new Language($PDO);
    $languages = $language->getAll();

    $settings = new Settings($PDO);

    $settings = $settings->getSettings();
    if ($settings !== false ) {
        $chosenLang = $settings['mainLangID'];
    } else {
        $chosenLang = 0;
    }

} catch (\Exception $e){
    $logger->alert($e->getMessage());
}

try {
    $templateRenderer = TemplateRenderer::getInstance();
    $templateRenderer->render('header', ['title' => 'Create new page', 'errors' => $errors,
        'includeCSS' => [URL_VENDORS.'flags/css/flag-icon.min.css']]);
    $templateRenderer->render('leftMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('topMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('createPage', ['token' => $token, 'templates' => $templates, 'langs' => $languages,
        'chosenLang' => $chosenLang]);
    $templateRenderer->render('footer', []);
} catch (\Exception $e){
    $logger->warning($e->getMessage());
}