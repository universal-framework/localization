<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 20.5.2018 г.
 * Time: 12:32 ч.
 */

use Localization\Utils\PDOWrapper;
use Localization\Utils\TemplateRenderer;
use Localization\Utils\Logger;
use Localization\Utils\Error;
use Localization\Utils\Token;
use Localization\Entity\Language;

$PDO = PDOWrapper::getInstance();
$logger = Logger::getInstance();
$error = Error::getInstance();


$errors = null;


if ($_GET['update'] >= 0) {
    $updateID = intval($_GET['update']);
} else {
    header('Location: langs.php?list');
}

if (empty($_SESSION['field_form_token'])) {
    $_SESSION['field_form_token'] = Token::generateToken();
}

if (isset($_POST) && count($_POST) > 0) {

    $updated = updateLang($updateID, $_POST, $error, $_SESSION['field_form_token'], $PDO);

    if ($updated) {
        header('Location: langs.php?update='.$updateID);
    } else {
        if (false === $error->isErrorSome()) {
            $errors = "Unknow error occured. Please contact the admin.";
        }

        $_SESSION['field_form_token'] = Token::generateToken();
    }
}

function updateLang($updateID, $data, Error $errorObj, $token, \PDO $PDO)
{
    if (empty($data['CSRF_TOKEN'])) {
        $errorObj->add("Could not save the form. Please try again!");
        $errorObj->alert("CSRF token is empty");
        return false;
    }

    if (false === hash_equals($token, $data['CSRF_TOKEN'])) {
        $errorObj->add("Could not save the form. Please try again!");
        $errorObj->alert("CSRF token does not match as expected!");
        return false;
    }

    $abbv = strip_tags(trim($data['abbv']));
    $desc = strip_tags(trim($data['desc']));
    $code = strip_tags(trim($data['code']));

    try {
        $language = new Language($PDO);
        $updated = $language->update($updateID, ['abbv' => $abbv, 'desc' => $desc, 'code' => $code]);

        return $updated;
    } catch (\Exception $e){
        $errorObj->add($e->getMessage());
        return false;
    }
}

$token = $_SESSION['field_form_token'];

try {
    $langObj = new Language($PDO);
    if ($langObj->load($updateID)) {
        $langData = $langObj->getData();
    } else {
        $error->add("No such language to update!");
    }
} catch (\Exception $e){
    $logger->alert($e->getMessage());
    $error->add("Could not process your request at this time! Please contact the admin");
}

if ($error->isErrorSome()) {
    $errors = $error->fetchAll();
}

try {
    $templateRenderer = TemplateRenderer::getInstance();
    $templateRenderer->render('header', ['title' => (isset($langData['abbv'])) ?
        sprintf('Update language %s', $langData['abbv']) : 'Updating language with no ID...', 'errors' => $errors,
        'includeCSS' => [URL_VENDORS.'flags/css/flag-icon.min.css']]);
    $templateRenderer->render('leftMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('topMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('addLang', ['token' => $token, 'langData' => $langData, 'update' => true]);
    $templateRenderer->render('footer', ['includeJS' => [URL_VENDORS.'select2/js/select2.min.js']]);
} catch (\Exception $e){
    $logger->warning($e->getMessage());
}