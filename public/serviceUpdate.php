<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 27.5.2018 г.
 * Time: 01:56 ч.
 */

include_once '_main.php';

use Localization\Entity\TranslatorService;
use Localization\Utils\PDOWrapper;

if (isset($_POST) && count($_POST) > 0) {

    $SID = intval($_POST['SID']);
    $apiKey = strip_tags(trim($_POST['api_key']));

    $transServ = new TranslatorService(PDOWrapper::getInstance());
    try {
        $transServ->updateService($SID, $apiKey);
        $message = "Successfully updated service with ID $SID";
    } catch (\Exception $e) {
        $message = $e->getMessage();
    }

    $_SESSION['success'] = $message;
}



header ('Location: settings.php');