<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 13.5.2018 г.
 * Time: 12:57 ч.
 */

include_once('../mainloader.php');

if (false === isset($_SESSION['UID']) || false === is_numeric($_SESSION['UID'])) {
    header('Location: login.php');
}