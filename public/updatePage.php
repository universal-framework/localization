<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 21.5.2018 г.
 * Time: 00:00 ч.
 */

use Localization\Entity\Template;
use Localization\Utils\PDOWrapper;
use Localization\Utils\TemplateRenderer;
use Localization\Utils\Logger;
use Localization\Utils\Error;
use Localization\Utils\Token;
use Localization\Page\Page;
use Localization\Entity\PageField;
use Localization\Entity\Language;
use Localization\Page\PageTranslator;
use Localization\Settings;

if ($_GET['update'] >= 0) {
    $updateID = intval($_GET['update']);
} else {
    header('Location: pages.php?list');
}

$PDO = PDOWrapper::getInstance();
$logger = Logger::getInstance();
$error = Error::getInstance();
$success = false;
$successMessage = "";

$errors = null;

if (empty($_SESSION['field_form_token'])) {
    $_SESSION['field_form_token'] = Token::generateToken();
}

$tempInfo = null;

$token = $_SESSION['field_form_token'];


if (isset($_POST) && count($_POST) > 0) {

    $language = new Language($PDO);

    $isUpdated = updatePage($updateID, $_POST, $error, $_SESSION['field_form_token'], $PDO, $logger, $language);

    if ($isUpdated === true) {
        $success = true;
        $successMessage = "Sucessfully updated the page";
    } else {
        if ($error->isErrorSome()) {
            $errors = $error->fetchAll();
        } else {
            $errors = "Unknow error occured. Please contact the admin.";
        }

        $_SESSION['field_form_token'] = Token::generateToken();
    }
}

function updatePage($PID, $data, Error $errorObj, $token, \PDO $PDO, Logger $logger, Language $langObj)
{
    $fields = [];

    $languages = $langObj->getAll();
    $translations = [];

    if (empty($data['CSRF_TOKEN'])) {
        $errorObj->add("Could not save the form. Please try again!");
        $errorObj->alert("CSRF token is empty");
        return false;
    }

    if (false === hash_equals($token, $data['CSRF_TOKEN'])) {
        $errorObj->add("Could not save the form. Please try again!");
        $errorObj->alert("CSRF token does not match as expected!");
        return false;
    }


    $label = strip_tags(trim($data['label']));
    $desc = strip_tags(trim($data['desc']));
    $hTemplate = intval($data['headerTemp']);
    $mTemplate = intval($data['standardTemp']);
    $fTemplate = intval($data['footerTemp']);

    if (is_array($data['fields']) && count($data['fields']) > 0) {
        foreach ($data['fields'] as $fieldID => $fieldLabel) {
            foreach($languages as $langData) {
                $translations[$fieldID][$langData['LID']] = $data['lang_'.$fieldID.'_'.$langData['LID']];
            }
            $fields[$fieldID] = strip_tags(trim($fieldLabel));
        }
    }

    try {
        $page = new Page($PDO);

        if ($hTemplate > 0) {
            $hTemplateObj = new Template($PDO);
            $hTemplateObj->load($hTemplate);
        } else {
            $hTemplateObj = null;
        }

        if ($mTemplate > 0) {
            $mTemplateObj = new Template($PDO);
            $mTemplateObj->load($mTemplate);
        } else {
            $mTemplateObj = null;
        }

        if ($fTemplate > 0) {
            $fTemplateObj = new Template($PDO);
            $fTemplateObj->load($fTemplate);
        } else {
            $fTemplateObj = null;
        }

        $updated = $page->update($PID, ['label' => $label, 'desc' => $desc, 'hTemplate' => $hTemplateObj,
            'mTemplate' => $mTemplateObj, 'fTemplate' => $fTemplateObj]);

        if ($updated) {
//
            $pageFields = new PageField($PDO);
            $pageTranslator = new PageTranslator($PDO);

            if (count ($fields) > 0) {
                $existingFields = $pageFields->getExistingFields($PID, $fields);
                $newRecords = array_diff($fields, $existingFields);
            } else {
                $existingFields = [];
                $newRecords = [];
            }

            $pageTranslator->deleteAllTransInPageExcept($PID, ['except' => array_keys($existingFields)]);
            $pageFields->deleteAllInPageExcept($PID, ['except' => array_keys($existingFields)]);

            if (count($newRecords) > 0) {
                foreach($newRecords as $key => $record) {
                    if (!$pageFields->save(['page_id' => $PID, 'field' => $record])) {
                        throw new \Exception("Could not add the pagefield");
                    }

                    if (!isset($translations[$key]) || count ($translations[$key]) == 0) {
                        continue;
                    }

                    $langToTranslation = $translations[$key];


                    foreach ($langToTranslation as $lang => $translation) {

                        $translation = strip_tags(trim($translation));

                        if (!empty($translation)) {
                            if ($langObj->load($lang)) {
                                $pageTranslator->save($page,$langObj, $pageFields, $translation);
                            }
                        }
                    }
                }
            }

            if (count($existingFields) > 0) {
                foreach($existingFields as $key => $record) {

                    if (!$pageFields->load($key, ['page_id' => $PID])) {
                        throw new \Exception(sprintf("Could not load field with ID %d", $key));
                    }

                    if (!isset($translations[$key]) || count ($translations[$key]) == 0) {
                        continue;
                    }

                    $langToTranslation = $translations[$key];


                    foreach ($langToTranslation as $lang => $translation) {

                        $translation = strip_tags(trim($translation));

                        if (!empty($translation)) {
                            if ($langObj->load($lang)) {
                                $pageTranslator->save($page,$langObj, $pageFields, $translation);
                            }
                        }
                    }
                }
            }
        }

        return true;


    } catch (\Exception $e){
        $errorObj->add($e->getMessage());
        return false;
    }
}

try {

    $page = new Page($PDO);
    if ($page->load($updateID)) {
        $pageData = $page->getData();

        $template = new Template($PDO);
        $templates = $template->getTemplatesByTempTypes();

        $pageField = new PageField($PDO);
        $extraFields = $pageField->loadAllFields($updateID);

        $pageTranslator = new PageTranslator($PDO);

        foreach ($extraFields as &$fieldData) {
            $pageField->load($fieldData['FID'], ['page_id' => $updateID]);
            $fieldData['translations'] = $pageTranslator->getTranslations($page, $pageField);
        }

        $language = new Language($PDO);
        $languages = $language->getAll();

        $settings = new Settings($PDO);

        $settings = $settings->getSettings();
        if ($settings !== false ) {
            $chosenLang = $settings['mainLangID'];
        } else {
            $chosenLang = 0;
        }

    } else {
        $error->add(sprintf('Could not find a page with ID %d', $updateID));
    }


} catch (\Exception $e){
    $logger->alert($e->getMessage());
    $error->add(Error::$standardMessage);
}

if ($error->isErrorSome()) {
    $errors = $error->fetchAll();
}

try {
    $templateRenderer = TemplateRenderer::getInstance();
    $templateRenderer->render('header', ['title' => sprintf('Update page %s', $pageData['label']),
            'errors' => $errors, 'success' => $success,
            'successMessage' => $successMessage,
            'includeCSS' => [URL_VENDORS.'flags/css/flag-icon.min.css']]
    );
    $templateRenderer->render('leftMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('topMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('createPage', ['update' => true,'pageData' => $pageData, 'templates' => $templates,
        'token' => $token, 'extraFields' => $extraFields, 'langs' => $languages, 'chosenLang' => $chosenLang]);
    $templateRenderer->render('footer', []);
} catch (\Exception $e){
    $logger->warning($e->getMessage());
}