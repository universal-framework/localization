(function($,sr){
    // debouncing function from John Hann
    // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
    var debounce = function (func, threshold, execAsap) {
        var timeout;

        return function debounced () {
            var obj = this, args = arguments;
            function delayed () {
                if (!execAsap)
                    func.apply(obj, args);
                timeout = null;
            }

            if (timeout)
                clearTimeout(timeout);
            else if (execAsap)
                func.apply(obj, args);

            timeout = setTimeout(delayed, threshold || 100);
        };
    };

    // smartresize
    jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');

/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var CURRENT_URL = window.location.href.split('#')[0].split('?')[0],
    $BODY = $('body'),
    $MENU_TOGGLE = $('#menu_toggle'),
    $SIDEBAR_MENU = $('#sidebar-menu'),
    $SIDEBAR_FOOTER = $('.sidebar-footer'),
    $LEFT_COL = $('.left_col'),
    $RIGHT_COL = $('.right_col'),
    $NAV_MENU = $('.nav_menu'),
    $DROP_DOWN = $('.dropdownable'),
    $FOOTER = $('footer');



// Sidebar
function init_sidebar() {
// TODO: This is some kind of easy fix, maybe we can improve this
    var setContentHeight = function () {
        // reset height
        $RIGHT_COL.css('min-height', $(window).height());

        var bodyHeight = $BODY.outerHeight(),
            footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(),
            leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
            contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

        // normalize content
        contentHeight -= $NAV_MENU.height() + footerHeight;

        $RIGHT_COL.css('min-height', contentHeight);
    };

    $SIDEBAR_MENU.find('a').on('click', function(ev) {
        var $li = $(this).parent();

        if ($li.is('.active')) {
            $li.removeClass('active active-sm');
            $('ul:first', $li).slideUp(function() {
                setContentHeight();
            });
        } else {
            // prevent closing menu if we are on child menu
            if (!$li.parent().is('.child_menu')) {
                $SIDEBAR_MENU.find('li').removeClass('active active-sm');
                $SIDEBAR_MENU.find('li ul').slideUp();
            }else
            {
                if ( $BODY.is( ".nav-sm" ) )
                {
                    $SIDEBAR_MENU.find( "li" ).removeClass( "active active-sm" );
                    $SIDEBAR_MENU.find( "li ul" ).slideUp();
                }
            }
            $li.addClass('active');

            $('ul:first', $li).slideDown(function() {
                setContentHeight();
            });
        }
    });

// toggle small or large menu
    $MENU_TOGGLE.on('click', function() {

        if ($BODY.hasClass('nav-md')) {
            $SIDEBAR_MENU.find('li.active ul').hide();
            $SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
        } else {
            $SIDEBAR_MENU.find('li.active-sm ul').show();
            $SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
        }

        $BODY.toggleClass('nav-md nav-sm');

        setContentHeight();
    });

    // check active menu
    $SIDEBAR_MENU.find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('current-page');

    $SIDEBAR_MENU.find('a').filter(function () {
        return this.href == CURRENT_URL;
    }).parent('li').addClass('current-page').parents('ul').slideDown(function() {
        setContentHeight();
    }).parent().addClass('active');

    // recompute content when resizing
    $(window).smartresize(function(){
        setContentHeight();
    });

    setContentHeight();

    // fixed sidebar
    if ($.fn.mCustomScrollbar) {
        $('.menu_fixed').mCustomScrollbar({
            autoHideScrollbar: true,
            theme: 'minimal',
            mouseWheel:{ preventDefault: true }
        });
    }
};
// /Sidebar

// Dropdown menu

function init_dropdownmenu() {
    $DROP_DOWN.find('a').on('click', function(){
        $(this).parent().find('.dropdown-menu').toggle();
    });
}

function formatState (state) {
    var $state = $(
        '<span title="' + state.text + '" class="flag-icon flag-icon-' + state.text.toLowerCase() + '"></span>'
        + '<span style="margin-left:2px">' + state.text + '</span>'
    );
    return $state;
};


// Panel toolbox
$(document).ready(function() {

    var newFieldsIDCounter = 0;

    $('.collapse-link').on('click', function() {
        var $BOX_PANEL = $(this).closest('.x_panel'),
            $ICON = $(this).find('i'),
            $BOX_CONTENT = $BOX_PANEL.find('.x_content');

        // fix for some div with hardcoded fix class
        if ($BOX_PANEL.attr('style')) {
            $BOX_CONTENT.slideToggle(200, function(){
                $BOX_PANEL.removeAttr('style');
            });
        } else {
            $BOX_CONTENT.slideToggle(200);
            $BOX_PANEL.css('height', 'auto');
        }

        $ICON.toggleClass('fa-chevron-up fa-chevron-down');
    });

    $('.close-link').click(function () {
        var $BOX_PANEL = $(this).closest('.x_panel');

        $BOX_PANEL.remove();
    });

    $('.fa-trash').click(function(e){
        e.preventDefault();
        if (confirm("Are you sure you want to delete the item?")) {
            window.location = $(this).attr('href');
        }
    });

    $('.delete-templates').click(function(e){
        e.preventDefault();
        if (confirm("Are you sure you want to delete ALL THE TEMPLATES?")) {
            window.location = $(this).attr('href');
        }
    });

    $('#abbv').on('keyup', function(){
        $(this).val($(this).val().toUpperCase());

        var lowerCased = $(this).val().toLowerCase();

        var parentEl = $(this).parent().parent();
        var spanEl = $(parentEl).find('.flag-icon');

        if (spanEl.length == 0) {

            spanEl = document.createElement('span');
            spanEl.title = lowerCased;
            spanEl.className = 'flag-icon flag-icon-' + lowerCased;

            parentEl[0].prepend(spanEl);
        } else {
            $(spanEl[0]).removeClass().addClass('flag-icon flag-icon-' + lowerCased);
        }
    });

    $(document).on('click','.option_field_icon:not(.fa-sync,.fa-arrow-up)', function(){

        var parentRow = $(this).closest('.row');
        var lastID = $(this).parent().find('.option_field').attr('id').split('field_')[1];
        var fieldBlock = $(parentRow).find('.trans_' + lastID);

        if ($(this).hasClass('fa-plus')) {

            newFieldsIDCounter = parseInt(lastID, 10) + 1;

            /** ADD THE NEW FIELD IN THE FORM */

            var parentEl = $(this).parent().parent();
            var controlEls = $(parentEl[0]).find('.controls');
            var lastControlEl = controlEls[controlEls.length - 1];

            var lastPlusIcon = $(lastControlEl).find('.option_field_icon');
            var newTransField = $(lastControlEl).clone();

            lastPlusIcon.removeClass('fa-plus').addClass('fa-times');

            newTransField.find('input').attr('name',"fields[]");
            newTransField.find('input').attr('id', 'field_' + newFieldsIDCounter);
            newTransField.find('input').val("");

            parentEl.append(newTransField);

            /** ADD TRANSLATION BLOCK TO THAT FIELD */

            var newField = fieldBlock.clone();

            newField.removeClass('trans_' + (newFieldsIDCounter - 1))
                .addClass('trans_' + newFieldsIDCounter);
            newField.find('.fieldLabel').text("");
            var inputTexts = newField.find(':input:text');
            inputTexts.each(function(){
                this.name = 'lang_' + newFieldsIDCounter + this.name.split('lang_' + (newFieldsIDCounter - 1))[1];
                this.val = "";
            });
            //newField.find('input:text').val('').name('lang_')

            parentRow.append(newField);
        } else {

            fieldBlock.remove();

            $(this).parent().remove();
        }
    });

    $(document).on('keyup', '.option_field', function(){
        var parentRow = $(this).closest('.row');
        var id = $(this).attr('id').split('field_')[1];
        var fieldBlock = $(parentRow).find('.trans_' + id);

        fieldBlock.find('.fieldLabel').text($(this).val());


    });

    $('#createPageSave').on('click', function(e){

        e.preventDefault();

        var form = $(this).closest('form');
        var parentRow = $(this).closest('.row');
        var fields = form.find('input[name="fields[]"]');
        var me = this;

        if (fields.length > 0) {
            fields.each(function(){
                var fieldThis = this;
                var id = $(fieldThis).attr('id').split('field_')[1];
                $(fieldThis).attr('name', 'fields[' + id +']');

                var transContainer = parentRow.find('.trans_' + id);

                var inputTexts = transContainer.find(':input:text');
                inputTexts.each(function(){
                    var clonedInput = $(this).clone();
                    clonedInput.attr('type', 'hidden');
                    $(form).append(clonedInput);
                });

            });
        }

        form.submit();
    });

    $('.trans_icon').on('click', function(){
        var parentContainer = $(this).closest('.row'),
            chosenLangID = $(parentContainer).find('input[name="chosen_lang"]').val();

        let currField = $(this).parent().find(':input')[0],
            currFieldLang = currField.name.split('service_trans_')[1];

        var textToTrans = $(parentContainer).find('input[name="lang_' + chosenLangID + '"]').val();



        $.post( "translate.php", {LID: currFieldLang, text: textToTrans})
            .done(function( data ) {
                var json = $.parseJSON(data);
                $(currField).val(json[currFieldLang]);
            })
            .fail(function(){
                alert ("ERROR!");
            });
    });

    $('.trans_icon_page').on('click', function(){
        var parentContainer = $(this).closest('.row'),
            chosenLangID = $(parentContainer).find('input[name="chosen_lang"]').val();

        let currField = $(this).parent().find(':input')[0],
            currFieldFIDAndLang = currField.name.split('service_trans_')[1].split('_'),
            currFieldFID = currFieldFIDAndLang[0],
            currFieldLang = currFieldFIDAndLang[1];

        var textToTrans = $(parentContainer).find('input[name="lang_' + currFieldFID +'_' + chosenLangID + '"]').val();

        $.post( "translate.php", {LID: currFieldLang, text: textToTrans})
            .done(function( data ) {
                var json = $.parseJSON(data);
                $(currField).val(json[currFieldLang]);
            })
            .fail(function(){
                alert ("ERROR!");
            });
    });

    $('.trans_apply').on('click', function(){
        var parentContainer = $(this).closest('.row'),
            currField = $(this).parent().find(':input')[0],
            currentFieldVal = $(currField).val(),
            lang = currField.name.split('service_trans_')[1],
            textApplyField = $(parentContainer).find('input[name="lang_' + lang +'"]');

        $(textApplyField).val(currentFieldVal);
    });

    $('.trans_apply_page').on('click', function(){
        var parentContainer = $(this).closest('.row'),
            currField = $(this).parent().find(':input')[0],
            currentFieldVal = $(currField).val(),
            FIDAndLang = currField.name.split('service_trans_')[1].split('_'),
            FID = FIDAndLang[0],
            lang = FIDAndLang[1],
            textApplyField = $(parentContainer).find('input[name="lang_' + FID +'_' + lang +'"]');

        $(textApplyField).val(currentFieldVal);
    });

    $('#transbtn').on('click', function(e){

        e.preventDefault();

        let parentContainer = $(this).closest('.row');
        var chosenLangID = $(parentContainer).find('input[name="chosen_lang"]').val();

        var textToTrans = $(parentContainer).find('input[name="lang_' + chosenLangID + '"]').val();

        $.post( "translate.php", {LID: "all", text: textToTrans})
            .done(function( data ) {
                var json = $.parseJSON(data);
                for (var lang in json) {
                    $(parentContainer).find('input[name="service_trans_' + lang + '"]').val(json[lang]);
                }

                $('#subsbtn').attr('disabled',false);
            })
            .fail(function(){
                alert ("ERROR!");
            });
    });

    $('.transbtn_page').on('click', function(e){

        e.preventDefault();
        let parentContainer = $(this).closest('.row');
        var chosenLangID = $(parentContainer).find('input[name="chosen_lang"]').val();
        let templateContainer = $(this).closest('.x_content');
        var firstFieldName = $(templateContainer.find(':input:text')[0]).attr('name'),
            FIDAndLang = firstFieldName.split('lang_')[1].split('_');
        let FID = FIDAndLang[0];

        var textToTrans = $(parentContainer).find('input[name="lang_'+ FID +'_' + chosenLangID + '"]').val();

        $.post( "translate.php", {LID: "all", text: textToTrans})
            .done(function( data ) {
                var json = $.parseJSON(data);
                for (var lang in json) {
                    $(parentContainer).find('input[name="service_trans_' + FID +'_' + lang + '"]').val(json[lang]);
                }
                $(templateContainer).find('.subsbtn_page').attr('disabled',false);
            })
            .fail(function(){
                alert ("ERROR!");
            });
    });

    $('#subsbtn').on('click', function(e){

        e.preventDefault();

        var parentContainer = $(this).closest('.row');
        var autoTransContainers = $(parentContainer).find('.trans_fields');

        autoTransContainers.each(function(){
            var transField = $(this).find(':input:text'),
                transValue = transField.val(),
                transName = transField.attr('name'),
                langID = transName.split('service_trans_')[1];


            if (transValue != "") {
                var closestRow = $(this).closest('.row');
                closestRow.find('input[name="lang_' + langID + '"]').val(transValue);
            }
        });
    });

    $('.subsbtn_page').on('click', function(e){

        e.preventDefault();

        var parentContainer = $(this).closest('.x_content');
        var autoTransContainers = $(parentContainer).find('.trans_fields');

        autoTransContainers.each(function(){
            var transField = $(this).find(':input:text'),
                transValue = transField.val(),
                transName = transField.attr('name'),
                FIDAndlang = transName.split('service_trans_')[1].split('_'),
                FID = FIDAndlang[0],
                lang = FIDAndlang[1];


            if (transValue != "") {
                var closestRow = $(this).closest('.x_content');
                closestRow.find('input[name="lang_' + FID +'_' + lang + '"]').val(transValue);
            }
        });
    });

    $('#transAll').on('click', function(e){

        e.preventDefault();

        var parentContainer = $(this).closest('.row'),
            lang_id = $(parentContainer).find(':input[name="lang_id"]').val();

        $.post( "translate_all.php", {LID: lang_id})
            .done(function( data ) {
                var json = $.parseJSON(data);

                if (json.success === true) {
                    alert("Success! Successfully translated all the fields in the language");
                } else {
                    alert("Error! Message: " + json.message);
                }
            })
            .fail(function(){
                alert ("ERROR!Please contact the administrator of your system.");
            });
    });
});
// /Panel toolbox


$(document).ready(function() {

    init_sidebar();
    init_dropdownmenu();
    if(jQuery.fn.select2) {
        $('.fields-combo').select2();
        $('.fields-combo-langs').select2({
            templateResult: formatState,
            templateSelection: formatState
        });
    }


});

