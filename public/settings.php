<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 26.5.2018 г.
 * Time: 15:59 ч.
 */

require_once('_main.php');

use Localization\Utils\PDOWrapper;
use Localization\Utils\TemplateRenderer;
use Localization\Utils\Logger;
use Localization\Utils\Error;
use Localization\Entity\Language;
use Localization\Utils\Token;
use Localization\Entity\TranslatorService;
use Localization\Settings;

$PDO = PDOWrapper::getInstance();
$logger = Logger::getInstance();
$error = Error::getInstance();
$errors = null;

if (empty($_SESSION['field_form_token'])) {
    $_SESSION['field_form_token'] = Token::generateToken();
}

$token = $_SESSION['field_form_token'];

$successMessage = "";

if (isset($_POST) && count($_POST) > 0) {

    $langObj = new Language($PDO);
    $transService = new TranslatorService($PDO);
    $settings = new Settings($PDO);

    $updated = updateSettings($_POST, $error, $_SESSION['field_form_token'], $PDO, $langObj, $transService, $settings);

    if ($updated) {
        $successMessage = "Successfully updated settings!";
    } else {
        if ($error->isErrorSome()) {
            $errors = $error->fetchAll();
        } else {
            $errors = "Unknow error occured. Please contact the admin.";
        }

        $_SESSION['field_form_token'] = Token::generateToken();
    }
}

function updateSettings($data, Error $errorObj, $token, \PDO $PDO, Language $langObj, TranslatorService $transService,
        Settings $settings)
{
    if (empty($data['CSRF_TOKEN'])) {
        $errorObj->add("Could not save the form. Please try again!");
        $errorObj->alert("CSRF token is empty");
        return false;
    }

    if (false === hash_equals($token, $data['CSRF_TOKEN'])) {
        $errorObj->add("Could not save the form. Please try again!");
        $errorObj->alert("CSRF token does not match as expected!");
        return false;
    }

    $mainLangID = intval($data['mainLang']);
    $SID = intval($data['transAPI']);
    $transType = $data['massTransType'];

    $updateArr = [];

    try {
        if ($langObj->load($mainLangID)) {
            $updateArr['mainLang'] = $mainLangID;
        } else {
            throw new \Exception("No such language!");
        }

        if ($transService->load($SID)) {
            $updateArr['SID'] = $SID;
        } else {
            throw new \Exception("No such service!");
        }

        if (!empty($transType)) {
            if (in_array($transType, Settings::$transTypes)){
                $updateArr['transType'] = $transType;
            } else {
                $updateArr['transType'] = null;
            }
        } else {
            $updateArr['transType'] = null;
        }

        if (count ($updateArr) > 0) {
            $settings->save($updateArr);
        }

        return true;
    } catch (\Exception $e){
        $errorObj->add($e->getMessage());
        return false;
    }
}

if (isset($_SESSION['success'])) {
    $successMessage = $_SESSION['success'];
    unset($_SESSION['success']);
}

try {
    $language = new Language($PDO);
    $languages = $language->getAll();

    $transService = new TranslatorService($PDO);
    $services = $transService->getAllServices();

    $bingIndex = false;

    if ($services !== false && count($services) > 0) {
        foreach ($services as $key => $transData) {
            if ($transData['sName'] == 'bing') {
                $bingIndex = $key;
                break;
            }
        }
    }

    $settingsObj = new Settings($PDO);
    $settings = $settingsObj->getSettings();

} catch (\Exception $e){
    $logger->alert($e->getMessage());
}

try {
    $templateRenderer = TemplateRenderer::getInstance();
    $templateRenderer->render('header', ['title' => "Settings", 'errors' => $errors, 'successMessage' => $successMessage,
            'includeCSS' => [URL_VENDORS.'flags/css/flag-icon.min.css', URL_VENDORS.'select2/css/select2.min.css']]
    );
    $templateRenderer->render('leftMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('topMenu', ['userName' => $_SESSION['username']]);
    $templateRenderer->render('settings', ['langs' => $languages, 'token' => $token, 'services' => $services,
        'settings' => $settings, 'bingIndex' => $bingIndex]);
    $templateRenderer->render('footer', ['includeJS' => [URL_VENDORS.'select2/js/select2.min.js']]);
} catch (\Exception $e){
    $logger->warning($e->getMessage());
}