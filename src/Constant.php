<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 30.4.2018 г.
 * Time: 22:16 ч.
 */

namespace Localization;


class Constant
{
    static $redBGCLI = "\033[41m";
    static $greenBGCLI = "\033[42m";
    static $yellowBGCLI = "\033[43m";
    static $whiteTextCLI = "\033[1;37m";
    static $colorEndCLI = "\033[0m";
}