<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 20.6.2018 г.
 * Time: 23:44 ч.
 */

namespace Localization\Translator;


class TransContainer
{
    private $translations = [];

    public function __construct(array $translations)
    {
        $this->translations = $translations;
    }

    public function get($fieldName, $type = null, $escape = true)
    {
        if ($type !== null && !in_array($type, ['header', 'main', 'footer', 'extra'])) {
            throw new \Exception("$type does not exist. Field $fieldName could not be fetched");
        }

        if ($type !== null) {
            if (!isset($this->translations[$type][$fieldName])) {
                return "";
            } else {
                $returnVal = $this->translations[$type][$fieldName];
            }
        } else {
            if (isset($this->translations['header'][$fieldName])) {
                $returnVal = $this->translations['header'][$fieldName];
            } elseif (isset($this->translations['main'][$fieldName])) {
                $returnVal = $this->translations['main'][$fieldName];
            } elseif (isset($this->translations['footer'][$fieldName])) {
                $returnVal = $this->translations['footer'][$fieldName];
            } elseif (isset($this->translations['extra'][$fieldName])) {
                $returnVal = $this->translations['extra'][$fieldName];
            } else {
                return "";
            }
        }

        if ($escape) {
            return htmlentities($returnVal);
        } else {
            return $returnVal;
        }
    }


}