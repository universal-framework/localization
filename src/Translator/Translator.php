<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 4.5.2018 г.
 * Time: 21:46 ч.
 */

namespace Localization\Translator;


use Localization\Entity\Field;
use Localization\Entity\Language;

class Translator
{
    private $PDO = null;
    private $field = null;
    private $lang = null;
    private $translation;
    private $force = false;

    public function __construct(\PDO $PDO)
    {
        $this->PDO = $PDO;
    }

    private function validateFieldAngLang()
    {
        if (empty($this->field->getId())) {
            throw new \Exception("Field ID is empty!");
        }

        if (empty($this->lang->getId())) {
            throw new \Exception("Lang ID is empty!");
        }
    }

    public function save(Field $field, Language $lang, $translation, $force = false)
    {
        $this->field = $field;
        $this->lang = $lang;
        $this->translation = $translation;
        $this->force = $force;

        $this->validateFieldAngLang();

        $fieldID = $this->field->getId();
        $langID = $this->lang->getId();
        $tableToUpdate = $this->getTableByTransLength();
        $deleteOppositeRecord = false;
        $oppositeTable = "";

        if ($this->force === false) {
            $existentTranslation = $this->load($field, $lang);

            if (is_array($existentTranslation) && count($existentTranslation) > 0) {
                throw new \Exception(
                    sprintf("Translation already exists in the database under the translation %s",
                        $existentTranslation[0]['translation'])
                );
            }
        } else {
            // We save translations in two tables, prefixed _short and _long. Short is for variables up to 255 chars
            // long is > 255. This will force deletion of the records in opposite table.
            $deleteOppositeRecord = true;
            $oppositeTable = $this->getOppositeTable($tableToUpdate);
        }

        if ($this->force === true) {
            $onDuplicateKey = ' ON DUPLICATE KEY UPDATE translation = :trans';
        } else {
            $onDuplicateKey = '';
        }

        $this->PDO->beginTransaction();

        $sth = $this->PDO->prepare(
            sprintf(
                "INSERT INTO %s SET FID = :fid, LID = :lid, translation = :trans %s",
                MAIN_DB.'.'.$tableToUpdate,
                $onDuplicateKey
            )
        );
        $sth->bindParam(':fid', $fieldID, \PDO::PARAM_INT);
        $sth->bindParam(':lid', $langID, \PDO::PARAM_INT);
        $sth->bindParam(':trans', $this->translation);

        if (!$sth->execute()) {
            $this->PDO->rollBack();
            throw new \Exception($sth->errorInfo()[2]);
        }

        if ($deleteOppositeRecord) {

            $sth = $this->PDO->prepare(
                sprintf("DELETE FROM %s WHERE FID = :fid AND LID = :lid", MAIN_DB . '.' . $oppositeTable)
            );

            $sth->bindParam(':fid', $fieldID, \PDO::PARAM_INT);
            $sth->bindParam(':lid', $langID, \PDO::PARAM_INT);

            if (!$sth->execute()) {
                $this->PDO->rollBack();
                throw new \Exception($sth->errorInfo()[2]);
            }

        }

        $this->PDO->commit();

        return true;
    }

    private function getTableByTransLength()
    {
        if (mb_strlen($this->translation) <= 255) {
            return 'fields_languages_short';
        } else {
            return 'fields_languages_long';
        }
    }

    private function load(Field $field, Language $lang)
    {
        $fieldID = $field->getId();
        $langID = $lang->getId();

        $sth = $this->PDO->prepare("
                            (
                                SELECT
                                    *
                                FROM
                                  ".MAIN_DB.".fields_languages_short AS fls
                                WHERE
                                  fls.FID = :fid AND fls.LID = :lid
                            ) UNION (
                                SELECT
                                    *
                                FROM
                                    ".MAIN_DB.".fields_languages_long AS fll
                                WHERE
                                  fll.FID = :fid AND fll.LID = :lid
                            )
        ");

        $sth->bindParam(':fid', $fieldID, \PDO::PARAM_INT);
        $sth->bindParam(':lid', $langID, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        return $sth->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getTranslations(Field $field)
    {
        $mapIDToTranslation = [];
        $fieldID = $field->getId();

        $sth = $this->PDO->prepare("
                            (
                                SELECT
                                    *
                                FROM
                                  ".MAIN_DB.".fields_languages_short AS fls
                                WHERE
                                  fls.FID = :fid
                            ) UNION (
                                SELECT
                                    *
                                FROM
                                    ".MAIN_DB.".fields_languages_long AS fll
                                WHERE
                                  fll.FID = :fid
                            )
        ");

        $sth->bindParam(':fid', $fieldID, \PDO::PARAM_INT);
        $sth->bindParam(':lid', $langID, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return [];
        }
        foreach($data as $transData) {
            $mapIDToTranslation[$transData['LID']] = $transData['translation'];
        }


        return $mapIDToTranslation;
    }

    private function getOppositeTable($tableName)
    {
        if ($tableName == 'fields_languages_short') {
            return 'fields_languages_long';
        } else {
            return 'fields_languages_short';
        }
    }

    public function update($id, array $data)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        $this->PDO->query(sprintf("USE %s;", MAIN_DB));

        $sth = $this->PDO->prepare(sprintf("DELETE fls.*, fll.*
                                    FROM %s.fields_languages_short AS fls
                                    LEFT JOIN %s.fields_languages_long AS fll ON (fll.FID = fls.FID)
                                    WHERE fls.FID = :fid",MAIN_DB, MAIN_DB));

        $sth->bindParam(':fid', $id, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        return true;
    }

    public function deleteByLID($lid)
    {
        $this->PDO->query(sprintf("USE %s;", MAIN_DB));

        $sth = $this->PDO->prepare(sprintf("DELETE fls.*, fll.*
                                    FROM %s.fields_languages_short AS fls
                                    LEFT JOIN %s.fields_languages_long AS fll ON (fll.FID = fls.FID AND fll.LID = fls.LID)
                                    WHERE fls.LID = :lid",MAIN_DB, MAIN_DB));

        $sth->bindParam(':lid', $lid, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        return true;
    }

    public function getCount()
    {
        $sth = $this->PDO->prepare("
                              SELECT COUNT(*) as transCount
                                  FROM (
                                    (
                                        SELECT
                                            *
                                        FROM
                                          ".MAIN_DB.".fields_languages_short AS fls
                                    ) UNION (
                                        SELECT
                                            *
                                        FROM
                                            ".MAIN_DB.".fields_languages_long AS fll
                                    )
                                  ) AS final
        ");

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return 0;
        }

        return $data[0]['transCount'];
    }

    /**
     *
     *
     * @param $LID
     * @param $sourceLID
     * @return array
     * @throws \Exception
     */

    public function getAllEmptyTranslations($LID, $sourceLID, $transType)
    {
        if ($transType == 'fillempty') {
            $fidQueryShort = "SELECT
                                        FID
                                    FROM
                                      ".MAIN_DB.".fields_languages_short AS fls
                                    WHERE
                                      fls.LID = :lid AND fls.translation IS NOT NULL AND fls.translation <> \"\"";
            $fidQueryLong = "SELECT
                                            fllMain.FID
                                        FROM
                                            ".MAIN_DB.".fields_languages_long AS fll
                                        WHERE
                                          fll.LID = :lid AND fll.translation IS NOT NULL AND fll.translation <> \"\"";
        } else {
            $fidQueryShort = "-1";
            $fidQueryLong = "-1";
        }
        $sth = $this->PDO->prepare("

                               SELECT *, 'field' AS transType FROM ".MAIN_DB.".fields_languages_short AS flsMain

                                WHERE
                                    flsMain.FID NOT IN($fidQueryShort)
                                AND
                                    flsMain.LID = :srcLID AND flsMain.translation IS NOT NULL AND flsMain.translation <> \"\"

                                UNION

                                SELECT *, 'field' AS transType FROM ".MAIN_DB.".fields_languages_long AS fllMain

                                WHERE fllMain.FID NOT IN($fidQueryLong)
                                AND
                                    fllMain.LID = :srcLID AND fllMain.translation IS NOT NULL AND fllMain.translation <> \"\"
        ");

        $sth->bindParam(':lid', $LID, \PDO::PARAM_INT);
        $sth->bindParam(':srcLID', $sourceLID, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return [];
        }

        return $data;
    }
}