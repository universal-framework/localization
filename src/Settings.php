<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 26.5.2018 г.
 * Time: 22:42 ч.
 */

namespace Localization;


class Settings
{
    /**
     * @var \PDO
     */
    private $PDO;
    private $settings = [];
    private $table = 'settings';
    public static $transTypes = ['fillempty', 'overwrite'];

    public function __construct(\PDO $PDO)
    {
        $this->PDO = $PDO;
    }

    public function getSettings()
    {
        if (count ($this->settings) > 0) {
            return $this->settings;
        }

        $sth = $this->PDO->prepare(sprintf('SELECT * FROM %s LIMIT 1;', MAIN_DB.'.'.$this->table));

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            $this->settings = [];
        } else {
            $this->settings= $data[0];
        }

        return $this->settings;
    }

    public function save(array $saveData)
    {
        $SID = $saveData['SID'];
        $mainLangID = $saveData['mainLang'];
        $transType = $saveData['transType'];

        $count = $this->getCount();

        if ($count > 0) {
            $sth = $this->PDO->prepare(sprintf('UPDATE %s SET chosenSID = :sid, mainLangID = :mLID, transType = :transType', MAIN_DB.'.'.$this->table));
        } else {
            $sth = $this->PDO->prepare(sprintf('INSERT INTO %s SET chosenSID = :sid, mainLangID = :mLID, transType = :transType', MAIN_DB.'.'.$this->table));
        }

        $sth->bindParam(':sid', $SID, \PDO::PARAM_INT);
        $sth->bindParam(':mLID', $mainLangID, \PDO::PARAM_INT);
        $sth->bindParam(':transType', $transType, \PDO::PARAM_STR);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $this->settings = ['chosenSID' => $SID, 'mainLangID' => $mainLangID, 'transType' => $transType];

        return true;
    }

    public function getCount()
    {
        $sth = $this->PDO->prepare(sprintf('SELECT * FROM %s LIMIT 1;', MAIN_DB.'.'.$this->table));

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) > 0) {
            return 1;
        } else {
            return 0;
        }
    }
}