<?php

/**
 * Created by PhpStorm.
 * User: Home
 * Date: 26.5.2018 г.
 * Time: 18:24 ч.
 */

namespace Localization\Automation;

interface IApi
{
    public function makeTranslationRequest(array $from, array $to, $text);
    public function getResult();
}