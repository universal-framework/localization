<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 26.5.2018 г.
 * Time: 18:28 ч.
 */

namespace Localization\Automation;

use Localization\Automation\IApi;
use Localization\Utils\QueryStringBuilder;

class Bing implements IApi
{
    private $host = 'https://api.cognitive.microsofttranslator.com/translate';
    private $versionKey = 'api-version';
    private $version = '3.0';
    private $key;
    private $text = "";
    private $response;

    public function __construct($key, QueryStringBuilder $qsb)
    {
        if (empty($key)) {
            throw new \Exception("No bing translator key set! Visit settings for more info!");
        }
        $this->key = $key;
        $this->qsb = $qsb;
    }

    public function makeTranslationRequest(array $from, array $to, $text)
    {
        $this->prepareParams($from, $to, $text);
        $this->response = $this->sendRequest();
    }

    private function prepareParams(array $from ,array $to, $text)
    {
        if (empty($text)) {
            throw new \Exception("Translaton text cannot be empty!");
        }

        if (count($to) == 0) {
            throw new \Exception("To parameter cannot be empty!");
        }

        $this->text = $text;

        foreach ($from as $lang) {
            $this->qsb->add('from',$this->convertToAPILang($lang));
        }

        foreach ($to as $lang) {
            $this->qsb->add('to',$this->convertToAPILang($lang));
        }
    }

    private function sendRequest()
    {
        $this->qsb->add($this->versionKey, $this->version);

        $args = $this->qsb->build();
        $url = $this->host.'?'.$args;

        $content = json_encode([
            [
                'Text' => $this->text
            ]
        ]);

        $headers = "Content-type: application/json\r\n" .
            "Content-length: " . strlen($content) . "\r\n" .
            "Ocp-Apim-Subscription-Key: $this->key\r\n";

        $options = array (
            'http' => array (
                'header' => $headers,
                'method' => 'POST',
                'content' => $content
            )
        );
        $context  = stream_context_create ($options);
        try {
            $result = @file_get_contents($url, false, $context);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
        if ($result === false) {
            throw new \Exception("Error on response from bing! Error message: ".error_get_last()['message']);
        }
        return json_encode(json_decode($result), JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    }

    public function getResult()
    {
        return json_decode($this->response,true);
    }

    private function convertToAPILang($lang)
    {
        if ($lang == 'gb') {
            $lang = 'en';
        }

        return $lang;
    }
}