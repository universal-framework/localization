<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 5.5.2018 г.
 * Time: 19:29 ч.
 */

namespace Localization\Template;


use Localization\Entity\Field;
use Localization\Entity\Template;

class Field2Template
{
    private $PDO;
    private $field;
    private $template;
    private $table = 'templates_fields';

    public function __construct(\PDO $PDO)
    {
        $this->PDO = $PDO;
    }

    private function validateFieldAndTemplate()
    {
        if (empty($this->field->getId())) {
            throw new \Exception("Field ID is empty!");
        }

        if (empty($this->template->getId())) {
            throw new \Exception("Temp ID is empty!");
        }
    }

    public function save(Field $field, Template $template)
    {
        $this->field = $field;
        $this->template = $template;

        $this->validateFieldAndTemplate();

        $fieldID = $this->field->getId();
        $tempID = $this->template->getId();

        $sth = $this->PDO->prepare(
            sprintf("INSERT INTO %s VALUES (:tid,:fid) ON DUPLICATE "."KEY UPDATE FID = :fid", MAIN_DB.'.'.$this->table)
        );
        $sth->bindParam(':tid', $tempID, \PDO::PARAM_INT);
        $sth->bindParam(':fid', $fieldID, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        return true;
    }

    public function load($TID)
    {
        $sth = $this->PDO->prepare(sprintf("SELECT * FROM %s WHERE TID = :tid", MAIN_DB.'.'.$this->table));
        $sth->bindParam(':tid', $TID, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return [];
        }

        $FIDs = array_column($data, 'FID');

        return $FIDs;
    }

    public function delete($TID)
    {
        if (!is_int($TID) || !intval($TID) > 0 ) {
            throw new \Exception("Invalid TID passed before deleting from fields_templates");
        }

        $sth = $this->PDO->prepare(sprintf("DELETE FROM %s WHERE TID = :tid", MAIN_DB.'.'.$this->table));
        $sth->bindParam(':tid', $TID, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        return true;
    }

    public function deleteAll()
    {
        throw new \Exception("DANGEROUS FUNCTION, AVOID!");
    }
}