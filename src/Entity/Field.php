<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 1.5.2018 г.
 * Time: 18:10 ч.
 */

namespace Localization\Entity;

use Localization\Entity\ICrudable;

class Field implements ICrudable
{
    protected $id = null;
    protected $uniqueLabel;
    protected $table = 'fields';
    /**
     * @var \
     * PDO $PDO
     */
    private $PDO;

    public function __construct(\PDO $PDO)
    {
        $this->PDO = $PDO;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     * @param array $data - 'field'
     * @return bool
     * @throws \Exception
     */

    public function load($id = null, array $data = [])
    {
        $where = [];
        $field = "";

        if ($id !== null && $id > 0) {
            $where[] = 'FID = :fid';
            $bindFID = true;
        }

        if(isset($data['field']) && false === empty($data['field'])) {
            $field = trim(strip_tags($data['field']));
            $where[] = 'fUniqueLabel = :fName';
            $bindUniqueLab = true;
        }

        if (count($where) > 0) {
            $whereAppend = "WHERE ".implode(" AND ", $where);
        } else {
            $whereAppend = "";
        }

        $sth = $this->PDO->prepare(sprintf("SELECT * FROM %s %s", MAIN_DB.'.'.$this->table, $whereAppend));
        if (isset($bindFID) && $bindFID == true) {
            $sth->bindParam(':fid', $id, \PDO::PARAM_INT);
        }

        if (isset($bindUniqueLab) && $bindUniqueLab == true) {
            $sth->bindParam(':fName', $field);
        }

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return false;
        }

        $this->id = $data[0]['FID'];
        $this->uniqueLabel = $data[0]['fUniqueLabel'];

        return true;
    }

    public function save(array $data)
    {
        $this->uniqueLabel = $data['label'];

        $sth = $this->PDO->prepare(sprintf("INSERT INTO %s SET fUniqueLabel = :label
", MAIN_DB.'.'.$this->table));
        $sth->bindParam(':label', $this->uniqueLabel);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $this->id = $this->PDO->lastInsertId();

        return $this->id;
    }

    public function update($id, array $data)
    {
        if (!$this->load($id)) {
            return false;
        }

         if (!isset($data['label']) || $this->uniqueLabel === $data['label']) {
             return true;
         }

        $label = trim(strip_tags($data['label']));

        $sth = $this->PDO->prepare(sprintf("UPDATE %s SET fUniqueLabel = :label WHERE FID = :fieldid", MAIN_DB.'.'.$this->table));
        $sth->bindParam(':fieldid', $this->id, \PDO::PARAM_INT);
        $sth->bindParam(':label', $label);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $this->uniqueLabel = $label;

        return true;
    }

    public function delete($FID)
    {
        if (!is_int($FID) || !intval($FID) > 0 ) {
            throw new \Exception("Invalid FID passed before deleting from fields_templates");
        }

        $sth = $this->PDO->prepare(sprintf("DELETE FROM %s WHERE FID = :fid", MAIN_DB.'.'.$this->table));
        $sth->bindParam(':fid', $FID, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        return true;
    }

    public function getCount()
    {
        $sth = $this->PDO->prepare(sprintf("SELECT COUNT(*) AS fieldCount FROM %s",MAIN_DB.'.'.$this->table));

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return 0;
        }

        return $data[0]['fieldCount'];
    }

    public function getAll()
    {
        $sth = $this->PDO->prepare(sprintf("SELECT * FROM %s",MAIN_DB.'.'.$this->table));

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return [];
        }

        return $data;
    }

    public function getTranslationsCount($fid = null)
    {
        if ($fid !== null && $fid > 0) {
            $this->id = $fid;
        }

        $sth = $this->PDO->prepare(
            sprintf(
                "SELECT
                    COUNT(*) AS fCount
                  FROM
                  (
                    ( SELECT * FROM %s.fields_languages_short WHERE FID = :fid)
                    UNION
                    ( SELECT * FROM %s.fields_languages_long WHERE FID = :fid)
                   ) AS x",
                MAIN_DB, MAIN_DB)
        );

        $sth->bindParam(':fid', $this->id, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetch(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return 0;
        }

        return $data['fCount'];
    }

    public function getData() {
        return ['id' => $this->id, 'label' => $this->uniqueLabel];
    }
}