<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 12.5.2018 г.
 * Time: 17:52 ч.
 */

namespace Localization\Entity;


class AdminUser implements ICrudable
{
    /**
     * @var \PDO
     */
    private $PDO;
    private $id;
    private $username;
    private $password;
    private $table = 'users';
    const MIN_PASSWORD_LENGTH = 3;

    public function __construct(\PDO $PDO)
    {
        $this->PDO = $PDO;
    }

    private function setUsername($username)
    {
        $username = trim($username);

        if ( !preg_match('/^[A-Za-z][A-Za-z0-9]{3,31}$/', $username) ) {
            throw new \Exception("Username does not match requirements for only alphanumeric characters,3 to 31 chars ");
        }

        $this->username = $username;
    }

    private function setPassword($password)
    {
        $password = trim($password);

        if (empty($password)) {
            throw new \Exception("Password is empty!");
        }

        if (strlen($password) < self::MIN_PASSWORD_LENGTH) {
            throw new \Exception(
                sprintf("For security reasons, username should be greater than %d characters", self::MIN_PASSWORD_LENGTH)
            );
        }

        $this->password = password_hash($password, PASSWORD_BCRYPT);
    }

    public function save(array $data)
    {
        $this->setUsername($data['username']);
        $this->setPassword($data['password']);

        $sth = $this->PDO->prepare(sprintf('INSERT INTO %s SET uUser=:user, uPass = :pass', MAIN_DB.'.'.$this->table));

        $sth->bindParam(':user', $this->username, \PDO::PARAM_STR);
        $sth->bindParam(':pass', $this->password, \PDO::PARAM_STR);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $this->id = $this->PDO->lastInsertId();

        return $this->id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function load($id, array $data)
    {
        $this->setUsername($data['user']);
        $rawPassword = $data['pass'];

        $sth = $this->PDO->prepare(
            sprintf("SELECT UID, uPass FROM %s WHERE uUser = :user",MAIN_DB.'.'.$this->table)
        );

        $sth->bindParam(':user', $this->username, \PDO::PARAM_STR);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if(count($data) === 0) {
            return false;
        } else {
            $this->id = $data[0]['UID'];
            $hash = $data[0]['uPass'];

            if ( false === password_verify($rawPassword, $hash)) {
                return false;
            }
        }
    }

    public function update($id, array $data)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }
}