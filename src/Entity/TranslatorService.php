<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 26.5.2018 г.
 * Time: 22:05 ч.
 */

namespace Localization\Entity;

use Localization\Entity\Language;

class TranslatorService
{
    private $PDO;
    private $table = 'services_translations';
    private $id;
    private $sName;
    private $apiKey;

    public function __construct(\PDO $PDO)
    {
        $this->PDO = $PDO;
    }

    public function load($id, array $data = [])
    {
        $where = [];

        if ($id !== null && $id > 0) {
            $where[] = 'SID = :sid';
            $bindSID = true;
        }

        if (count($where) > 0) {
            $whereAppend = "WHERE ".implode(" AND ", $where);
        } else {
            $whereAppend = "";
        }

        $sth = $this->PDO->prepare(sprintf("SELECT * FROM %s %s", MAIN_DB.'.translator_services', $whereAppend));

        if (isset($bindSID) && $bindSID == true) {
            $sth->bindParam(':sid', $id, \PDO::PARAM_INT);
        }

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return false;
        }

        $this->id = $data[0]['SID'];
        $this->sName = $data[0]['sName'];
        $this->apiKey = $data[0]['sAPIKey'];

        return true;
    }

    public function save($SID, Language $lang, $translationText, $sourceLID, $sourceTranslation)
    {
        if (empty($SID) || !is_numeric($SID)) {
           throw new \Exception("SID is not valid!");
        }

        $langID = $lang->getId();

        $translationText = strip_tags(trim($translationText));

        $sLID = intval($sourceLID);
        $sTranslation = strip_tags(trim($sourceTranslation));

        $sth = $this->PDO->prepare(
            sprintf(
                "INSERT INTO %s SET SID = :sid, LID = :LID, translation = :trans,".
                " sourceLID = :sLID, sourceTranslation = :sTrans ".
                "ON DUPLICATE KEY UPDATE translation = :trans, sourceLID = :sLID, sourceTranslation = :sTrans",
                MAIN_DB.'.'.$this->table
            )
        );
        $sth->bindParam(':sid', $SID, \PDO::PARAM_INT);
        $sth->bindParam(':LID', $langID, \PDO::PARAM_INT);
        $sth->bindParam(':trans', $translationText, \PDO::PARAM_STR);
        $sth->bindParam(':sLID', $sLID, \PDO::PARAM_INT);
        $sth->bindParam(':sTrans', $sTranslation, \PDO::PARAM_STR);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        return true;

    }

    public function update($id, array $data)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function getAllServices()
    {
        $sth = $this->PDO->prepare(
            sprintf(
                "SELECT * FROM %s",
                MAIN_DB.'.translator_services'
            )
        );
        $sth->bindParam(':sid', $SID, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return false;
        }

        return $data;
    }

    public function getServiceByID($SID)
    {
        if (empty($SID) || !is_numeric($SID)) {
            throw new \Exception("INVALID SID!");
        }

        $sth = $this->PDO->prepare(
            sprintf(
                "SELECT * FROM %s WHERE SID = :sid",
                MAIN_DB.'.translator_services'
            )
        );
        $sth->bindParam(':sid', $SID, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return [];
        }

        return $data[0];
    }

    public function updateService($SID, $apikey)
    {
        $SID = intval($SID);
        $apikey = strip_tags(trim($apikey));

        $sth = $this->PDO->prepare(sprintf('UPDATE %s SET sAPIKey = :apikey WHERE SID = :sid', MAIN_DB.'.translator_services'));

        $sth->bindParam(':sid', $SID, \PDO::PARAM_INT);
        $sth->bindParam(':apikey', $apikey, \PDO::PARAM_STR);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        return true;
    }

    public function getTranslationFromCache($SID, $LID, $sourceLID, $sourceTranslation)
    {
        $retArr = [];

        if($LID == "all") {
            $LIDQ = "";
        } else {
            $LIDQ = "AND LID = :lid";
        }
        $sth = $this->PDO->prepare(
            sprintf("SELECT LID,translation FROM %s WHERE SID = :sid ".$LIDQ." AND sourceLID = :slid AND sourceTranslation = :sTrans",
                MAIN_DB.'.'.$this->table)
        );

        $sth->bindParam(':sid', $SID, \PDO::PARAM_INT);
        if ($LIDQ != "") {
            $sth->bindParam(':lid', $LID, \PDO::PARAM_INT);
        }
        $sth->bindParam(':slid', $sourceLID, \PDO::PARAM_STR);
        $sth->bindParam(':sTrans', $sourceTranslation, \PDO::PARAM_STR);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return [];
        }

        foreach($data as $transData) {
            $retArr[$transData['LID']] = $transData['translation'];
        }

        return $retArr;
    }

    public function getAllTrans($SID, $sourceLang, $sourceTrans)
    {
        $retArr = [];

        $sth = $this->PDO->prepare(
            sprintf("SELECT translation, LID FROM %s WHERE SID = :sid AND sourceLID = :slid AND ".
                "sourceTranslation = :strans",
                MAIN_DB.'.'.$this->table)
        );

        $sth->bindParam(':sid', $SID, \PDO::PARAM_INT);
        $sth->bindParam(':slid', $sourceLang, \PDO::PARAM_INT);
        $sth->bindParam(':strans', $sourceTrans, \PDO::PARAM_STR);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) === 0) {
            return [];
        }

        foreach($data as $servTranslations) {
            $retArr[$servTranslations['LID']] = $servTranslations['translation'];
        }

        return $retArr;
    }
}