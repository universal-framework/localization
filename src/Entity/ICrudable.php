<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 2.5.2018 г.
 * Time: 21:04 ч.
 */

namespace Localization\Entity;


interface ICrudable
{
    public function load($id, array $data);
    public function save(array $data);
    public function update($id, array $data);
    public function delete($id);

}