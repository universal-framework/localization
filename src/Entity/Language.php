<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 2.5.2018 г.
 * Time: 21:37 ч.
 */

namespace Localization\Entity;


class Language implements ICrudable
{
    private $lang;
    private $desc;
    private $id;
    private $bingRef;
    const langAbbvLength = 2;
    /**
     * @var \PDO $PDO
     */
    private $PDO;
    private $table = 'languages';

    public function __construct(\PDO $PDO)
    {
        $this->PDO = $PDO;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAbbv()
    {
        return $this->lang;
    }


    public function load($id, array $data = [])
    {
        $where = [];
        $field = "";

        if ($id !== null && $id >= 0) {
            $where[] = 'LID = :lid';
            $bindLID = true;
        }

        if (isset($data['lang']) && false === empty($data['lang'])) {
            $lang = trim(strip_tags($data['lang']));
            $where[] = 'lLangAbbv = :lang';
            $bindUniqueLang = true;
        }

        if (isset($data['desc']) && false === empty($data['desc'])) {
            $desc = trim(strip_tags($data['desc']));
            $where[] = 'lLangDesc = :desc';
            $bindDesc = true;
        }

        if (isset($data['code']) && false === empty($data['code'])) {
            $code = trim(strip_tags($data['code']));
            $where[] = 'lBingRef = :bingref';
            $bingRef = true;
        }

        if (count($where) > 0) {
            $whereAppend = "WHERE ".implode(" AND ", $where);
        } else {
            $whereAppend = "";
        }

        $sth = $this->PDO->prepare(sprintf("SELECT * FROM %s %s", MAIN_DB.'.'.$this->table, $whereAppend));
        if (isset($bindLID) && $bindLID == true) {
            $sth->bindParam(':lid', $id, \PDO::PARAM_INT);
        }

        if (isset($bindUniqueLang) && $bindUniqueLang == true) {
            $sth->bindParam(':lang', $lang);
        }

        if (isset($bindDesc) && $bindDesc == true) {
            $sth->bindParam(':desc', $desc);
        }

        if (isset ($bingRef) && $bingRef == true) {
            $sth->bindParam(':bingref', $code, \PDO::PARAM_STR);
        }

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return false;
        }

        $this->id = $data[0]['LID'];
        $this->lang = $data[0]['lLangAbbv'];
        $this->desc = $data[0]['lLangDesc'];
        $this->bingRef = $data[0]['lBingRef'];

        return true;
    }

    public function save(array $data)
    {
        $this->lang = $data['abbv'];

        if (strlen($this->lang) > self::langAbbvLength) {
           throw new \Exception(sprintf("Lang abbv should not be longer than %d symbols", self::langAbbvLength));
        }

        $this->desc = $data['desc'];

        $this->bingRef = $data['code'];

        $sth = $this->PDO->prepare(
            sprintf("INSERT INTO %s SET lLangAbbv = :abbv, lLangDesc = :desc, lBingRef = :code", MAIN_DB.'.'.$this->table)
        );
        $sth->bindParam(':abbv', $this->lang);
        $sth->bindParam(':desc', $this->desc);
        $sth->bindParam(':code', $this->bingRef);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $this->id = $this->PDO->lastInsertId();

        return $this->id;
    }

    public function update($id, array $data = [])
    {
        if (!$this->load($id)) {
            return false;
        }

        if (!isset($data['abbv']) || empty($data['abbv'])) {
            throw new \Exception("Abbv is not filled");
        }

        if ($this->lang === $data['abbv'] && $this->desc === $data['desc'] && $this->bingRef === $data['code']) {
            return true;
        }

        $abbv = trim(strip_tags($data['abbv']));
        $desc = trim(strip_tags($data['desc']));
        $code = trim(strip_tags($data['code']));

        $sth = $this->PDO->prepare(
            sprintf("UPDATE %s SET lLangAbbv = :abbv, lLangDesc = :desc, lBingRef = :code WHERE LID = :lid", MAIN_DB.'.'.$this->table)
        );
        $sth->bindParam(':lid', $this->id, \PDO::PARAM_INT);
        $sth->bindParam(':abbv', $abbv, \PDO::PARAM_STR);
        $sth->bindParam(':desc', $desc, \PDO::PARAM_STR);
        $sth->bindParam(':code', $code, \PDO::PARAM_STR);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $this->lang = $abbv;
        $this->desc = $desc;

        return true;
    }

    public function delete($LID)
    {
        if (!is_int($LID) || !intval($LID) > 0 ) {
            throw new \Exception("Invalid LID passed before deleting from Language");
        }

        $sth = $this->PDO->prepare(sprintf("DELETE FROM %s WHERE LID = :lid", MAIN_DB.'.'.$this->table));
        $sth->bindParam(':lid', $LID, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        return true;
    }

    public function getCount()
    {
        $sth = $this->PDO->prepare(sprintf("SELECT COUNT(*) AS langCount FROM %s",MAIN_DB.'.'.$this->table));

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return 0;
        }

        return $data[0]['langCount'];
    }

    public function getAll()
    {
        $sth = $this->PDO->prepare(sprintf("SELECT * FROM %s",MAIN_DB.'.'.$this->table));

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return [];
        }

        return $data;
    }

    public function getData()
    {
        return ['abbv' => $this->lang, 'desc' => $this->desc, 'code' => $this->bingRef, 'id' => $this->id];
    }

}