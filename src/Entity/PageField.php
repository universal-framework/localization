<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 12.5.2018 г.
 * Time: 01:11 ч.
 */

namespace Localization\Entity;


class PageField implements ICrudable
{
    protected $id = null;
    protected $PID = null;
    protected $uniqueLabel;
    protected $table = 'page_fields';
    /**
     * @var \PDO $PDO
     */
    private $PDO;

    public function __construct(\PDO $PDO)
    {
        $this->PDO = $PDO;
    }

    public function getId()
    {
        return $this->id;
    }

    private function validate()
    {
        if (empty($this->uniqueLabel)) {
            throw new \Exception("field label is empty!");
        }

        if ($this->PID === null || !is_numeric($this->PID)) {
            throw new \Exception("Page ID is empty!");
        }
    }

    /**
     * @param null $id
     * @param array $data - 'field'
     * @return bool
     * @throws \Exception
     */

    public function load($id = null, array $data = [])
    {
        $where = [];
        $whereAppended = "";
        $bindFID = false;
        $bindField = false;

        if ($id !== null && $id > 0) {
            $where[] = 'FID = :fid';
            $bindFID = true;
        }

//        if (!isset($data['field']) || empty($data['field'])) {
//            throw new \Exception("No field set for loading");
//        }

        if(isset($data['field']) && !empty($data['field'])) {
            $field = trim(strip_tags($data['field']));
            $bindField = true;
            $where[] = 'fUniqueLabel = :fName';
        }

        if (!isset($data['page_id']) || empty($data['page_id'])) {
            throw new \Exception("No page set as a relation to the field.");
        }

        $page_id = intval($data['page_id']);

        if (count($where) > 0) {
            $whereAppended = " AND ".implode(" AND ", $where);
        }

        $sth = $this->PDO->prepare(
            sprintf("SELECT * FROM %s WHERE PID = :page_id %s",
                MAIN_DB.'.'.$this->table, $whereAppended
            )
        );

        if ($bindField) {
            $sth->bindParam(':fName', $field, \PDO::PARAM_STR);
        }

        if ($bindFID) {
            $sth->bindParam(':fid', $id, \PDO::PARAM_INT);
        }
        $sth->bindParam(':page_id', $page_id, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return false;
        }

        $this->id = $data[0]['FID'];
        $this->PID = $data[0]['PID'];
        $this->uniqueLabel = $data[0]['fUniqueLabel'];

        return true;
    }

    public function save(array $data)
    {
        $this->uniqueLabel = $data['field'];
        $this->PID = $data['page_id'];

        $this->validate();

        $sth = $this->PDO->prepare(
            sprintf("INSERT INTO %s SET fUniqueLabel = :label, PID = :pid", MAIN_DB.'.'.$this->table)
        );
        $sth->bindParam(':label', $this->uniqueLabel, \PDO::PARAM_STR);
        $sth->bindParam(':pid', $this->PID, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $this->id = $this->PDO->lastInsertId();

        return $this->id;
    }

    public function loadAllFields($PID)
    {
        if (empty($PID)) {
            throw new \Exception("No PID set for loading");
        }

        $PID = intval($PID);

        $sth = $this->PDO->prepare(
            sprintf("SELECT * FROM %s WHERE  PID = :page_id",
                MAIN_DB.'.'.$this->table
            )
        );

        $sth->bindParam(':page_id', $PID, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return [];
        }

        return $data;
    }

    public function update($id, array $data)
    {
        // TODO: Implement update() method.
    }

    public function delete($PID)
    {
        if (!is_int($PID) || !intval($PID) > 0 ) {
            throw new \Exception("Invalid PID passed before deleting from page_fields");
        }

        $sth = $this->PDO->prepare(sprintf("DELETE FROM %s WHERE PID = :pid", MAIN_DB.'.'.$this->table));
        $sth->bindParam(':pid', $PID, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        return true;
    }

    public function getExistingFields($PID, array $fields)
    {
        $FIDToLabel = [];

        if (empty($PID)) {
            throw new \Exception("No PID set for loading");
        }

        $PID = intval($PID);

        if (count($fields) == 0) {
            return [];
        }

        foreach ($fields as &$field) {
            $field = "'".$field."'";
        }

        $implodedFields = implode(',', $fields);

        $sth = $this->PDO->prepare(
            sprintf("SELECT FID, fUniqueLabel FROM %s WHERE fUniqueLabel IN (%s) AND PID = :pid",
                MAIN_DB.'.'.$this->table, $implodedFields
            )
        );

        $sth->bindParam(':pid', $PID, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return [];
        }

        foreach ($data as $pageField) {
            $FIDToLabel[$pageField['FID']] = $pageField['fUniqueLabel'];
        }

        return $FIDToLabel;

    }

    public function deleteAllInPageExcept($PID, array $options = [])
    {
        if (empty($PID)) {
            throw new \Exception("No PID set for loading");
        }

        $PID = intval($PID);
        $except = [];

        if (count($options['except']) > 0) {
            foreach ($options['except'] as $FID) {
                $except[] = intval($FID);
            }

            $fidNotInQuery = "AND FID NOT IN (".implode(', ', $except).")";
        } else {
            $fidNotInQuery = "";
        }

        $sth = $this->PDO->prepare(
            sprintf("DELETE FROM %s WHERE PID = :pid %s",
                MAIN_DB.'.'.$this->table, $fidNotInQuery
            )
        );

        $sth->bindParam(':pid', $PID, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        return true;
    }

    public function getData()
    {
        return ['id' => $this->id, 'PID' => $this->PID, 'label' => $this->uniqueLabel];
    }
}