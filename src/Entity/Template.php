<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 5.5.2018 г.
 * Time: 18:56 ч.
 */

namespace Localization\Entity;


class Template implements ICrudable
{
    private $PDO;
    private $label;
    private $tType;
    private $id;
    private $desc;
    private $table = 'templates';
    private $allowedTTypes = ['h' => 'Header','s' => 'Standard','f' => 'Footer'];

    public function __construct(\PDO $PDO)
    {
        $this->PDO = $PDO;
    }

    public function getData()
    {
        return ['id' => $this->id, 'label' => $this->label, 'desc' => $this->desc, 'ttype' => $this->tType,
                'fullTempType' => $this->allowedTTypes[$this->tType]];
    }

    public function load($id, array $data = [])
    {
        $where = [];
        $field = "";

        if ($id !== null && $id >= 0) {
            $where[] = 'TID = :tid';
            $bindTID = true;
        }

        if(isset($data['name']) && false === empty($data['name'])) {
            $name = trim(strip_tags($data['name']));
            $where[] = 'tUniqueLabel = :tName';
            $bindUniqueLab = true;
        }

        if (count($where) > 0) {
            $whereAppend = "WHERE ".implode(" AND ", $where);
        } else {
            $whereAppend = "";
        }

        $sth = $this->PDO->prepare(sprintf("SELECT * FROM %s %s", MAIN_DB.'.'.$this->table, $whereAppend));
        if (isset($bindTID) && $bindTID == true) {
            $sth->bindParam(':tid', $id, \PDO::PARAM_INT);
        }

        if (isset($bindUniqueLab) && $bindUniqueLab == true) {
            $sth->bindParam(':tName', $name);
        }

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return false;
        }

        $this->id = $data[0]['TID'];
        $this->label = $data[0]['tUniqueLabel'];
        $this->desc = $data[0]['tDesc'];
        $this->tType = $data[0]['tType'];

        return true;
    }

    public function save(array $data)
    {
        //TODO VALIDATE FIELDS!
        $this->label = $data['name'];
        $this->desc = $data['desc'];

        if (!isset($data['tType']) || !in_array($data['tType'], array_keys($this->allowedTTypes))) {
            throw new \Exception("No template type specified!");
        }

        $this->tType = $data['tType'];

        $sth = $this->PDO->prepare(
            sprintf("INSERT INTO %s SET tUniqueLabel = :name, tDesc = :desc, tType = :ttype", MAIN_DB.'.'.$this->table)
        );
        $sth->bindParam(':name', $this->label);
        $sth->bindParam(':desc', $this->desc);
        $sth->bindParam(':ttype', $this->tType);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $this->id = $this->PDO->lastInsertId();

        return $this->id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function update($TID, array $data)
    {
        if (!is_int($TID) || !intval($TID) > 0 ) {
            throw new \Exception("Invalid TID passed before deleting from fields_templates");
        }


        if (!isset($data['tType']) || !in_array($data['tType'], array_keys($this->allowedTTypes))) {
            throw new \Exception("No template type specified!");
        }

        $tType = $data['tType'];

        $name = trim($data['name']);
        $desc = trim($data['desc']);

        $sth = $this->PDO->prepare(
            sprintf("UPDATE %s SET tUniqueLabel = :name, tDesc = :desc, tType = :ttype WHERE TID = :tid", MAIN_DB.'.'.$this->table)
        );
        $sth->bindParam(':tid', $TID, \PDO::PARAM_INT);
        $sth->bindParam(':name', $name, \PDO::PARAM_STR);
        $sth->bindParam(':desc', $desc, \PDO::PARAM_STR);
        $sth->bindParam(':ttype', $tType, \PDO::PARAM_STR);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $this->id = $TID;
        $this->label = $name;
        $this->desc = $desc;
        $this->tType = $tType;

        return true;
    }

    public function delete($TID)
    {
        if (!is_int($TID) || !intval($TID) > 0 ) {
            throw new \Exception("Invalid TID passed before deleting from fields_templates");
        }

        $sth = $this->PDO->prepare(sprintf("DELETE FROM %s WHERE TID = :tid", MAIN_DB.'.'.$this->table));
        $sth->bindParam(':tid', $TID, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        return true;
}

    public function getCount()
    {
        $sth = $this->PDO->prepare(sprintf("SELECT COUNT(*) AS templateCount FROM %s",MAIN_DB.'.'.$this->table));

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return 0;
        }

        return $data[0]['templateCount'];
    }

    public function listAll()
    {
        $sth = $this->PDO->prepare(
            sprintf("
                    SELECT
                        TID, tUniqueLabel, tDesc, tType,
                        (SELECT COUNT(*) FROM ".MAIN_DB.".templates_fields WHERE TID = x.TID) AS fCount
                    FROM %s AS x
                   ",MAIN_DB.'.'.$this->table)
        );

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $list = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($list) == 0) {
            return [];
        }

        foreach ($list as &$singleTemplate) {
            $singleTemplate['fullTempType'] = $this->allowedTTypes[$singleTemplate['tType']];
        }

        return $list;
    }

    public function deleteAll()
    {
        throw new \Exception("DANGEROUS FUNCTION, AVOID!");
    }

    public function getTemplatesByTempTypes()
    {
        $templatesByTempType = ['h' => [], 'f' => [], 's' => []];

        $sth = $this->PDO->prepare(
            sprintf("
                    SELECT
                        TID, tUniqueLabel, tDesc, tType
                    FROM %s ORDER BY tType;
                   ",MAIN_DB.'.'.$this->table)
        );

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $list = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($list) == 0) {
            return $templatesByTempType;
        }

        foreach ($list as $singleTemplate) {
            $singleTemplate['fullTempType'] = $this->allowedTTypes[$singleTemplate['tType']];
            $templatesByTempType[$singleTemplate['tType']][] = $singleTemplate;

        }

        return $templatesByTempType;
    }
}