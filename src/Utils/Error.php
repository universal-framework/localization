<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 12.5.2018 г.
 * Time: 18:44 ч.
 */

namespace Localization\Utils;


class Error
{
    private $errors = [];
    private $hasError = false;
    private $realErrors = [];
    public static $standardMessage = "Couldn't process your request. Please try again!";
    private static $instance = null;

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new Error();
        }

        return self::$instance;
    }

    public function add($message)
    {
        if ($this->hasError == false) {
            $this->hasError = true;
        }

        $this->errors[] = $message;
    }

    public function isErrorSome()
    {
        return (true === $this->hasError);
    }

    public function fetchAll()
    {
        return implode("<br />", $this->errors);
    }

    public function alert($message)
    {
        $this->realErrors[] = $message;
    }
}