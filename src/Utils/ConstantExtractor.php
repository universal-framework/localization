<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 13.5.2018 г.
 * Time: 14:10 ч.
 */

namespace Localization\Utils;


class ConstantExtractor
{
    private static $instance;

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getTemplatesPath()
    {
        return TEMPLATES_PATH;
    }

    public function getLogPath()
    {
        return LOGS_PATH;
    }
}