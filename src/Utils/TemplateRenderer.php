<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 13.5.2018 г.
 * Time: 14:04 ч.
 */

namespace Localization\Utils;


class TemplateRenderer
{
    private static $instance = null;
    private $includePath = null;

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self(ConstantExtractor::getInstance()->getTemplatesPath());
        }

        return self::$instance;
    }

    public function __construct($includePath)
    {
        $this->includePath = $includePath;
    }

    public function render($tempName, $data = [])
    {
        if (count($data) > 0) {
            extract($data);
        }

        $tempPath = $this->includePath.$tempName.'.inc.php';

        if (file_exists($tempPath)) {
            include_once ($tempPath);
        } else {
            throw new \Exception(sprintf("Include path %s does not exist!", $tempName));
        }

    }
}