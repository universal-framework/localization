<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 26.5.2018 г.
 * Time: 21:55 ч.
 */

namespace Localization\Utils;


class QueryStringBuilder
{
    private $parts = array();

    public function add($key, $value) {
        $this->parts[] = array(
            'key'   => $key,
            'value' => $value
        );
    }

    public function build($separator = '&', $equals = '=') {
        $queryString = array();

        foreach($this->parts as $part) {
            $queryString[] = $part['key'] . $equals . urlencode($part['value']);
        }

        return implode($separator, $queryString);
    }

    public function __toString() {
        return $this->build();
    }
}