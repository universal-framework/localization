<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 1.5.2018 г.
 * Time: 00:50 ч.
 */

namespace Localization\Utils;


class PDOWrapper
{
    private static $pdoInstances = [];
    private static $defaultConns = ['default' => ['host' => 'localhost', 'user' => 'root', 'password' => '',
        'db' => MAIN_DB]];

    public static function getInstance($dbName = 'default')
    {
        if (false === array_key_exists($dbName, self::$defaultConns)) {
            throw new \Exception(sprintf("No such database host as %s exists!", $dbName));
        }

        if (!isset(self::$pdoInstances[$dbName])) {

            $dbString = (isset(self::$pdoInstances[$dbName]['db']))? ';dbname='.self::$pdoInstances[$dbName]['db'] : '';
            self::$pdoInstances[$dbName] = new \PDO(
                'mysql:host='.self::$defaultConns[$dbName]['host'].$dbString.';charset=utf8'
                ,self::$defaultConns[$dbName]['user'],
                self::$defaultConns[$dbName]['password']
            );
        }

        return self::$pdoInstances[$dbName];
    }
}