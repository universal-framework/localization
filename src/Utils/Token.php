<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 15.5.2018 г.
 * Time: 23:56 ч.
 */

namespace Localization\Utils;


class Token
{
    public static function generateToken()
    {
        if (function_exists('mcrypt_create_iv')) {
            $token = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
        } else {
            $token = bin2hex(openssl_random_pseudo_bytes(32));
        }

        return $token;
    }
}