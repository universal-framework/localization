<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 13.5.2018 г.
 * Time: 14:22 ч.
 */

namespace Localization\Utils;


class Logger
{
    private $logPath;

    private static $instance;

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self(ConstantExtractor::getInstance()->getLogPath());
        }

        return self::$instance;
    }

    public function __construct($logPath)
    {
        $this->logPath = $logPath;
    }

    public function alert($msg)
    {
        $this->log('alert',$msg);
    }

    public function warning($msg)
    {
        $this->log('warning',$msg);
    }

    public function notice($msg)
    {
        $this->log('notice',$msg);
    }

    private function log($level, $message)
    {
        if (empty($msg)) {
            return;
        }

        $today = date('Y_m_d');

        $logFile = $this->logPath.'_'.$today.'_'.$level;

        $finalLogMsg = '['.date('Y_m_d H:i:s').']: '.$message.PHP_EOL;

        $fp = fopen($logFile, 'a');

        fwrite($fp, $finalLogMsg);

        fclose($fp);
    }
}