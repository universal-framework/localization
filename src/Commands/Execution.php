<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 28.4.2018 г.
 * Time: 17:33 ч.
 */

namespace Localization\Commands;

use Localization\Entity\AdminUser;
use Localization\Entity\PageField;
use Localization\Entity\Template;
use Localization\Migrations\DatabaseStructure;
use Localization\Page\Page;
use Localization\Page\PageTranslator;
use Localization\Template\Field2Template;
use Localization\Translator\Translator;
use Localization\Utils\PDOWrapper;
use Localization\Entity\Field;
use Localization\Entity\Language;

class Execution
{
    static $dbCreateFn;
    static $fieldCreateFn;
    static $insertLangFn;
    static $makeTranslationFn;
    static $insertTransFn;
    static $emptyFnWithExit;
    static $insertTemplateFn;
    static $addFieldToTempFn;
    static $createPageFn;
    static $insertPageTranslationFn;
    static $createAdminUserFn;
    private static $instance = null;

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function __construct()
    {
        /**
         * @param $dbName
         * @param bool|false $force
         * @return array - ['success'=> true/false, 'message' => string]
         */
        self::$dbCreateFn = function($dbName, $force = false) {

            try{
                $dbStructureMigration = new DatabaseStructure(PDOWrapper::getInstance(), $dbName, $force);

                $dbStructureMigration->run();

                if ($dbStructureMigration->isSuccessful()) {
                    return ['success' => true, 'message' => sprintf('Successfully created database %s', $dbName)];
                } else{
                    return ['success' => false, 'message' => sprintf("Database %s couldn't be created successfully", $dbName)];
                }
            } catch ( \Exception $e) {
                return ['success' => false, 'message' => $e->getMessage()];
            }
        };

        self::$fieldCreateFn = function($fieldName) {

            $fieldObj = new Field(PDOWrapper::getInstance());
            try{
                $newID = $fieldObj->save(['label' => $fieldName]);

                return ['success' => true, 'message' => sprintf('Successfully created field with label %s. New ID: %d',
                    $fieldName, $newID)];
            } catch (\Exception $e) {
                return ['success' => false, 'message' => $e->getMessage()];
            }

        };

        self::$insertLangFn = function($abbv, $desc = null) {

            $langObj = new Language(PDOWrapper::getInstance());
            try{
                $newID = $langObj->save(['abbv' => $abbv, 'desc' => $desc]);

                return ['success' => true, 'message' => sprintf('Successfully created language %s. New ID: %d',
                    $abbv, $newID)];
            } catch (\Exception $e) {
                return ['success' => false, 'message' => $e->getMessage()];
            }
        };

        self::$insertTransFn = function($field, $lang, $translation, $force = false) {
            $PDOWrapper = PDOWrapper::getInstance();

            $fieldObj = new Field($PDOWrapper);
            if (false === $fieldObj->load(null, ['field' => $field])) {
                return [
                    'success' => false,
                    'message' => sprintf('Field %s does not exist in the database. Would you like to add?', $field),
                    'prompt' => true,
                    'yes' => ['fn' => self::$fieldCreateFn, 'arg' => [$field]],
                    'no' => ['fn' => self::$emptyFnWithExit],
                    'continue' => true
                ];
            }

            $langObj = new Language($PDOWrapper);
            if (false === $langObj->load(null, ['lang' => $lang])) {
                return [
                    'success' => false,
                    'message' => sprintf('Lang %s does not exist in the database. Would you like to add?', $lang),
                    'prompt' => true,
                    'yes' => ['fn' => self::$insertLangFn, 'arg' => [$lang]],
                    'no' => ['fn' => self::$emptyFnWithExit],
                    'continue' => true
                ];
            }

            try {

                $translatorObj = new Translator($PDOWrapper);

                $translatorObj->save($fieldObj, $langObj, $translation, $force);

                return ['success' => true, 'message' => sprintf('Successfully saved translation under'.'
                 field %s and lang %s', $field, $lang)];
            } catch (\Exception $e) {
                return ['success' => false, 'message' => $e->getMessage()];
            }
        };

        self::$insertTemplateFn = function ($name, $desc = '') {
            $templateObj = new Template(PDOWrapper::getInstance());
            try{
                $newID = $templateObj->save(['name' => $name, 'desc' => $desc]);

                return ['success' => true, 'message' => sprintf('Successfully created template %s. New ID: %d',
                    $name, $newID)];
            } catch (\Exception $e) {
                return ['success' => false, 'message' => $e->getMessage()];
            }
        };

        self::$addFieldToTempFn = function($field, $temp) {
            $PDOWrapper = PDOWrapper::getInstance();

            $fieldObj = new Field($PDOWrapper);
            if (false === $fieldObj->load(null, ['field' => $field])) {
                return [
                    'success' => false,
                    'message' => sprintf('Field %s does not exist in the database. Would you like to add?', $field),
                    'prompt' => true,
                    'yes' => ['fn' => self::$fieldCreateFn, 'arg' => [$field]],
                    'no' => ['fn' => self::$emptyFnWithExit],
                    'continue' => true
                ];
            }

            $templateObj = new Template($PDOWrapper);
            if (false === $templateObj->load(null, ['name' => $temp])) {
                return [
                    'success' => false,
                    'message' => sprintf('Template %s does not exist in the database. Would you like to add?', $temp),
                    'prompt' => true,
                    'yes' => ['fn' => self::$insertTemplateFn, 'arg' => [$temp]],
                    'no' => ['fn' => self::$emptyFnWithExit],
                    'continue' => true
                ];
            }

            try {

                $field2TemplateObj = new Field2Template($PDOWrapper);

                $field2TemplateObj->save($fieldObj, $templateObj);

                return ['success' => true, 'message' => sprintf('Successfully saved field %s under'.'
                 template %s', $field, $temp)];
            } catch (\Exception $e) {
                return ['success' => false, 'message' => $e->getMessage()];
            }
        };

        self::$createPageFn = function($name, $desc = null, $hTemplate = null, $template = null, $fTemplate = null) {

            $PDO = PDOWrapper::getInstance();

            $fTemplateObj = null;
            $hTemplateObj = null;
            $templateObj = null;

            if ($hTemplate !== null) {
                $hTemplateObj = new Template($PDO);
                if (false === $hTemplateObj->load(null, ['name' => $hTemplate])) {
                    return [
                        'success' => false,
                        'message' => sprintf('Could not load template %s from the database. Would you like to add?',
                            $hTemplate),
                        'prompt' => true,
                        'yes' => ['fn' => self::$insertTemplateFn, 'arg' => [$hTemplate]],
                        'no' => ['fn' => self::$emptyFnWithExit],
                        'continue' => true
                    ];
                }
            }

            if ($template !== null) {
                $templateObj = new Template($PDO);

                if (false === $templateObj->load(null, ['name' => $template])) {
                    return [
                        'success' => false,
                        'message' => sprintf('Could not load template %s from the database. Would you like to add?',
                            $template),
                        'prompt' => true,
                        'yes' => ['fn' => self::$insertTemplateFn, 'arg' => [$template]],
                        'no' => ['fn' => self::$emptyFnWithExit],
                        'continue' => true
                    ];
                }
            }

            if ($fTemplate !== null) {

                $fTemplateObj = new Template($PDO);
                if (false === $fTemplateObj->load(null, ['name' => $fTemplate])) {
                    return [
                        'success' => false,
                        'message' => sprintf('Could not load template %s from the database. Would you like to add?',
                            $fTemplate),
                        'prompt' => true,
                        'yes' => ['fn' => self::$insertTemplateFn, 'arg' => [$fTemplate]],
                        'no' => ['fn' => self::$emptyFnWithExit],
                        'continue' => true
                    ];
                }
            }

            $pageObj = new Page($PDO);

            try{
                $newID = $pageObj->save($name, $desc, $hTemplateObj, $templateObj, $fTemplateObj);

                return ['success' => true, 'message' => sprintf('Successfully created page with label %s. New ID: %d',
                    $name, $newID)];
            } catch (\Exception $e) {
                return ['success' => false, 'message' => $e->getMessage()];
            }
        };

        self::$insertPageTranslationFn = function ($page, $field, $lang, $translation) {

            $PDOWrapper = PDOWrapper::getInstance();

            $pageObj = new Page($PDOWrapper);
            if (false === $pageObj->load(null, ['name' => $page])) {
                return [
                    'success' => false,
                    'message' => sprintf('Page %s does not exist in the database. Would you like to add?', $page),
                    'prompt' => true,
                    'yes' => ['fn' => self::$createPageFn, 'arg' => [$page]],
                    'no' => ['fn' => self::$emptyFnWithExit],
                    'continue' => true
                ];
            }

            $pageFieldObj = new PageField($PDOWrapper);
            if (false === $pageFieldObj->load(null, ['field' => $field, 'page_id' => $pageObj->getId()])) {
                try {
                    $pageFieldObj->save(['field' => $field, 'page_id' => $pageObj->getId()]);
                } catch (\Exception $e) {
                    return ['success' => false, 'message' => $e->getMessage()];
                }
            }

            $langObj = new Language($PDOWrapper);
            if (false === $langObj->load(null, ['lang' => $lang])) {
                return [
                    'success' => false,
                    'message' => sprintf('Lang %s does not exist in the database. Would you like to add?', $lang),
                    'prompt' => true,
                    'yes' => ['fn' => self::$insertLangFn, 'arg' => [$lang]],
                    'no' => ['fn' => self::$emptyFnWithExit],
                    'continue' => true
                ];
            }

            try {

                $pageTranslator = new PageTranslator($PDOWrapper);
                $pageTranslator->save($pageObj, $langObj, $pageFieldObj, $translation);

                return ['success' => true, 'message' => sprintf('Successfully saved translation under'.'
                 page %s, field %s and lang %s',$page, $field, $lang)];
            } catch (\Exception $e) {
                return ['success' => false, 'message' => $e->getMessage()];
            }

        };

        self::$createAdminUserFn = function($user, $pass) {
            $userAdmin = new AdminUser(PDOWrapper::getInstance());
            try{
                $newID = $userAdmin->save(['username' => $user, 'password' => $pass]);

                return ['success' => true, 'message' =>
                    sprintf('Successfully saved user with username: "%s" and password "%s". New ID: %d', $user, $pass, $newID)];
            } catch (\Exception $e) {
                return ['success' => false, 'message' => $e->getMessage()];
            }
        };

        self::$emptyFnWithExit = function() {
            exit(0);
        };
    }
}