<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 15.4.2018 г.
 * Time: 20:10 ч.
 */

namespace Localization\Commands;

use Localization\Commands\Execution;
use Localization\Constant;

class Rules
{

    /**
     * Commands contains array of allowed commands to run.
     *
     * @var array
     */

    private $commands = [
        'help' =>
        [
            'description' => 'Lists all commands with description'
        ],
        'create:database' =>
        [
            'params' => [
                'dbName' => ['desc' => 'The name of the database in which the structure will be applied', 'type' => '--',
                    'required' => true, 'dataType' => 'string'],
                'f' => ['desc' => 'If the database name exists, the -f option will overwrite the database.',
                    'type' => '-', 'default' => false]
            ],
            'description' => 'Allows you to create a database structure for translations with a '.'
                                  specified name.'
        ],
        'create:field' =>
        [
            'params' => [
                'name' => ['desc' => 'The name of the field to save to a database', 'type' => '--',
                    'required' => true, 'dataType' => 'string']
            ],
            'description' => 'Allows you to create a field, which will be used to have translations of'
        ],
        'insert:language' =>
        [
            'params' => [
                'abbv' => ['desc' => 'The language abbv to insert into the database', 'type' => '--', 'required' => true,
                    'dataType' => 'string'],
                'desc' => ['desc' => 'The language description', 'type' => '--', 'required' => false, 'default' => null,
                    'dataType' => 'string']
            ],
            'description' => 'The command adds a language to the database.'
        ],
        'insert:translation' =>
        [
            'params' => [
                'field' => ['desc' => 'The field unique name under whom the translation will be applied', 'type' => '--',
                    'required' => true, 'dataType' => 'string'],
                'lang' => ['desc' => 'The language in which the translation will be made', 'type' => '--',
                    'required' => true, 'dataType' => 'string'],
                'translation' => ['desc' => 'The translation text. Could be up to any characters long.', 'type' => '--',
                    'required' => true, 'dataType' => 'string'],
                'f' => ['desc' => 'Force updating the field+lang if a translation already exists in the database',
                    'type' => '-', 'required' => false, 'dataType' => 'string', 'default' => false]
            ],
            'description' => 'Adds a translation to the database!'
        ],
        'insert:template' =>
        [
            'params' => [
                'name' => ['desc' => 'Name of the template', 'type' => '--', 'required' => true, 'dataType' => 'string'],
                'desc' => ['desc' => 'Description of the template', 'type' => '--',
                    'required' => false, 'dataType' => 'string']
            ],
            'description' => 'Inserts a template, which acts as a container for certain types of fields.'
        ],
        'add:field2template' =>
        [
            'params' => [
                'field' => ['desc' => 'The unique field name', 'type' => '--',
                    'required' => true, 'dataType' => 'string'],
                'template' => ['desc' => 'The unique name of the template', 'type' => '--',
                    'required' => true, 'dataType' => 'string']
             ],
            'description' => 'Adds a certain field to a template'
        ],
        'create:page' =>
        [
            'params' => [
                'name' => ['desc' => 'The unique name of the page', 'type' => '--',
                    'required' => true, 'dataType' => 'string'],
                'desc' => ['desc' => 'The description of the page', 'type' => '--',
                    'required' => false, 'dataType' => 'string'],
                'hTemplate' => ['desc' => 'The header template name', 'type' => '--',
                    'required' => false, 'dataType' => 'string'],
                'template' => ['desc' => 'The main template name', 'type' => '--',
                    'required' => false, 'dataType' => 'string'],
                'fTemplate' => ['desc' => 'The footer template name', 'type' => '--',
                    'required' => false, 'dataType' => 'string']
            ],
            'description' => 'Creates a page, which involves header/footer and main template!'
        ],
        'insert:pagetranslation' =>
        [
            'params' => [
                'page' => ['desc' => 'The unique label of the page', 'type' => '--',
                    'required' => true, 'dataType' => 'string'],
                'field' => ['desc' => 'A unique fieldname to be added. If the field already exists, it updates the value',
                            'type' => '--', 'required' => true, 'dataType' => 'string'],
                'lang' => ['desc' => 'The lang in which the translation will be applied', 'type' => '--',
                    'required' => true, 'dataType' => 'string'],
                'translation' => ['desc' => 'The translation in the corrensponding language', 'type' => '--',
                    'required' => true, 'dataType' => 'string']
            ],
            'description' => 'Creates a translation, that is specific to a page. Used only locally by the page.'
        ],
        'create:user' =>
        [
            'params' => [
                'user' => ['desc' => 'The unique username of the admin.', 'type' => '--',
                    'required' => true, 'dataType' => 'string'],
                'pass' => ['desc' => 'The password of the username', 'type' => '--',
                    'required' => true, 'dataType' => 'string']
            ],
            'description' => 'Creates admin user. Password requirements: greater than 3 digits'
        ]
    ];

    private $debugInfo = [];
    private $arguments;

    public function __construct()
    {
        $this->buildExecutionPlan();
    }

    private function buildExecutionPlan()
    {
        $executor = Execution::getInstance();

        $this->commands['help']['execution'] = function()
        {
            return ['success' => true, 'message' => $this->getAvailableCommands(), 'nocolor' => true];
        };

        $this->commands['create:database']['execution'] = $executor::$dbCreateFn;
        $this->commands['create:database']['paramOrder'] = ['dbName', 'f'];

        $this->commands['create:field']['execution'] = $executor::$fieldCreateFn;

        $this->commands['insert:language']['execution'] = $executor::$insertLangFn;
        $this->commands['insert:language']['paramOrder'] = ['abbv', 'desc'];

        $this->commands['insert:translation']['execution'] = $executor::$insertTransFn;
        $this->commands['insert:translation']['paramOrder'] = ['field', 'lang', 'translation', 'f'];

        $this->commands['insert:template']['execution'] = $executor::$insertTemplateFn;
        $this->commands['insert:template']['paramOrder'] = ['name', 'desc'];

        $this->commands['add:field2template']['execution'] = $executor::$addFieldToTempFn;
        $this->commands['add:field2template']['paramOrder'] = ['field','temp'];

        $this->commands['create:page']['execution'] = $executor::$createPageFn;
        $this->commands['create:page']['paramOrder'] = ['name', 'desc', 'hTemplate', 'template', 'fTemplate'];

        $this->commands['insert:pagetranslation']['execution'] = $executor::$insertPageTranslationFn;
        $this->commands['insert:pagetranslation']['paramOrder'] = ['page', 'field', 'lang', 'translation'];

        $this->commands['create:user']['execution'] = $executor::$createAdminUserFn;
        $this->commands['create:user']['paramOrder'] = ['user', 'pass'];
     }

    public function validateAndExecute($commandName, array $arguments = [])
    {
        $valid = $this->isValidCommand($commandName, $arguments);

        if (true == $valid) {
            return $this->execute($commandName);
        }

        return false;
    }

    private function isValidCommand($commandName, array $arguments = [])
    {
        return ($this->checkCommandName($commandName) && $this->checkArguments($commandName, $arguments));
    }

    private function checkCommandName($commandName)
    {
        if (array_key_exists($commandName,$this->commands)) {
            $this->debugInfo[] = sprintf("Command name %s exists!", $commandName);
            return true;
        }

        throw new \Exception(sprintf('Command %s does not exist!', $commandName));
    }

    private function checkArguments($commandName, array $arg = [])
    {
        $command = $this->commands[$commandName];

        if (false === isset($command['params']) || count($command['params']) == 0) {
            $this->debugInfo[] = sprintf("Function blueprint shows that the function accepts no arguments, continuing...");

            return true;
        }

        foreach($command['params'] as $param => $paramData){

            //short option
            if ($paramData['type'] == '-') {

                $exists = false;

                foreach ($arg as $argument) {
                    // We leave it 0, because we need an exact match at the first index!
                    if (0 === stripos($argument, '-'.$param)) {
                        $exists = true;
                        break;
                    }
                }

                if ($exists === false) {

                    if (isset($paramData['required']) && $paramData['required']) {
                        throw new \Exception(sprintf("Required param -%s is empty!",$param));
                    }

                    $this->debugInfo[] = sprintf('No value provided for option -%s.', $param);
                } else {
                    $this->arguments[$param] = true;
                }

                // If short option is present, it means we pass true as its argument


            } else {

                $exists = false;

                foreach ($arg as $argument) {
                    // We leave it 0, because we need an exact match at the first index!
                    if (0 === stripos($argument, '--'.$param.'=')) {
                        $exists = true;
                        $optionValue = explode("=",$argument);
                        $optionValue = $optionValue[1];
                        break;
                    }
                }

                if ($exists === false || empty($optionValue)) {

                    if (isset($paramData['required']) && $paramData['required']) {
                        throw new \Exception(sprintf("Required param --%s is empty!", $param));
                    }

                    $this->debugInfo[] = sprintf('No value provided for option --%s.', $param);
                } else {
                    if (false === $this->isValidType($optionValue, $paramData['dataType'])) {
                        throw new \Exception(
                            sprintf("Invalid type for option %s. Got value %s, expected %s",
                                $param, $optionValue, $paramData['dataType']
                            )
                        );
                    }

                    $this->arguments[$param] = $optionValue;
                }
            }
        }

        return true;
    }

    public function isValidType($option, $dataType) {

        if($dataType == 'string') {
            return true;
        }

        if($dataType == 'int') {
            $castedDataType = intval($dataType);
            if ($dataType != 0 && $castedDataType == 0) {
                return false;
            }
        }

        if ($dataType == 'bool') {
            if (strtolower($option) !== 'true' && strtolower($option) !== 'false') {
                return false;
            }
        }

        return true;
    }

    /**
     * @param $commandName
     * @return array - ['success'=> true/false, 'message' => string]
     */

    private function execute($commandName)
    {
        if( !isset($this->commands[$commandName]['params']) || count($this->commands[$commandName]['params']) == 0) {
            $result =  $this->commands[$commandName]['execution']();

            $continue = isset($result['continue'])? $result['continue'] : false;

            if (isset($result['prompt'])) {
                $result = $this->prompt($result);

                if( $result['success'] === false) {
                    return $result;
                } else {
                    $msg = $result['message'];
                }

                if ($continue) {
                    $result = $this->commands[$commandName]['execution']();
                    $result['message'] = $msg.PHP_EOL.PHP_EOL.$result['message'];

                }
            }

            return $result;
        }

        if (!isset($this->commands[$commandName]['paramOrder'])) {
            $paramOrder = array_keys($this->arguments);
        } else {
            $paramOrder = $this->commands[$commandName]['paramOrder'];
        }

        foreach ($paramOrder as $param) {
            if (false === isset($this->arguments[$param])) {
                $this->arguments[$param] = isset($this->commands[$commandName]['params'][$param]['default']) ?
                    ($this->commands[$commandName]['params'][$param]['default']) : null;
            }

            $paramsPass[] = $this->arguments[$param];
        }

        $result = call_user_func_array($this->commands[$commandName]['execution'], $paramsPass);

        $continue = isset($result['continue'])? $result['continue'] : false;

        if (isset($result['prompt'])) {
            $result = $this->prompt($result);

            if( $result['success'] === false) {
                return $result;
            } else {
                $msg = $result['message'];
            }

            if ($continue) {
                $result = call_user_func_array($this->commands[$commandName]['execution'], $paramsPass);
                $result['message'] = $msg.PHP_EOL.PHP_EOL.$result['message'];
            }
        }

        return $result;
    }

    public function getDebugInfo()
    {
        array_walk($this->debugInfo, function(&$item) { $item = '[INFO]: '.$item; });

        return implode(PHP_EOL,$this->debugInfo);
    }

    public function prompt(array $returnData)
    {
        echo PHP_EOL.PHP_EOL.Constant::$yellowBGCLI.Constant::$whiteTextCLI.
            $returnData['message']." Answer with yes/no!".Constant::$colorEndCLI.PHP_EOL;

        $passedOnce = false;

        do{

            if ($passedOnce === true) {
                echo "Enter yes/no for answer!".PHP_EOL;
            }

            $fnContainer = null;

            $inputHandler = fopen('php://stdin',"r");

            $line = strtolower(trim(fgets($inputHandler)));

            if($line == 'yes') {

                $fnContainer = $returnData['yes'];
                $fn = isset($returnData['yes']['fn'])? $returnData['yes']['fn'] : null;
                $arg = isset($returnData['yes']['arg'])? $returnData['yes']['arg'] : null;

            } else {

                $fnContainer = $returnData['no'];
                isset($returnData['no']['fn'])? $returnData['no']['fn'] : null;
                $arg = isset($returnData['no']['arg'])? $returnData['no']['arg'] : null;

            }

            if (isset($fnContainer) && (isset($fn) && is_callable($fn))) {

                fclose($inputHandler);

                if (isset($arg) && count($arg) > 0) {
                    $return = call_user_func_array($fn, $arg);
                } else {
                    $return = $fn();
                }

                if (isset($return['prompt'])) {
                    $this->prompt($return);
                } else {
                    return $return;
                }
            }

            $passedOnce = true;

        }while($line != 'yes' && $line != 'no');


        fclose($inputHandler);

        echo "No action has been called!".PHP_EOL;

        exit(1);
    }


    /**
     * @return string
     */

    public function getAvailableCommands()
    {
        $output = PHP_EOL."=========================COMMANDS===========================".PHP_EOL.PHP_EOL;

        foreach ($this->commands as $command => $details) {
            $output .= PHP_EOL.PHP_EOL.sprintf("Command name => %s", $command).PHP_EOL;
            $output .= "-------------------------------------------------------------------------".PHP_EOL;
            $output .= sprintf("Description => %s",$details['description']).PHP_EOL.PHP_EOL;
            if (isset($details['params']) && count($details['params'])) {
                $output .= "Params:".PHP_EOL;

                foreach ($details['params'] as $param => $paramsOpt) {
                    $output .= sprintf("%s : %s", $paramsOpt['type'].$param, $paramsOpt['desc']).PHP_EOL;
                }
            }


        }

        $output .= "=========================================================================".PHP_EOL;


        return $output;
    }
}