<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 12.5.2018 г.
 * Time: 01:35 ч.
 */

namespace Localization\Page;


use Localization\Entity\Field;
use Localization\Entity\Language;
use Localization\Entity\PageField;

class PageTranslator
{
    private $PDO = null;
    private $page = null;
    private $lang = null;
    private $pField = null;
    private $table = 'page_translations';
    private $translation;

    public function __construct(\PDO $PDO)
    {
        $this->PDO = $PDO;
    }

    private function validate()
    {
        if (empty($this->pField->getId())) {
            throw new \Exception("Page Field ID is empty!");
        }

        if (empty($this->lang->getId())) {
            throw new \Exception("Lang ID is empty!");
        }

        if (empty($this->page->getId())) {
            throw new \Exception("Page ID is empty!");
        }
    }

    public function save(Page $page, Language $lang,PageField $pField, $translation)
    {
        $this->page = $page;
        $this->lang = $lang;
        $this->pField = $pField;
        $this->translation = $translation;

        $this->validate();

        $pageID = $this->page->getId();
        $pFieldID = $this->pField->getId();
        $langAbbv = $this->lang->getAbbv();

        if (empty($this->translation)) {
            throw new \Exception("No translation provided!");
        }

        $sth = $this->PDO->prepare(
            sprintf(
                "INSERT INTO %s SET PID = :pid, pFieldID = :pfid, pLang = :labbv, translation = :trans ".
                "ON DUPLICATE KEY UPDATE translation = :trans",
                MAIN_DB.'.'.$this->table
            )
        );
        $sth->bindParam(':pid', $pageID, \PDO::PARAM_INT);
        $sth->bindParam(':pfid', $pFieldID, \PDO::PARAM_INT);
        $sth->bindParam(':labbv', $langAbbv, \PDO::PARAM_STR);
        $sth->bindParam(':trans', $this->translation);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        return true;
    }

    public function update($id, array $data)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function getCount()
    {
        $sth = $this->PDO->prepare(sprintf("SELECT COUNT(*) AS pageTrans FROM %s",MAIN_DB.'.'.$this->table));

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return 0;
        }

        return $data[0]['pageTrans'];
    }

    public function getCustomTranslationsCount($PID)
    {
        if (empty($PID)) {
            throw new \Exception("No PID passed");
        }

        $sth = $this->PDO->prepare(
            sprintf("SELECT COUNT(*) AS customTransCount FROM %s WHERE pid=:pid", MAIN_DB.'.'.$this->table)
        );

        $sth->bindParam(':pid',$PID, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return 0;
        }

        return $data[0]['customTransCount'];
    }

    public function getTranslations(Page $page, PageField $field)
    {
        $mapIDToTranslation = [];
        $fieldID = $field->getId();
        $pageID = $page->getId();

        $sth = $this->PDO->prepare(sprintf("SELECT
                                      *
                                    FROM
                                      %s
                                    WHERE
                                      pFieldID = :fid AND PID = :pid",MAIN_DB.'.'.$this->table));

        $sth->bindParam(':fid', $fieldID, \PDO::PARAM_INT);
        $sth->bindParam(':pid', $pageID, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return [];
        }
        foreach($data as $transData) {
            $mapIDToTranslation[$transData['pLang']] = $transData['translation'];
        }


        return $mapIDToTranslation;
    }

    public function deleteAllTransInPageExcept($PID, array $options = [])
    {
        if (empty($PID)) {
            throw new \Exception("No PID set for loading");
        }

        $PID = intval($PID);
        $except = [];

        if (count($options['except']) > 0) {
            foreach ($options['except'] as $FID) {
                $except[] = intval($FID);
            }

            $fidNotInQuery = "AND pFieldID NOT IN (".implode(', ', $except).")";
        } else {
            $fidNotInQuery = "";
        }

        $sth = $this->PDO->prepare(
            sprintf("DELETE FROM %s WHERE PID = :pid %s",
                MAIN_DB.'.'.$this->table, $fidNotInQuery
            )
        );

        $sth->bindParam(':pid', $PID, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        return true;
    }

    public function getAllEmptyTranslations($lAbbv, $lSourceAbbv, $transType)
    {
        if ($transType == 'fillempty') {
            $fidNotIn = "SELECT
                            pFieldID
                        FROM
                          %s AS fls
                        WHERE
                          fls.pLang = :labbv AND fls.translation IS NOT NULL AND fls.translation <> \"\"";

            $bindAbbv = true;
        } else {
            $fidNotIn = "SELECT -1 FROM %s";
            $bindAbbv = false;
        }

        $sth = $this->PDO->prepare(sprintf("

                               SELECT *, 'page' AS transType FROM %s AS flsMain

                                WHERE
                                    flsMain.pFieldID NOT IN($fidNotIn)
                                AND
                                    flsMain.pLang = :lsourceabbv AND flsMain.translation IS NOT NULL AND flsMain.translation <> \"\"
        ", MAIN_DB.'.'.$this->table, MAIN_DB.'.'.$this->table));

        if ($bindAbbv) {
            $sth->bindParam(':labbv', $lAbbv, \PDO::PARAM_INT);
        }
        $sth->bindParam(':lsourceabbv', $lSourceAbbv, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return [];
        }

        return $data;
    }
}