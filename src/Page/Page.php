<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 10.5.2018 г.
 * Time: 23:51 ч.
 */

namespace Localization\Page;


use Localization\Entity\Template;

class Page
{
    private $id = null;
    private $label = null;
    private $desc = null;
    private $hTempID = null;
    private $mTempID = null;
    private $fTempID = null;
    /**
     * @var Template
     */
    private $hTempObj = null;
    /**
     * @var Template
     */
    private $tempObj = null;
    /**
     * @var Template
     */
    private $fTempObj = null;
    private $table = 'pages';
    /**
     * @var \PDO
     */
    private $PDO = null;

    public function __construct(\PDO $PDO)
    {
        $this->PDO = $PDO;
    }

    public function getId()
    {
        return $this->id;
    }

    private function validateFields()
    {
        if (empty($this->label)) {
            throw new \Exception("Label for page cannot be empty!");
        }

        if ($this->hTempObj !== null && empty($this->hTempObj->getId())) {
            throw new \Exception("No ID provided for header template object");
        }

        if ($this->tempObj !== null && empty($this->tempObj->getId())) {
            throw new \Exception("No ID provided for template object");
        }

        if ($this->fTempObj !== null && empty($this->fTempObj->getId())) {
            throw new \Exception("No ID provided for footer template object");
        }
    }

    public function load($id, array $data = [])
    {
        $where = [];
        $bindPID = false;
        $bindName = false;

        if ($id !== null && $id >= 0) {
            $where[] = 'PID = :pid';
            $bindPID = true;
        }

        if (false === empty($data['name'])) {
            $name = trim(strip_tags($data['name']));
            $where[] = 'pLabel = :label';
            $bindName = true;
        }

        if (count($where) > 0) {
            $whereAppend = "WHERE ".implode(" AND ", $where);
        } else {
            $whereAppend = "";
        }

        $sth = $this->PDO->prepare(
            sprintf("SELECT * FROM %s %s",
                MAIN_DB.'.'.$this->table, $whereAppend
            )
        );

        if ($bindName) {
            $sth->bindParam(':label', $name, \PDO::PARAM_STR);
        }

        if ($bindPID) {
            $sth->bindParam(':pid', $id, \PDO::PARAM_INT);
        }

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return false;
        }

        $this->id = $data[0]['PID'];
        $this->label = $data[0]['pLabel'];
        $this->desc = $data[0]['pDesc'];
        $this->hTempID = $data[0]['pTempHeaderID'];
        $this->mTempID = $data[0]['pTempMainID'];
        $this->fTempID = $data[0]['pTempFooterID'];

        return true;
    }

    public function save($label, $desc = null, Template $hTempObj = null, Template $tempObj = null,Template $fTempObj = null)
    {
        $this->label = $label;
        $this->desc = $desc;
        $this->hTempObj = $hTempObj;
        $this->tempObj = $tempObj;
        $this->fTempObj = $fTempObj;

        $this->validateFields();

        $HID = null;
        $FID = null;
        $TID = null;

        if ($this->hTempObj !== null) {
            $HID = $this->hTempObj->getId();
        }

        if ($this->tempObj !== null) {
            $TID = $this->tempObj->getId();
        }

        if ($this->fTempObj !== null) {
            $FID = $this->fTempObj->getId();
        }

        $sth = $this->PDO->prepare(
            sprintf("INSERT INTO %s SET pLabel = :label, pTempHeaderID = :HID, pTempMainID = :TID, pTempFooterID = :FID,".
                "pDesc = :pDesc", MAIN_DB.'.'.$this->table)
        );
        $sth->bindParam(':label', $this->label);
        $sth->bindParam(':HID', $HID, \PDO::PARAM_INT);
        $sth->bindParam(':TID', $TID, \PDO::PARAM_INT);
        $sth->bindParam(':FID', $FID, \PDO::PARAM_INT);
        $sth->bindParam(':pDesc', $this->desc, \PDO::PARAM_STR);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $this->id = $this->PDO->lastInsertId();

        return $this->id;
    }

    public function update($PID, array $data)
    {
        if (!$this->load($PID)) {
            throw new \Exception("No such page exists!");
        }

        $this->label = $data['label'];
        $this->desc = $data['desc'];
        $this->hTempObj = $data['hTemplate'];
        $this->tempObj = $data['mTemplate'];
        $this->fTempObj = $data['fTemplate'];

        $this->validateFields();

        $HID = null;
        $FID = null;
        $TID = null;

        if ($this->hTempObj !== null) {
            $HID = $this->hTempObj->getId();
        } else {
            $HID = null;
        }

        if ($this->tempObj !== null) {
            $TID = $this->tempObj->getId();
        } else {
            $TID = null;
        }

        if ($this->fTempObj !== null) {
            $FID = $this->fTempObj->getId();
        } else {
            $FID = null;
        }

        $sth = $this->PDO->prepare(
            sprintf("UPDATE %s SET pLabel = :label, pTempHeaderID = :HID, pTempMainID = :TID, pTempFooterID = :FID,".
                "pDesc = :pDesc WHERE PID = :pid", MAIN_DB.'.'.$this->table)
        );
        $sth->bindParam(':label', $this->label);
        $sth->bindParam(':HID', $HID, \PDO::PARAM_INT);
        $sth->bindParam(':TID', $TID, \PDO::PARAM_INT);
        $sth->bindParam(':FID', $FID, \PDO::PARAM_INT);
        $sth->bindParam(':pDesc', $this->desc, \PDO::PARAM_STR);
        $sth->bindParam(':pid', $PID, \PDO::PARAM_STR);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        return true;
    }

    public function delete($PID)
    {
        if (!is_int($PID) || !intval($PID) > 0 ) {
            throw new \Exception("Invalid PID passed before deleting from page_fields");
        }

        $sth = $this->PDO->prepare(sprintf("DELETE FROM %s WHERE PID = :pid", MAIN_DB.'.'.$this->table));
        $sth->bindParam(':pid', $PID, \PDO::PARAM_INT);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        return true;
    }

    public function getCount()
    {
        $sth = $this->PDO->prepare(sprintf("SELECT COUNT(*) AS pageCount FROM %s",MAIN_DB.'.'.$this->table));

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return 0;
        }

        return $data[0]['pageCount'];
    }

    public function getAll()
    {
        $sth = $this->PDO->prepare(sprintf("SELECT * FROM %s",MAIN_DB.'.'.$this->table));

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return [];
        }

        return $data;
    }

    public function getData()
    {
        return ['id' => $this->id, 'label' => $this->label, 'desc' => $this->desc,'hTempID' => $this->hTempID,
            'mTempID' => $this->mTempID, 'fTempID' => $this->fTempID];
    }
}