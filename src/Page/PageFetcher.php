<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 20.6.2018 г.
 * Time: 21:40 ч.
 */

namespace Localization\Page;


class PageFetcher
{
    /**
     * @var \PDO
     */
    private $PDO;

    /**
     * @var string
     */
    private $dbName;

    private $langID;

    public function __construct(\PDO $PDO, $dbName)
    {
        $this->PDO = $PDO;
        $this->dbName = $dbName;
    }

    private function getLanguageByAbbv($abbv)
    {
        if ($this->langID !== null) {
            return $this->langID;
        }

        $sth = $this->PDO->prepare("
              SELECT
                  LID
              FROM
                {$this->dbName}.languages
              WHERE
                  lLangAbbv = :lang

              ");

        $sth->bindParam(':lang', $abbv);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetch(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            throw new \Exception("No such language");
        }

        $this->langID = $data['LID'];

        return $this->langID;
    }

    public function getTranslationsByName($language, $pageName)
    {
        $translations = [];

        $translations['header'] = $this->getHeaderFields($language, $pageName);
        $translations['main'] = $this->getMainFields($language, $pageName);
        $translations['footer'] = $this->getFooterFields($language, $pageName);
        $translations['extra'] = $this->getExtraFields($language, $pageName);

        return $translations;
    }

    public function getHeaderFields($language, $pageName)
    {
        $fieldTranslations = [];

        $lid = $this->getLanguageByAbbv($language);

        $sth = $this->PDO->prepare("
              SELECT
                  f.fUniqueLabel AS field,
                  IF(fls.translation IS NOT NULL, fls.translation, fll.translation) AS translation
              FROM
                {$this->dbName}.pages AS p
              LEFT JOIN {$this->dbName}.templates AS t_header ON (p.pTempHeaderID = t_header.TID)
              LEFT JOIN {$this->dbName}.templates_fields AS tf_header ON (t_header.TID = tf_header.TID)
              LEFT JOIN {$this->dbName}.fields AS f ON (f.FID = tf_header.FID)
              LEFT JOIN {$this->dbName}.fields_languages_short AS fls ON (fls.FID = f.FID AND fls.LID = :lid)
              LEFT JOIN {$this->dbName}.fields_languages_long AS fll ON (fll.FID = f.FID AND fll.LID = :lid)
              LEFT JOIN {$this->dbName}.languages AS l_fls ON (fls.LID = l_fls.LID)
              LEFT JOIN {$this->dbName}.languages AS l_fll ON (fll.LID = l_fll.LID)
              WHERE
                  p.pLabel = :pageName

              ");

        $sth->bindParam(':lid', $lid, \PDO::PARAM_INT);
        $sth->bindParam(':pageName', $pageName);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return [];
        }

        foreach ($data as $record) {
            $fieldTranslations[$record['field']] = $record['translation'];
        }

        return $fieldTranslations;
    }

    public function getMainFields($language, $pageName)
    {
        $fieldTranslations = [];

        $LID = $this->getLanguageByAbbv($language);

        $sth = $this->PDO->prepare("
              SELECT
                  f.fUniqueLabel AS field,
                  IF(fls.translation IS NOT NULL, fls.translation, fll.translation) AS translation
              FROM
                {$this->dbName}.pages AS p
              LEFT JOIN {$this->dbName}.templates AS t_main ON (p.pTempMainID = t_main.TID)
              LEFT JOIN {$this->dbName}.templates_fields AS tf_main ON (t_main.TID = tf_main.TID)
              LEFT JOIN {$this->dbName}.fields AS f ON (f.FID = tf_main.FID)
              LEFT JOIN {$this->dbName}.fields_languages_short AS fls ON (fls.FID = f.FID AND fls.LID = :lid)
              LEFT JOIN {$this->dbName}.fields_languages_long AS fll ON (fll.FID = f.FID AND fll.LID = :lid)
              LEFT JOIN {$this->dbName}.languages AS l_fls ON (fls.LID = l_fls.LID)
              LEFT JOIN {$this->dbName}.languages AS l_fll ON (fll.LID = l_fll.LID)
              WHERE
                  p.pLabel = :pageName
              ");

        $sth->bindParam(':lid', $LID, \PDO::PARAM_INT);
        $sth->bindParam(':pageName', $pageName);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return [];
        }

        foreach ($data as $record) {
            $fieldTranslations[$record['field']] = $record['translation'];
        }

        return $fieldTranslations;
    }

    public function getFooterFields($language, $pageName)
    {

        $fieldTranslations = [];

        $lid = $this->getLanguageByAbbv($language);

        $sth = $this->PDO->prepare("
              SELECT
                  f.fUniqueLabel AS field,
                  IF(fls.translation IS NOT NULL, fls.translation, fll.translation) AS translation
              FROM
                {$this->dbName}.pages AS p
              LEFT JOIN {$this->dbName}.templates AS t_footer ON (p.pTempFooterID = t_footer.TID)
              LEFT JOIN {$this->dbName}.templates_fields AS tf_footer ON (t_footer.TID = tf_footer.TID)
              LEFT JOIN {$this->dbName}.fields AS f ON (f.FID = tf_footer.FID)
              LEFT JOIN {$this->dbName}.fields_languages_short AS fls ON (fls.FID = f.FID AND fls.LID = :lid)
              LEFT JOIN {$this->dbName}.fields_languages_long AS fll ON (fll.FID = f.FID AND fll.LID = :lid)
              LEFT JOIN {$this->dbName}.languages AS l_fls ON (fls.LID = l_fls.LID)
              LEFT JOIN {$this->dbName}.languages AS l_fll ON (fll.LID = l_fll.LID)
              WHERE
                  p.pLabel = :pageName
              ");

        $sth->bindParam(':lid', $lid, \PDO::PARAM_INT);
        $sth->bindParam(':pageName', $pageName);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return [];
        }

        foreach ($data as $record) {
            $fieldTranslations[$record['field']] = $record['translation'];
        }

        return $fieldTranslations;
    }

    public function getExtraFields($language, $pageName)
    {
        $fieldTranslations = [];

        $sth = $this->PDO->prepare("
              SELECT
                  pf.fUniqueLabel AS field,
                  pt.translation
              FROM
                {$this->dbName}.pages AS p
              LEFT JOIN {$this->dbName}.page_translations AS pt ON (p.PID = pt.PID)
              LEFT JOIN {$this->dbName}.page_fields AS pf ON (pt.pFieldID = pf.FID)

              WHERE
                  pt.pLang = :lang
                AND
                  p.pLabel = :pageName

              ");

        $sth->bindParam(':lang', $language);
        $sth->bindParam(':pageName', $pageName);

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return [];
        }

        foreach ($data as $record) {
            $fieldTranslations[$record['field']] = $record['translation'];
        }

        return $fieldTranslations;
    }

    public function getAvailableLanguages()
    {
        $sth = $this->PDO->prepare("
              SELECT
                  lLangAbbv
              FROM
                {$this->dbName}.languages
              ");

        if (!$sth->execute()) {
            throw new \Exception($sth->errorInfo()[2]);
        }

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        if (count($data) == 0) {
            return [];
        }

        return array_column($data, 'lLangAbbv');
    }

}