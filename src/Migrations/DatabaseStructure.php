<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 1.4.2018 г.
 * Time: 15:57 ч.
 */
namespace Localization\Migrations;

class DatabaseStructure
{
    private $conn;
    private $dbName = null;
    private $overwrite = false;
    private $success = false;

    public function __construct(\PDO $conn, $dbName, $overwrite = false)
    {
        $this->conn = $conn;
        if (false === $this->validateDBName($dbName)) {
            throw new \Exception(sprintf("Invalid database name %s!", $dbName));
        }

        $this->dbName = $dbName;

        $this->overwrite = ($overwrite == true);
    }

    public function run()
    {
        $this->conn->beginTransaction();

        try{
            $created = $this->createDatabase();
            if (!$created) {
                throw new \Exception(sprintf("Could not create database! Error: %s",$this->conn->errorInfo()[2]));
            }

            $this->createTables();

            $this->conn->commit();

            $this->insertDefaultData();

            $this->success = true;

        } catch (\Exception $e) {
            $this->success = false;
            $this->conn->rollBack();
            throw $e;
        }


    }

    private function createDatabase()
    {
        if($this->overwrite === true){
            $this->conn->query(sprintf('DROP DATABASE IF EXISTS `%s`',$this->dbName));
        }

        return $this->conn->query(sprintf('CREATE DATABASE `%s` CHARACTER SET utf8 COLLATE utf8_unicode_ci',$this->dbName));
    }

    private function createTables()
    {
        try {

            $stmt = $this->conn->prepare(sprintf(
                'CREATE TABLE `%s`.`fields` (
                  FID SMALLINT(11) AUTO_INCREMENT PRIMARY KEY,
                  fUniqueLabel VARCHAR(255) NOT NULL UNIQUE
            ); ', $this->dbName));

            if(!$stmt->execute()) {
                throw new \Exception($stmt->errorInfo()[2]);
            }


            $stmt = $this->conn->prepare(sprintf(
                    'CREATE TABLE `%s`.`languages` (
                      LID TINYINT(4) AUTO_INCREMENT PRIMARY KEY,
                      lLangAbbv CHAR(2) UNIQUE NOT NULL,
                      lLangDesc VARCHAR(255),
                      lBingRef VARCHAR(8)
            ) CHARACTER SET utf8 COLLATE utf8_unicode_ci; ', $this->dbName));

            if(!$stmt->execute()) {
                throw new \Exception($stmt->errorInfo()[2]);
            }

            $stmt = $this->conn->prepare(sprintf(
                    'CREATE TABLE `%s`.`fields_languages_short` (
                      FID SMALLINT(11) NOT NULL,
                      LID TINYINT(4) NOT NULL,
                      translation VARCHAR(255),
                      PRIMARY KEY(FID, LID),
                      FOREIGN KEY (FID) REFERENCES fields(FID),
                      FOREIGN KEY (LID) REFERENCES languages(LID)
            ) CHARACTER SET utf8 COLLATE utf8_unicode_ci; ', $this->dbName));

            if(!$stmt->execute()) {
                throw new \Exception($stmt->errorInfo()[2]);
            }

            $stmt = $this->conn->prepare(sprintf(
                'CREATE TABLE `%s`.`fields_languages_long` (
                      FID SMALLINT(11) NOT NULL,
                      LID TINYINT(4) NOT NULL,
                      translation TEXT,
                      PRIMARY KEY(FID, LID),
                      FOREIGN KEY (FID) REFERENCES fields(FID),
                      FOREIGN KEY (LID) REFERENCES languages(LID)
            ) CHARACTER SET utf8 COLLATE utf8_unicode_ci; ', $this->dbName));

            if(!$stmt->execute()) {
                throw new \Exception($stmt->errorInfo()[2]);
            }

            $stmt = $this->conn->prepare(sprintf(
                    "CREATE TABLE `%s`.`templates` (
                      TID SMALLINT(11) AUTO_INCREMENT PRIMARY KEY,
                      tUniqueLabel VARCHAR(255) UNIQUE NOT NULL,
                      tDesc TEXT,
                      tType ENUM('h','s','f') NOT NULL
            ) CHARACTER SET utf8 COLLATE utf8_unicode_ci;
            ", $this->dbName));


            if(!$stmt->execute()) {
                throw new \Exception($stmt->errorInfo()[2]);
            }

            $stmt = $this->conn->prepare(sprintf(
                    'CREATE TABLE `%s`.`templates_fields` (
                      TID SMALLINT(11) NOT NULL,
                      FID SMALLINT(11) NOT NULL,
                      PRIMARY KEY(TID, FID),
                      FOREIGN KEY (TID) REFERENCES templates(TID),
                      FOREIGN KEY (FID) REFERENCES fields(FID)

            ); ', $this->dbName));

            if(!$stmt->execute()) {
                throw new \Exception($stmt->errorInfo()[2]);
            }

            $stmt = $this->conn->prepare(sprintf(
                'CREATE TABLE `%s`.`pages` (
                      PID SMALLINT(11) AUTO_INCREMENT PRIMARY KEY,
                      pLabel VARCHAR(255) UNIQUE NOT NULL,
                      pDesc TEXT,
                      pTempHeaderID SMALLINT(11),
                      pTempFooterID SMALLINT(11),
                      pTempMainID SMALLINT(11),
                      FOREIGN KEY (pTempHeaderID) REFERENCES templates(TID),
                      FOREIGN KEY (pTempFooterID) REFERENCES templates(TID),
                      FOREIGN KEY (pTempMainID) REFERENCES templates(TID)

            ) CHARACTER SET utf8 COLLATE utf8_unicode_ci; ', $this->dbName));

            if(!$stmt->execute()) {
                throw new \Exception($stmt->errorInfo()[2]);
            }

            $stmt = $this->conn->prepare(sprintf(
                'CREATE TABLE `%s`.`page_fields` (
                  FID SMALLINT(11) AUTO_INCREMENT PRIMARY KEY,
                  PID SMALLINT(11) NOT NULL,
                  fUniqueLabel VARCHAR(255) NOT NULL UNIQUE,
                  FOREIGN KEY (PID) REFERENCES pages(PID)

            ); ', $this->dbName));

            if(!$stmt->execute()) {
                throw new \Exception($stmt->errorInfo()[2]);
            }

            $stmt = $this->conn->prepare(sprintf(
                'CREATE TABLE `%s`.`page_translations` (
                    PID SMALLINT(11) NOT NULL,
                    pLang CHAR(2) NOT NULL,
                    pFieldID SMALLINT(11) NOT NULL,
                    translation VARCHAR(255) NOT NULL,
                    PRIMARY KEY(PID, pFieldID, pLang),
                    FOREIGN KEY (PID) REFERENCES pages(PID),
                    FOREIGN KEY (pLang) REFERENCES languages(lLangAbbv),
                    FOREIGN KEY (pFieldID) REFERENCES page_fields(FID)
            ) CHARACTER SET utf8 COLLATE utf8_unicode_ci; ', $this->dbName));

            if(!$stmt->execute()) {
                throw new \Exception($stmt->errorInfo()[2]);
            }

            //TODO ADD FIELD TYPE

            $stmt = $this->conn->prepare(sprintf(
                'CREATE TABLE `%s`.`users` (
                  UID TINYINT(11) AUTO_INCREMENT PRIMARY KEY,
                  uUser VARCHAR(255) UNIQUE NOT NULL,
                  uPass CHAR(60) NOT NULL
            ); ', $this->dbName));

            if(!$stmt->execute()) {
                throw new \Exception($stmt->errorInfo()[2]);
            }

            $stmt = $this->conn->prepare(sprintf(
                'CREATE TABLE `%s`.`translator_services` (
                    SID TINYINT(11) PRIMARY KEY AUTO_INCREMENT,
                    sName VARCHAR(255) UNIQUE NOT NULL,
                    sAPIKey VARCHAR(255)
            ) CHARACTER SET utf8 COLLATE utf8_unicode_ci; ', $this->dbName));

            if(!$stmt->execute()) {
                throw new \Exception($stmt->errorInfo()[2]);
            }

            $stmt = $this->conn->prepare(sprintf(
                'CREATE TABLE `%s`.`services_translations` (
                      SID TINYINT(11) NOT NULL,
                      LID TINYINT(4) NOT NULL,
                      translation TEXT,
                      sourceLID TINYINT(4) NOT NULL,
                      sourceTranslation TEXT,
                      PRIMARY KEY(SID, LID),
                      FOREIGN KEY (SID) REFERENCES translator_services(SID),
                      FOREIGN KEY (LID) REFERENCES languages(LID),
                      FOREIGN KEY (sourceLID) REFERENCES languages(LID)
            ) CHARACTER SET utf8 COLLATE utf8_unicode_ci; ', $this->dbName));

            if(!$stmt->execute()) {
                throw new \Exception($stmt->errorInfo()[2]);
            }

            $stmt = $this->conn->prepare(sprintf(
                'CREATE TABLE `%s`.`settings` (
                      chosenSID TINYINT(11) NOT NULL,
                      mainLangID TINYINT(4) NOT NULL,
                      transType ENUM (\'fillempty\', \'overwrite\'),
                      FOREIGN KEY (chosenSID) REFERENCES translator_services(SID),
                      FOREIGN KEY (mainLangID) REFERENCES languages(LID)
            ) CHARACTER SET utf8 COLLATE utf8_unicode_ci; ', $this->dbName));

            if(!$stmt->execute()) {
                throw new \Exception($stmt->errorInfo()[2]);
            }

            $stmt = $this->conn->prepare(sprintf(
                'CREATE TABLE `%s`.`bing_languages` (
                      lang VARCHAR(8) NOT NULL PRIMARY KEY
            ) ', $this->dbName));

            if(!$stmt->execute()) {
                throw new \Exception($stmt->errorInfo()[2]);
            }


        } catch (\PDOException $e) {
            throw new \Exception("Query error. Message: %s", $e->getMessage());
        }
    }

    private function insertDefaultData()
    {
        $stmt = $this->conn->prepare(sprintf('INSERT INTO %s.translator_services VALUES(NULL, "bing", NULL)', $this->dbName));

        if(!$stmt->execute()) {
            throw new \Exception($stmt->errorInfo()[2]);
        }

        $stmt = $this->conn->prepare(sprintf('INSERT INTO %s.bing_languages VALUES (\'af\'),(\'ar\'),(\'bg\'),(\'bn\'),
            (\'bs\'),(\'ca\'),(\'cs\'),(\'cy\'),(\'da\'),(\'de\'),(\'el\'),(\'en\'),(\'es\'),(\'et\'),(\'fa\'),(\'fi\'),
            (\'fil\'),(\'fj\'),(\'fr\'),(\'he\'),(\'hi\'),(\'hr\'),(\'ht\'),(\'hu\'),(\'id\'),(\'is\'),(\'it\'),(\'ja\'),
            (\'ko\'),(\'lt\'),(\'lv\'),(\'mg\'),(\'ms\'),(\'mt\'),(\'mww\'),(\'nb\'),(\'nl\'),(\'oqt\'),(\'pl\'),(\'pt\'),
            (\'ro\'),(\'ru\'),(\'sk\'),(\'sl\'),(\'sm\'),(\'sr-Cyrl\'),(\'sr-Latn\'),(\'sv\'),(\'sw\'),(\'ta\'),(\'th\'),
            (\'tlh\'),(\'tlh-Qaak\'),(\'to\'),(\'tr\'),(\'ty\'),(\'uk\'),(\'ur\'),(\'vi\'),(\'yua\'),(\'yue\'),
            (\'zh-Hans\'),(\'zh-Hant\');', $this->dbName));

        if(!$stmt->execute()) {
            throw new \Exception($stmt->errorInfo()[2]);
        }

        $stmt = $this->conn->prepare(sprintf('INSERT INTO %s.languages VALUES(NULL, "gb", "english-british language","en")', $this->dbName));

        if(!$stmt->execute()) {
            throw new \Exception($stmt->errorInfo()[2]);
        }

        $stmt = $this->conn->prepare(sprintf('INSERT INTO %s.settings VALUES(1, 1, NULL)', $this->dbName));

        if(!$stmt->execute()) {
            throw new \Exception($stmt->errorInfo()[2]);
        }
    }


    private function validateDBName($dbName)
    {
        //TODO DO NOT FORGET!
        return true;
    }

    public function isSuccessful()
    {
        return $this->success;
    }

}


