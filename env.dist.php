<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 30.4.2018 г.
 * Time: 21:46 ч.
 */

define('APP_ENV', 'dev');

define('MAIN_DB', '__translations');

define('DOMAIN', 'http://localization.universallib.tk/');
define('URL_CSS', DOMAIN.'assets/css/');
define('URL_JS', DOMAIN.'assets/js/');
define('URL_VENDORS', DOMAIN.'assets/vendors/');
define('URL_BOOTSTRAP', DOMAIN.'assets/vendors/bootstrap/');
define('URL_JQUERY', DOMAIN.'assets/vendors/jquery/');
define('URL_FONTAWESOME', DOMAIN.'assets/vendors/fontawesome/');
define('TEMPLATES_PATH', __DIR__.'/templates/');

define('ADMIN_EMAIL', 'example@example.com');
define('LOGS_PATH', __DIR__.'/logs/');