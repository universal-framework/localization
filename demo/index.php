<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 20.6.2018 г.
 * Time: 21:31 ч.
 */

include_once '../mainloader.php';

use Localization\Utils\PDOWrapper;
use Localization\Page\PageFetcher;
use Localization\Translator\TransContainer;

$pName = 'Main Page';

if (!isset($_GET['lang'])) {
    $chosenLang = 'gb';
} else {
    $chosenLang = $_GET['lang'];
}

$pageFetcher = new PageFetcher(PDOWrapper::getInstance(), MAIN_DB);
try {
    $translations = $pageFetcher->getTranslationsByName($chosenLang, $pName);
    $transContainer = new TransContainer($translations);
    $langs = $pageFetcher->getAvailableLanguages();
} catch (\Exception $e) {
    die ($e->getMessage());
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<title>Demo project</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="styles/layout.css" type="text/css">
<!--[if lt IE 9]><script src="scripts/html5shiv.js"></script><![endif]-->
</head>
<body>
<div class="wrapper row1">
  <header id="header" class="clear">
    <div id="hgroup">
      <h1><a href="#"><?php echo $transContainer->get('headerLogo', 'header') ?></a></h1>
      <h2><?php echo $transContainer->get('headerSubText', 'header') ?></h2>
    </div>
      <form action="#">
          <fieldset style="margin-left: 5px;">
              <legend>Language:</legend>
              <select name="langs" onchange="changeLang(this)" style="width: 20%">
                  <?php foreach ($langs as $lang) { ?>
                      <option value="<?php echo htmlentities($lang) ?>"
                              <?php echo ($chosenLang === $lang)? 'selected' : '' ?>><?php echo htmlentities(strtolower($lang)) ?></option>
                  <?php }?>
              </select>
          </fieldset>
      </form>
    <form action="#" method="post">
      <fieldset>
        <legend>Search:</legend>
        <input type="text" value="<?php echo $transContainer->get('searchPlaceholder', 'header') ?>&hellip;"
               onFocus="this.value=(this.value=='Search Our Website&hellip;')? '' : this.value ;">
        <input type="submit" id="sf_submit" value="<?php echo $transContainer->get('submitButton', 'header') ?>">
      </fieldset>
    </form>

    <nav>
      <ul>
        <li><a href="#"><?php echo $transContainer->get('homeBtn', 'header') ?></a></li>
        <li><a href="#"><?php echo $transContainer->get('btnNews', 'header') ?></a></li>
        <li><a href="#"><?php echo $transContainer->get('btnTrading', 'header') ?></a></li>
        <li><a href="#"><?php echo $transContainer->get('btnEducation', 'header') ?></a></li>
          <li><a href="#"><?php echo $transContainer->get('btnCalendar', 'header') ?></a></li>
          <li><a href="#"><?php echo $transContainer->get('btnRates', 'header') ?></a></li>
          <li><a href="#"><?php echo $transContainer->get('btnTools', 'header') ?></a></li>
        <li><a href="#"><?php echo $transContainer->get('btnCommunity', 'header') ?></a></li>
          <li class="last"><?php echo $transContainer->get('btnCommunityNew', 'header') ?></li>

      </ul>
    </nav>
  </header>
</div>

<!-- content -->
<div class="wrapper row2">
    <div id="container" class="clear">
        <!-- Slider -->
        <section id="slider" class="clear">
            <figure><img src="images/demo/630x300.gif" alt="">
                <figcaption>
                    <h2><?php echo $transContainer->get('testTitle', 'extra') ?></h2>
                    <p><?php echo $transContainer->get('testContent', 'extra') ?></p>
                    <footer class="more"><a href="#"><?php echo $transContainer->get('linkReadMore', 'main') ?> &raquo;</a></footer>
                </figcaption>
            </figure>
        </section>
        <!-- main content -->
        <div id="intro">
            <section class="clear">
                <!-- article 1 -->
                <article class="two_quarter">
                    <h2><?php echo $transContainer->get('extraTitle', 'main') ?></h2>
                    <?php echo $transContainer->get('extraContent', 'extra') ?>
                </article>
                <!-- article 2 -->
                <article class="two_quarter lastbox">
                    <figure>
                        <ul class="clear">
                            <li><a href="#"><img src="images/demo/130x130.gif" width="130" height="130" alt=""></a></li>
                            <li><a href="#"><img src="images/demo/130x130.gif" width="130" height="130" alt=""></a></li>
                            <li class="last"><a href="#"><img src="images/demo/130x130.gif" width="130" height="130" alt=""></a></li>
                        </ul>
                        <figcaption><a href="#"><?php echo $transContainer->get('btnViewGallery', 'main') ?> &raquo;</a></figcaption>
                    </figure>
                </article>
            </section>
        </div>
        <!-- ########################################################################################## -->
        <!-- ########################################################################################## -->
        <!-- ########################################################################################## -->
        <!-- ########################################################################################## -->
        <div id="homepage" class="last clear">
            <section class="one_quarter">
                <h2 class="title"><?php echo $transContainer->get('txtFromTheBlock', 'main') ?></h2>
                <article>
                    <header>
                        <h2><?php echo $transContainer->get('artTitle', 'main') ?></h2>
                        <address>
                            <a href="#">Admin</a>, domainname.com
                        </address>
                        <time datetime="2000-04-06">Friday, 6<sup>th</sup> April 2000</time>
                    </header>
                    <p><?php echo $transContainer->get('artText', 'main') ?></p>
                    <footer class="more"><a href="#"><?php echo $transContainer->get('linkReadMore', 'main') ?> &raquo;</a></footer>
                </article>
            </section>
            <section class="one_quarter">
                <h2 class="title"><?php echo $transContainer->get('txtQuickLinks') ?></h2>
                <nav>
                    <ul>
                        <li><a href="#"><?php echo $transContainer->get('homeBtn', 'footer') ?></a></li>
                        <li><a href="#"><?php echo $transContainer->get('btnTrading', 'footer') ?></a></li>
                        <li><a href="#"><?php echo $transContainer->get('btnCalendar', 'footer') ?></a></li>
                        <li><a href="#"><?php echo $transContainer->get('btnCommunity', 'footer') ?></a></li>
                        <li class="last"><a href="#"><?php echo $transContainer->get('btnTools', 'footer') ?></a></li>
                    </ul>
                </nav>
            </section>
            <section class="two_quarter lastbox">
                <h2 class="title"><?php echo $transContainer->get('txtAboutUs', 'main') ?></h2>
                <img class="imgl" src="images/demo/130x130.gif" width="130" height="130" alt="">
                <p><?php echo $transContainer->get('contAboutUs', 'main') ?></p>
                <footer class="more"><a href="#"><?php echo $transContainer->get('linkReadMore', 'main') ?> &raquo;</a></footer>
            </section>
        </div>
        <!-- / content body -->
    </div>
</div>

<div class="wrapper row3">
    <footer id="footer" class="clear">
        <p class="fl_left"><?php echo $transContainer->get('txtCopyright', 'footer') ?> - <a href="#">Localization test host</a></p>
        <p class="fl_right"><?php echo $transContainer->get(' txtTemplateBy', 'footer') ?><a href="http://www.os-templates.com/" title="Free Website Templates">OS Templates</a></p>
    </footer>
</div>
</body>
</html>

<script type="text/javascript" src="scripts/main.js">
</script>

