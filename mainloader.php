<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 30.4.2018 г.
 * Time: 22:21 ч.
 */

$GLOBALS['start'] = microtime(true);

require_once(__DIR__.'/vendor/autoload.php');

require_once(__DIR__.'/.env.php');


if (APP_ENV === 'dev') {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}

session_start();
