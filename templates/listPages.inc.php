<div class="right_col" role="main">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Pages list(<?php echo $pagesCount ?>)</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdownable dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="pages.php?create">Create new</a>
                                </li>
                                <li><a href="pages.php?delete=all" class="delete-templates">Delete all</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-times"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <?php if ($pagesCount == 0) { ?>
                        No pages created so far. <a href="pages.php?create">Create</a>
                    <?php } else { ?>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Label</th>
                                <th scope="col">Description</th>
                                <th scope="col">Header Template</th>
                                <th scope="col">Main template</th>
                                <th scope="col">Footer template</th>
                                <th scope="col">Custom Translations count</th>
                                <th scope="col">Options</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php foreach($pageData as $page) { ?>
                                <tr>
                                    <th scope="row"><?php echo $page['PID'] ?></th>
                                    <td><?php echo htmlspecialchars($page['pLabel']) ?></td>
                                    <td><?php echo htmlspecialchars($page['pDesc']) ?></td>
                                    <td><a href="templates.php?overview=<?php echo $page['pTempHeaderID'] ?>"
                                        ><?php echo htmlentities($page['pTempHeaderName']) ?></a></td>
                                    <td><a href="templates.php?overview=<?php echo $page['pTempMainID'] ?>"
                                        ><?php echo htmlentities($page['pTempMainName']) ?></a></td>
                                    <td><a href="templates.php?overview=<?php echo $page['pTempFooterID'] ?>"
                                        ><?php echo htmlentities($page['pTempFooterName']) ?></a></td>
                                    <td><?php echo $page['pTransCount'] ?></td>
                                    <td>
                                        <a href="pages.php?overview=<?php echo $page['PID'] ?>" class="fa fa-info" title="Overview"></a>
                                        <a href="pages.php?update=<?php echo $page['PID'] ?>" class="fa fa-edit" title="Edit"></a>
                                        <a href="pages.php?delete=<?php echo $page['PID'] ?>" class="fa fa-trash" title="Delete"></a>
                                    </td>
                                </tr>
                            <?php } ?>

                            </tbody>
                        </table>

                    <?php } ?>
                </div>
            </div>
        </div>

    </div>

</div>
