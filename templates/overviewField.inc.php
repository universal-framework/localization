
<div class="right_col" role="main">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?php echo sprintf('Overview field %s',$fieldData['label'])?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdownable dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="fields.php?update=<?php echo $fieldData['id'] ?>">Update</a>
                                </li>
                                <li><a href="fields.php?update=<?php echo $fieldData['id'] ?>" class="delete-templates">Delete</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-times"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <?php if ($fieldData['id'] > 0) { ?>

                    <fieldset>
                        <div class="control-group">
                            <!-- Label -->
                            <label class="control-label" for="label">Label</label>
                            <div class="controls">
                                <input type="text" id="label" disabled
                                       value="<?php echo htmlentities($fieldData['label']) ?>"
                                       name="label" placeholder="" maxlength="255" class="full_field form-control">
                            </div>
                        </div>

                        <?php if (count($langs) > 0) { ?>
                            <?php foreach ($langs as $langData) { ?>
                                <div class="control-group">
                                    <!-- Label -->
                                    <span title="<?php echo htmlspecialchars($langData['lLangDesc']) ?>"
                                          class="flag-icon flag-icon-<?php echo htmlspecialchars(strtolower($langData['lLangAbbv'])) ?>"></span>
                                    <label class="control-label" for="label"><?php echo htmlspecialchars($langData['lLangAbbv']) ?></label>
                                    <div class="controls">
                                        <input type="text" id="label" disabled
                                               value="<?php echo (isset($translations[$langData['LID']])) ?
                                                   htmlentities($translations[$langData['LID']]) : ''?>"
                                               name="lang_<?php echo htmlspecialchars($langData['LID']) ?>" placeholder=""
                                               maxlength="255" class="full_field form-control">
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>

                        <br />

                    </fieldset>

                    <?php } else {?>
                        No such field!
                    <?php } ?>
                </div>
            </div>
        </div>

    </div>

</div>
