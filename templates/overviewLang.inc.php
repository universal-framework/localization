
<div class="right_col" role="main">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?php echo sprintf('Overview lang %s',$langData['abbv'])?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-times"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <?php if (false === empty($langData['abbv'])) { ?>
                        <fieldset>
                            <div class="control-group">
                                <!-- Label -->

                                <span title="<?php echo htmlspecialchars($langData['desc']) ?>"
                                      class="flag-icon flag-icon-<?php
                                      echo htmlspecialchars(strtolower($langData['abbv'])) ?>"></span>

                                <label class="control-label" for="label">Abbv</label>
                                <div class="controls">
                                    <input type="text" disabled id="abbv"
                                           value="<?php echo htmlentities($langData['abbv'])?>"
                                           name="abbv" placeholder="" maxlength="2" class="full_field form-control">
                                </div>
                            </div>

                            <div class="control-group">
                                <!-- Description -->
                                <label class="control-label" for="desc">Description</label>
                                <div class="controls">
                                    <textarea class="form-control" rows="5" id="desc" disabled name="desc"><?php
                                        echo htmlentities($langData['desc'])?></textarea>
                                </div>
                            </div>

                            <div class="control-group">
                                <!-- Code -->

                                <label class="control-label" for="code">Code</label>
                                <div class="controls">
                                    <input type="text" disabled id="code"
                                           value="<?php echo htmlentities($langData['code'])?>"
                                           name="code" placeholder="" maxlength="3" class="full_field form-control">
                                </div>
                            </div>

                        </fieldset>
                    <?php } else {?>
                        No such Lang!
                    <?php } ?>
                </div>
            </div>
        </div>

    </div>

</div>
