
<div class="right_col" role="main">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?php echo (isset($update) && $update === true) ?
                            sprintf('Update field %s',$fieldData['label']) : 'Create Field' ?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-times"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form class="form-horizontal" action='' method="POST">
                        <input type="hidden" name="CSRF_TOKEN" value="<?php echo $token ?>" />
                        <input type="hidden" name="chosen_lang" value="<?php echo $chosenLang ?>" disabled />
                        <fieldset>
                            <div class="control-group">
                                <!-- Label -->
                                <label class="control-label" for="label">Label</label>
                                <div class="controls">
                                    <input type="text" id="label"
                                           value="<?php echo (isset($update) && $update === true) ? htmlentities($fieldData['label']) : ''?>"
                                           name="label" placeholder="" maxlength="255" class="full_field form-control">
                                    <p class="help-block">Label may contain up to 255 characters from A-Z, 0-9</p>
                                </div>
                            </div>

                            <?php if (count($langs) > 0) { ?>
                                <?php foreach ($langs as $langData) { ?>
                                    <div class="control-group">
                                        <!-- Label -->
                                        <span title="<?php echo htmlspecialchars($langData['lLangDesc']) ?>"
                                              class="flag-icon flag-icon-<?php echo htmlspecialchars(strtolower($langData['lLangAbbv'])) ?>"></span>
                                        <label class="control-label" for="label"><?php echo htmlspecialchars($langData['lLangAbbv']) ?></label>
                                        <div class="controls">
                                            <input type="text" id="label"
                                                   value="<?php echo (isset($update) && $update === true && isset($translations[$langData['LID']]))
                                                       ? htmlentities($translations[$langData['LID']]) : ''?>"
                                                   name="lang_<?php echo htmlspecialchars($langData['LID']) ?>" placeholder=""
                                                   maxlength="255" class="full_field form-control">
                                            <p class="help-block">Translation may contain up to 255 characters from A-Z, 0-9</p>
                                        </div>
                                    </div>

                                    <div class="control-group trans_fields">
                                        <!-- Service translations -->
                                        <span title="<?php echo htmlspecialchars($langData['lLangDesc']) ?>"
                                              class="flag-icon flag-icon-<?php echo htmlspecialchars(strtolower($langData['lLangAbbv'])) ?>"></span>
                                        <label class="control-label" for="label"><?php echo htmlspecialchars($langData['lLangAbbv']) ?></label>
                                        <div class="controls">
                                            <input type="text" id="label"
                                                   value="<?php echo (isset($update) && $update === true && isset($serviceTrans[$langData['LID']]))
                                                       ? htmlentities($serviceTrans[$langData['LID']]) : ''?>" disabled
                                                   name="service_trans_<?php echo htmlspecialchars($langData['LID']) ?>" placeholder=""
                                                   maxlength="255" class="option_field form-control"><i class="fa fa-sync fa-2x option_field_icon trans_icon"></i>
                                            <i class="fa fa-arrow-up fa-2x option_field_icon trans_apply"></i>
                                            <div class="clearfix"></div>
                                            <p class="help-block">Click the generate button to use the automated translation</p>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>

                            <br />

                            <div class="control-group">
                                <!-- Button -->
                                <div class="controls">
                                    <button class="btn btn-success" type="submit">Save</button>
                                    <button class="btn btn-primary" disabled style="float: right" id="subsbtn">Substitute all</button>
                                    <button class="btn btn-primary" type="button" style="float: right" id="transbtn">Translate all</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>

    </div>

</div>
