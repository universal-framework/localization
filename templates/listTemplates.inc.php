<div class="right_col" role="main">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Templates list(<?php echo $templatesCount ?>)</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdownable dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="templates.php?create">Create new</a>
                                </li>
                                <li><a href="templates.php?delete=all" class="delete-templates">Delete all</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-times"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <?php if (count($templatesData) == 0) { ?>
                        No templates created so far. <a href="templates.php?create">Create</a>
                    <?php } else { ?>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Label</th>
                                <th scope="col">Description</th>
                                <th scope="col">Template type</th>
                                <th scope="col">Fields count</th>
                                <th scope="col">Options</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php foreach($templatesData as $row) { ?>
                                    <tr>
                                        <th scope="row"><?php echo $row['TID'] ?></th>
                                        <td><?php echo htmlentities($row['tUniqueLabel']) ?></td>
                                        <td><?php echo htmlentities($row['tDesc']) ?></td>
                                        <td><?php echo htmlentities($row['fullTempType']) ?></td>
                                        <td><?php echo $row['fCount'] ?></td>
                                        <td>
                                            <a href="templates.php?overview=<?php echo $row['TID'] ?>" class="fa fa-info" title="Overview"></a>
                                            <a href="templates.php?update=<?php echo $row['TID'] ?>" class="fa fa-edit" title="Edit"></a>
                                            <a href="templates.php?delete=<?php echo $row['TID'] ?>" class="fa fa-trash" title="Delete"></a>
                                        </td>
                                    </tr>
                            <?php } ?>

                            </tbody>
                        </table>

                    <?php } ?>
                </div>
            </div>
        </div>

    </div>

</div>
