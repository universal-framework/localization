
<div class="right_col" role="main">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?php echo (isset($update) && $update === true) ?
                        sprintf('Update template %s',$tempInfo['label']) : 'Create template' ?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-times"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form class="form-horizontal" action='' method="POST">
                        <input type="hidden" name="CSRF_TOKEN" value="<?php echo $token ?>" />
                        <fieldset>
                            <div class="control-group">
                                <!-- Label -->
                                <label class="control-label" for="label">Label</label>
                                <div class="controls">
                                    <input type="text" id="label"
                                           value="<?php echo (isset($update) && $update === true) ? htmlentities($tempInfo['label']) : ''?>"
                                           name="label" placeholder="" maxlength="255" class="full_field form-control">
                                    <p class="help-block">Label may contain up to 255 characters from A-Z, 0-9</p>
                                </div>
                            </div>

                            <div class="control-group">
                                <!-- Description -->
                                <label class="control-label" for="desc">Description</label>
                                <div class="controls">
                                    <textarea class="form-control" rows="5" id="desc" name="desc"><?php
                                        echo (isset($update) && $update === true) ? htmlentities($tempInfo['desc']) : ''?></textarea>
                                    <p class="help-block">Write a meaningful description</p>
                                </div>
                            </div>

                            <div class="control-group">
                                <!-- Fields-->
                                <label class="control-label" for="fields">Fields</label>
                                <select title="fields" class="fields-combo full_field form-control" name="fields[]" multiple="multiple">
                                    <?php if (count($fields) > 0) { ?>
                                        <?php foreach ($fields as $field) { ?>
                                            <option value="<?php echo $field['FID'] ?>"
                                            <?php
                                                echo (isset($update) && $update === true &&
                                                    in_array($field['FID'], $tempInfo['fields'])) ? 'selected' : ''
                                                ?>
                                            ><?php echo $field['fUniqueLabel'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="tempType">Template type</label>
                                <div class="radio">
                                    <label><input type="radio" name="tempType"
                                                 <?php echo (isset($tempInfo) && $tempInfo['ttype'] == 'h')? 'checked' : ''?>
                                                 value="h">Header</label>
                                </div>
                                <div class="radio">
                                    <label><input type="radio" name="tempType"
                                                 <?php echo (isset($tempInfo) && $tempInfo['ttype'] == 's')? 'checked' : ''?>
                                                  value="s">Standard</label>
                                </div>
                                <div class="radio">
                                    <label><input type="radio" name="tempType"
                                                  <?php echo (isset($tempInfo) && $tempInfo['ttype'] == 'f')? 'checked' : ''?>
                                                  value="f">Footer</label>
                                </div>
                            </div>

                            <br />

                            <div class="control-group">
                                <!-- Button -->
                                <div class="controls">
                                    <button class="btn btn-success">Save</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>

    </div>

</div>
