<div class="right_col" role="main">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Overview template <?php echo htmlentities($tempInfo['label']) ?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdownable dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="templates.php?create">Create new</a>
                                </li>
                                <li><a href="templates.php?update=<?php echo $tempInfo['id']?>">Update</a>
                                </li>
                                <li><a href="templates.php?delete=all" class="delete-templates">Delete all</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-times"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <fieldset>
                        <div class="control-group">
                            <!-- Label -->
                            <label class="control-label" for="label">Label</label>
                            <div class="controls">
                                <input disabled type="text" id="label"
                                       value="<?php echo htmlentities($tempInfo['label'])?>"
                                       name="label" placeholder="" maxlength="255" class="full_field form-control">
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="ttype">Template type</label>
                            <div class="controls">
                                <input type="text" id="ttype" disabled
                                       value="<?php echo htmlentities($tempInfo['fullTempType']) ?>"
                                        name = "ttype" class="full_field form-control">
                            </div>
                        </div>

                        <div class="control-group">
                            <!-- Description -->
                            <label class="control-label" for="desc">Description</label>
                            <div class="controls">
                                    <textarea class="form-control" disabled rows="5" id="desc" name="desc"><?php
                                        echo htmlentities($tempInfo['desc']) ?></textarea>
                            </div>
                        </div>

                    </fieldset>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Fields</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdownable dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="fields.php?create">Create new</a>
                                </li>
                                <li><a href="fields.php?delete=all" class="delete-fields">Delete all</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-times"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <?php if (isset($tempInfo['fields']) && count($tempInfo['fields']) == 0) { ?>
                        No fields assigned so far.
                    <?php } else { ?>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Label</th>
                                <th scope="col">Number of translations</th>
                                <th scope="col">Options</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php foreach($tempInfo['fields'] as $row) { ?>
                                <tr>
                                    <th scope="row"><?php echo $row['id'] ?></th>
                                    <td><?php echo $row['label'] ?></td>
                                    <td><?php echo $row['fCount'] ?></td>
                                    <td>
                                        <a href="fields.php?overview=<?php echo $row['id'] ?>" class="fa fa-info" title="Overview"></a>
                                        <a href="fields.php?update=<?php echo $row['id'] ?>" class="fa fa-edit" title="Edit"></a>
                                        <a href="fields.php?delete=<?php echo $row['id'] ?>" class="fa fa-trash" title="Delete"></a>
                                    </td>
                                </tr>
                            <?php } ?>

                            </tbody>
                        </table>

                    <?php } ?>
                </div>
            </div>
        </div>

    </div>

</div>
