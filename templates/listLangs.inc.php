<div class="right_col" role="main">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Languages list(<?php echo $langsCount ?>)</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdownable dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="langs.php?add">Add new</a>
                                </li>
                                <li><a href="langs.php?delete=all" class="delete-templates">Delete all</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-times"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <?php if ($langsCount == 0) { ?>
                        No Languages set so far. <a href="langs.php?add">Add</a>
                    <?php } else { ?>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Abbv</th>
                                <th scope="col">Code</th>
                                <th scope="col">Desc</th>
                                <th scope="col">Options</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php foreach($langsData as $row) { ?>
                                <tr>
                                    <th scope="row"><?php echo $row['LID'] ?></th>
                                    <td><?php echo htmlspecialchars($row['lLangAbbv']) ?></td>
                                    <td><?php echo htmlspecialchars($row['lBingRef']) ?></td>
                                    <td><?php echo htmlspecialchars($row['lLangDesc']) ?></td>
                                    <td>
                                        <a href="langs.php?overview=<?php echo $row['LID'] ?>" class="fa fa-info" title="Overview"></a>
                                        <a href="langs.php?update=<?php echo $row['LID'] ?>" class="fa fa-edit" title="Edit"></a>
                                        <a href="langs.php?delete=<?php echo $row['LID'] ?>" class="fa fa-trash" title="Delete"></a>
                                    </td>
                                </tr>
                            <?php } ?>

                            </tbody>
                        </table>

                    <?php } ?>
                </div>
            </div>
        </div>

    </div>

</div>
