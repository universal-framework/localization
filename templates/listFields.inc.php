<div class="right_col" role="main">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Fields list(<?php echo $fieldsCount ?>)</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdownable dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="fields.php?create">Create new</a>
                                </li>
                                <li><a href="fields.php?delete=all" class="delete-templates">Delete all</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-times"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <?php if (count($fieldsData) == 0) { ?>
                        No templates created so far. <a href="fields.php?create">Create</a>
                    <?php } else { ?>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Label</th>
                                <th scope="col">Translations count</th>
                                <th scope="col">Options</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php foreach($fieldsData as $row) { ?>
                                <tr>
                                    <th scope="row"><?php echo $row['FID'] ?></th>
                                    <td><?php echo htmlspecialchars($row['fUniqueLabel']) ?></td>
                                    <td><?php echo $row['tCount'] ?></td>
                                    <td>
                                        <a href="fields.php?overview=<?php echo $row['FID'] ?>" class="fa fa-info" title="Overview"></a>
                                        <a href="fields.php?update=<?php echo $row['FID'] ?>" class="fa fa-edit" title="Edit"></a>
                                        <a href="fields.php?delete=<?php echo $row['FID'] ?>" class="fa fa-trash" title="Delete"></a>
                                    </td>
                                </tr>
                            <?php } ?>

                            </tbody>
                        </table>

                    <?php } ?>
                </div>
            </div>
        </div>

    </div>

</div>
