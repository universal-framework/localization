
<div class="right_col" role="main">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?php echo (isset($update) && $update === true) ?
                            sprintf('Update language %s',$langData['abbv']) : 'Add Language' ?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-times"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form class="form-horizontal" action='' method="POST">
                        <input type="hidden" name="CSRF_TOKEN" value="<?php echo $token ?>" />
                        <?php if (isset($update) && $update === true) { ?>
                            <input type="hidden" disabled name="lang_id" value="<?php echo $langData['id'] ?>"
                        <?php } ?>
                        <fieldset>
                            <div class="control-group">
                                <!-- Label -->
                                <?php if (isset($update) && $update === true) {?>
                                    <span title="<?php echo htmlspecialchars($langData['desc']) ?>"
                                          class="flag-icon flag-icon-<?php
                                          echo htmlspecialchars(strtolower($langData['abbv'])) ?>"></span>
                                <?php } ?>
                                <label class="control-label" for="label">Abbv</label>
                                <div class="controls">
                                    <input type="text" id="abbv"
                                           value="<?php echo (isset($update) && $update === true) ?
                                               htmlentities($langData['abbv']) : ''?>"
                                           name="abbv" placeholder="" maxlength="2" class="full_field form-control">
                                    <p class="help-block">Abbv may contain exactly 2 characters, uppercase!</p>
                                </div>
                            </div>

                            <div class="control-group">
                                <!-- Description -->
                                <label class="control-label" for="desc">Description</label>
                                <div class="controls">
                                    <textarea class="form-control" rows="5" id="desc" name="desc"><?php
                                        echo (isset($update) && $update === true) ? htmlentities($langData['desc']) : ''?></textarea>
                                    <p class="help-block">Write a meaningful description</p>
                                </div>
                            </div>

                            <div class="control-group">
                                <!-- Description -->
                                <label class="control-label" for="desc">Code</label>
                                <div class="controls">
                                    <input title="Code" class="form-control" id="code" name="code"
                                           value="<?php echo (isset($update) && $update === true) ? htmlentities($langData['code']) : ''?>" >
                                    <p class="help-block">Code that signifies </p>
                                </div>
                            </div>

                            <br />

                            <div class="control-group">
                                <!-- Button -->
                                <div class="controls">
                                    <button class="btn btn-success">Save</button>
                                    <?php if (isset($update) && $update === true) { ?>
                                        <button class="btn btn-danger" id="transAll" style="float: right;">Translate all</button>
                                    <?php } ?>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>

    </div>

</div>
