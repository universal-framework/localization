<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $title ?></title>
    <link rel="stylesheet" type="text/css" href="<?php echo URL_BOOTSTRAP.'css/bootstrap.min.css' ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo URL_FONTAWESOME.'fontawesome-all.min.css' ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo URL_CSS.'main.css'; ?>" />
    <?php if (isset($includeCSS) && count($includeCSS) > 0) { ?>
        <?php foreach ($includeCSS as $path) { ?>
            <link rel="stylesheet" type="text/css" href="<?php echo $path ?>" />
        <?php } ?>
    <?php } ?>
</head>
<body class="nav-md">

<?php if (false === empty($errors)) {?>
<div class="alert alert-danger">
    <strong>Error!</strong> <?php echo htmlentities($errors) ?>
</div>
<?php } ?>

<?php if (false === empty($successMessage)) {?>
    <div class="alert alert-success">
        <?php echo htmlentities($successMessage) ?>
    </div>
<?php } ?>
