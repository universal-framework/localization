<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="index.html" class="site_title"><i class="fa fa-flag"></i> <span>Localization</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">

                    <div class="profile_pic">
                        <i class="img-circle profile_img fa fa-user fa-3x"></i>
                    </div>

                    <div class="profile_info">
                        <span>Welcome,</span>
                        <h2><?php echo htmlspecialchars($userName) ?></h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">
                            <li><a href="<?php echo DOMAIN.'dashboard.php' ?>"><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a></li>
                            <li><a><i class="fa fa-columns"></i> Templates <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="<?php echo DOMAIN.'templates.php?list' ?>">List</a></li>
                                    <li><a href="<?php echo DOMAIN.'templates.php?create' ?>">Create</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-file-alt"></i> Pages <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="<?php echo DOMAIN.'pages.php?list' ?>">List</a></li>
                                    <li><a href="<?php echo DOMAIN.'pages.php?create' ?>">Create</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-table"></i> Fields <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="<?php echo DOMAIN.'fields.php?list' ?>">List</a></li>
                                    <li><a href="<?php echo DOMAIN.'fields.php?create' ?>">Create</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-language"></i> Languages <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="<?php echo DOMAIN.'langs.php?list' ?>">List</a></li>
                                    <li><a href="<?php echo DOMAIN.'langs.php?add' ?>">Add</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" href="<?php echo DOMAIN.'settings.php' ?>" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo DOMAIN.'logout.php' ?>">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>
