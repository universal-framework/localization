<!-- page content -->
<div class="right_col" role="main">
    <!-- top tiles -->
    <div class="row tile_count">
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-file-alt"></i> Total Pages</span>
            <div class="count"><?php echo $pagesCount ?></div>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-columns"></i> Total Templates</span>
            <div class="count"><?php echo $templatesCount ?></div>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-table"></i> Total Fields</span>
            <div class="count green"><?php echo $fieldsCount ?></div>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-language"></i> Total Languages</span>
            <div class="count"><?php echo $languagesCount ?></div>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Total Translations</span>
            <div class="count"><?php echo $translationsCount ?></div>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Total Page Translations</span>
            <div class="count"><?php echo $pageTransCount ?></div>
        </div>
    </div>
    <!-- /top tiles -->
</div>
<!-- /page content -->
