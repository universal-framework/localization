
<div class="right_col" role="main">

    <div class="row">

        <form class="form-horizontal" action='' method="POST">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?php echo (isset($update) && $update === true) ?
                            sprintf('Update page %s',$pageData['label']) : 'Create Page' ?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-times"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                        <input type="hidden" name="CSRF_TOKEN" value="<?php echo $token ?>" />
                        <input type="hidden" name="chosen_lang" value="<?php echo $chosenLang ?>" disabled />
                        <fieldset>
                            <div class="control-group">
                                <!-- Label -->
                                <label class="control-label" for="label">Label</label>
                                <div class="controls">
                                    <input type="text" id="label"
                                           value="<?php echo (isset($update) && $update === true) ? htmlentities($pageData['label']) : ''?>"
                                           name="label" placeholder="" maxlength="255" class="full_field form-control">
                                    <p class="help-block">Label may contain up to 255 characters from A-Z, 0-9</p>
                                </div>
                            </div>

                            <div class="control-group">
                                <!-- Description -->
                                <label class="control-label" for="desc">Description</label>
                                <div class="controls">
                                    <textarea class="form-control" rows="5" id="desc" name="desc"><?php
                                        echo (isset($update) && $update === true) ? htmlentities($pageData['desc']) : ''?></textarea>
                                    <p class="help-block">Write a meaningful description</p>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="headertemp">Header template</label>
                                <div class="controls">
                                    <select name="headerTemp" class="form-control">
                                        <option></option>
                                        <?php if (isset($templates['h']) && count($templates['h']) > 0) { ?>
                                            <?php foreach ($templates['h'] as $headerTempData) { ?>
                                                <option value="<?php echo $headerTempData['TID'] ?>"
                                                    <?php echo (isset($pageData['hTempID']) &&
                                                        $pageData['hTempID'] === $headerTempData['TID']) ? 'selected' : ''?> >
                                                    <?php echo htmlentities($headerTempData['tUniqueLabel']) ?>
                                                </option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="standardtemp">Main template</label>
                                <div class="controls">
                                    <select name="standardTemp" class="form-control">
                                        <option></option>
                                        <?php if (isset($templates['s']) && count($templates['s']) > 0) { ?>
                                            <?php foreach ($templates['s'] as $standardTempData) { ?>
                                                <option value="<?php echo $standardTempData['TID'] ?>"
                                                <?php echo (isset($pageData['mTempID']) &&
                                                    $pageData['mTempID'] === $standardTempData['TID']) ? 'selected' : ''?>
                                                ><?php echo htmlentities($standardTempData['tUniqueLabel']) ?>
                                                </option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="footerTemp">Footer template</label>
                                <div class="controls">
                                    <select name="footerTemp" class="form-control">
                                        <option></option>
                                        <?php if (isset($templates['f']) && count($templates['f']) > 0) { ?>
                                            <?php foreach ($templates['f'] as $footerTempData) { ?>
                                                <option value="<?php echo $footerTempData['TID'] ?>"
                                                <?php echo (isset($pageData['hTempID']) &&
                                                    $pageData['fTempID'] === $footerTempData['TID']) ? 'selected' : ''?>
                                                ><?php echo htmlentities($footerTempData['tUniqueLabel']) ?>
                                                </option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="additionalTrans">Additional fields</label>

                            <?php if (isset($extraFields) && count($extraFields) > 0) { ?>
                                <?php $lastElement = count($extraFields)-1; ?>
                                <?php foreach ($extraFields as $key => $fieldData) { ?>
                                    <div class="controls">
                                        <input type="text"
                                               id="field_<?php echo $fieldData['FID'] ?>"
                                               value="<?php echo htmlentities($fieldData['fUniqueLabel']) ?>"
                                               name="fields[]" placeholder="" maxlength="255"
                                               class="option_field form-control"><i class="fa <?php echo ($key == $lastElement) ?
                                                'fa-plus' : 'fa-times' ?> fa-2x option_field_icon"></i>
                                        <div class="clearfix"></div>
                                        <p class="help-block">Use additional fields, specific only for that page</p>
                                    </div>
                                <?php } ?>
                            <?php } else {  ?>

                                <div class="controls">
                                    <input type="text"
                                           value=""
                                           id="field_0"
                                           name="fields[]" placeholder="" maxlength="255"
                                           class="option_field form-control"><i class="fa fa-plus fa-2x option_field_icon"></i>
                                        <div class="clearfix"></div>
                                        <p class="help-block">Use additional fields, specific only for that page</p>
                                    </div>

                                <?php } ?>

                                </div>

                                <br />

                                <div class="control-group">
                                    <!-- Button -->
                                    <div class="controls">
                                        <button class="btn btn-success" id="createPageSave">Save</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>

            <?php if (isset($extraFields) && count($extraFields) > 0) { ?>
                    <?php foreach ($extraFields as $fieldData) { ?>
                    <div class="col-md-6 col-sm-6 col-xs-6 trans_<?php echo $fieldData['FID'] ?>">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Translations / <span class="fieldLabel"><?php echo htmlentities($fieldData['fUniqueLabel']) ?></span></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-times"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">

                                <?php if (count($langs) > 0) { ?>

                                    <fieldset>
                                        <?php foreach($langs as $langData) { ?>
                                            <div class="control-group">
                                                <!-- Label -->
                                                    <span title="<?php echo htmlspecialchars($langData['lLangDesc']) ?>"
                                                          class="flag-icon flag-icon-<?php echo htmlspecialchars(strtolower($langData['lLangAbbv'])) ?>"></span>
                                                <label class="control-label" for="label"><?php echo htmlspecialchars($langData['lLangAbbv']) ?></label>
                                                <div class="controls">
                                                    <input type="text" id="label"
                                                           value="<?php echo (isset($fieldData['translations'][$langData['lLangAbbv']])) ?
                                                           htmlentities($fieldData['translations'][$langData['lLangAbbv']]) : ''?>"
                                                           name="lang_<?php echo $fieldData['FID'] ?>_<?php echo htmlspecialchars($langData['LID']) ?>" placeholder=""
                                                           maxlength="255" class="full_field form-control">
                                                </div>
                                            </div>

                                        <div class="control-group trans_fields">
                                                <!-- Service translations -->
                                            <span title="<?php echo htmlspecialchars($langData['lLangDesc']) ?>"
                                              class="flag-icon flag-icon-<?php echo htmlspecialchars(strtolower($langData['lLangAbbv'])) ?>"></span>
                                                <label class="control-label" for="label"><?php echo htmlspecialchars($langData['lLangAbbv']) ?></label>
                                                <div class="controls">
                                                    <input type="text" id="label"
                                                           value="<?php echo (isset($update) && $update === true && isset($serviceTrans[$langData['LID']]))
                                                               ? htmlentities($serviceTrans[$langData['LID']]) : ''?>" disabled
                                                           name="service_trans_<?php echo $fieldData['FID'] ?>_<?php echo htmlspecialchars($langData['LID']) ?>" placeholder=""
                                                           maxlength="255" class="option_field form-control" style="width:85%"><i class="fa fa-sync fa-2x option_field_icon trans_icon_page"></i>
                                                    <i class="fa fa-arrow-up fa-2x option_field_icon trans_apply_page"></i>
                                                    <div class="clearfix"></div>
                                                    <p class="help-block">Click the generate button to use the automated translation</p>
                                                </div>
                                        </div>

                                        <?php } ?>

                                        <button class="btn btn-primary subsbtn_page" disabled style="float: right">Substitute all</button>
                                        <button class="btn btn-primary transbtn_page" type="button" style="float: right">Translate all</button>

                                    </fieldset>

                                <?php } else { ?>
                                    No languages defined!
                                <?php } ?>
                            </div>
                        </div>

                    </div>
                <?php } ?>
            <?php } else { ?>

                <div class="col-md-6 col-sm-6 col-xs-6 trans_0">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Translations / <span class="fieldLabel"></span></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <?php if (count($langs) > 0) { ?>

                                <fieldset>
                                    <?php foreach($langs as $langData) { ?>
                                        <div class="control-group">
                                            <!-- Label -->
                                                <span title="<?php echo htmlspecialchars($langData['lLangDesc']) ?>"
                                                      class="flag-icon flag-icon-<?php echo htmlspecialchars(strtolower($langData['lLangAbbv'])) ?>"></span>
                                            <label class="control-label" for="label"><?php echo htmlspecialchars($langData['lLangAbbv']) ?></label>
                                            <div class="controls">
                                                <input type="text" id="label"
                                                       value=""
                                                       name="lang_0_<?php echo htmlspecialchars($langData['LID']) ?>" placeholder=""
                                                       maxlength="255" class="full_field form-control">
                                            </div>
                                        </div>

                                        <div class="control-group trans_fields">
                                            <!-- Service translations -->
                                            <span title="<?php echo htmlspecialchars($langData['lLangDesc']) ?>"
                                                  class="flag-icon flag-icon-<?php echo htmlspecialchars(strtolower($langData['lLangAbbv'])) ?>"></span>
                                            <label class="control-label" for="label"><?php echo htmlspecialchars($langData['lLangAbbv']) ?></label>
                                            <div class="controls">
                                                <input type="text" id="label"
                                                       value="" disabled
                                                       name="service_trans_0_<?php echo htmlspecialchars($langData['LID']) ?>" placeholder=""
                                                       maxlength="255" class="option_field form-control" style="width:85%"><i class="fa fa-sync fa-2x option_field_icon trans_icon_page"></i>
                                                <i class="fa fa-arrow-up fa-2x option_field_icon trans_apply_page"></i>
                                                <div class="clearfix"></div>
                                                <p class="help-block">Click the generate button to use the automated translation</p>
                                            </div>
                                        </div>

                                    <?php } ?>

                                </fieldset>

                            <?php } else {?>
                                No languages defined!
                            <?php } ?>
                        </div>
                    </div>
                </div>

            <?php } ?>

    </div>

</div>
