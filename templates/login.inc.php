<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="<?php echo URL_CSS.'bootstrap.min.css' ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo URL_CSS.'login.css'; ?>" />
</head>
<body>

<?php if (false === empty($errors)) {?>
    <div class="alert alert-danger">
        <strong>Error!</strong> <?php echo $errors ?>
    </div>
<?php } ?>

<div class="container">
    <div class="login_container">
        <form method="POST" href="">
                <input type="hidden" name="CSRF_TOKEN" value="<?php echo $token ?>">
                <div class="form-group">
                    <label for="usr">Name:</label>
                    <input type="text" name="username" class="form-control" id="username">
                </div>
                <div class="form-group">
                    <label for="pass">Password:</label>
                    <input type="password" name="password" class="form-control" id="password">
                </div>

                <input type="submit" class="btn btn-primary btn-md" value="Sign in">
        </form>
    </div>
</div>

</body>
</html>