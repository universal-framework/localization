        <!-- footer content -->
        <footer>
            <div class="pull-right">
                Localization admin panel. All rights reserved
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>


<script src="<?php echo URL_JQUERY.'jquery-3.3.1.min.js'; ?>" type="text/javascript"></script>
<script src="<?php echo URL_JS.'main.js'; ?>" type="text/javascript"></script>
<?php if (isset($includeJS) && count($includeJS) > 0) { ?>
    <?php foreach ($includeJS as $path) { ?>
        <script type="text/javascript" src="<?php echo $path ?>"></script>
    <?php } ?>
<?php } ?>

</body>


</html>


<?php

if (APP_ENV === 'dev') {
    $end = microtime(true);

    echo sprintf("Script loaded in: %.2f", ( $end - $GLOBALS['start']));
}

?>