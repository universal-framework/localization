
<div class="right_col" role="main">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?php echo (isset($pageData['label'])) ? sprintf('Overview page %s',$pageData['label']) : ''?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdownable dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="pages.php?update=<?php echo $pageData['id'] ?>">Update</a>
                                </li>
                                <li><a href="pages.php?delete=<?php echo $pageData['id'] ?>" class="delete-templates">Delete</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-times"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <?php if (isset($pageData['id']) && $pageData['id'] > 0) { ?>

                        <fieldset>
                            <div class="control-group">
                                <!-- Label -->
                                <label class="control-label" for="label">Label</label>
                                <div class="controls">
                                    <input type="text" id="label" disabled
                                           value="<?php echo htmlentities($pageData['label']) ?>"
                                           name="label" placeholder="" maxlength="255" class="full_field form-control">
                                </div>
                            </div>

                            <div class="control-group">
                                <!-- Description -->
                                <label class="control-label" for="desc">Description</label>
                                <div class="controls">
                                    <textarea class="form-control" disabled rows="5" id="desc" name="desc"><?php
                                        echo htmlentities($pageData['desc']) ?></textarea>
                                    <p class="help-block">Write a meaningful description</p>
                                </div>
                            </div>

                            <div class="control-group">
                                <!-- Label -->
                                <label class="control-label" for="label">Header template</label>
                                <div class="controls">
                                    <input type="text" id="label" disabled
                                           value="<?php echo (isset($pageData['hTempName'])) ?
                                               htmlentities($pageData['hTempName']) : '' ?>"
                                           name="label" placeholder="" maxlength="255" class="full_field form-control">
                                </div>
                            </div>

                            <div class="control-group">
                                <!-- Label -->
                                <label class="control-label" for="label">Main template</label>
                                <div class="controls">
                                    <input type="text" id="label" disabled
                                           value="<?php echo (isset($pageData['mTempName'])) ?
                                               htmlentities($pageData['mTempName']) : '' ?>"
                                           name="label" placeholder="" maxlength="255" class="full_field form-control">
                                </div>
                            </div>

                            <div class="control-group">
                                <!-- Label -->
                                <label class="control-label" for="label">Footer template</label>
                                <div class="controls">
                                    <input type="text" id="label" disabled
                                           value="<?php echo (isset($pageData['fTempName'])) ?
                                               htmlentities($pageData['fTempName']) : '' ?>"
                                           name="label" placeholder="" maxlength="255" class="full_field form-control">
                                </div>
                            </div>

                        </fieldset>

                    <?php } else {?>
                        No such Page!
                    <?php } ?>
                </div>
            </div>
        </div>

        <?php if (count($pageData) > 0 && $pageData['hTempID']) { ?>

            <?php if (count($pageData['hTempTranslations']) > 0) { ?>
                <?php foreach($pageData['hTempTranslations'] as $fLabel => $translations) { ?>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Header Fields / <?php echo htmlentities($fLabel) ?></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-times"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">

                                <?php if (count($langs) > 0) { ?>

                                    <fieldset>
                                    <?php foreach($langs as $langData) { ?>
                                        <div class="control-group">
                                                <!-- Label -->
                                        <span title="<?php echo htmlspecialchars($langData['lLangDesc']) ?>"
                                              class="flag-icon flag-icon-<?php echo htmlspecialchars(strtolower($langData['lLangAbbv'])) ?>"></span>
                                                <label class="control-label" for="label"><?php echo htmlspecialchars($langData['lLangAbbv']) ?></label>
                                                <div class="controls">
                                                    <input type="text" id="label" disabled
                                                           value="<?php echo (isset($translations[$langData['LID']])) ?
                                                               htmlentities($translations[$langData['LID']]) : ''?>"
                                                           name="lang_<?php echo htmlspecialchars($langData['LID']) ?>" placeholder=""
                                                           maxlength="255" class="full_field form-control">
                                                </div>
                                        </div>

                                    <?php } ?>

                                    </fieldset>

                                <?php } else {?>
                                    No languages defined!
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>

        <?php } ?>

        <?php if (count($pageData) > 0 && $pageData['mTempID']) { ?>

            <?php if (count($pageData['mTempTranslations']) > 0) { ?>
                <?php foreach($pageData['mTempTranslations'] as $fLabel => $translations) { ?>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Main Fields / <?php echo htmlentities($fLabel) ?></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-times"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">

                                <?php if (count($langs) > 0) { ?>

                                    <fieldset>
                                        <?php foreach($langs as $langData) { ?>
                                            <div class="control-group">
                                                <!-- Label -->
                                        <span title="<?php echo htmlspecialchars($langData['lLangDesc']) ?>"
                                              class="flag-icon flag-icon-<?php echo htmlspecialchars(strtolower($langData['lLangAbbv'])) ?>"></span>
                                                <label class="control-label" for="label"><?php echo htmlspecialchars($langData['lLangAbbv']) ?></label>
                                                <div class="controls">
                                                    <input type="text" id="label" disabled
                                                           value="<?php echo (isset($translations[$langData['LID']])) ?
                                                               htmlentities($translations[$langData['LID']]) : ''?>"
                                                           name="lang_<?php echo htmlspecialchars($langData['LID']) ?>" placeholder=""
                                                           maxlength="255" class="full_field form-control">
                                                </div>
                                            </div>

                                        <?php } ?>

                                    </fieldset>

                                <?php } else {?>
                                    No languages defined!
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>

        <?php } ?>

        <?php if (count($pageData) > 0 && $pageData['fTempID']) { ?>

            <?php if (count($pageData['fTempTranslations']) > 0) { ?>
                <?php foreach($pageData['fTempTranslations'] as $fLabel => $translations) { ?>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Footer Fields / <?php echo htmlentities($fLabel) ?></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-times"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">

                                <?php if (count($langs) > 0) { ?>

                                    <fieldset>
                                        <?php foreach($langs as $langData) { ?>
                                            <div class="control-group">
                                                <!-- Label -->
                                        <span title="<?php echo htmlspecialchars($langData['lLangDesc']) ?>"
                                              class="flag-icon flag-icon-<?php echo htmlspecialchars(strtolower($langData['lLangAbbv'])) ?>"></span>
                                                <label class="control-label" for="label"><?php echo htmlspecialchars($langData['lLangAbbv']) ?></label>
                                                <div class="controls">
                                                    <input type="text" id="label" disabled
                                                           value="<?php echo (isset($translations[$langData['LID']])) ?
                                                               htmlentities($translations[$langData['LID']]) : ''?>"
                                                           name="lang_<?php echo htmlspecialchars($langData['LID']) ?>" placeholder=""
                                                           maxlength="255" class="full_field form-control">
                                                </div>
                                            </div>

                                        <?php } ?>

                                    </fieldset>

                                <?php } else {?>
                                    No languages defined!
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>

        <?php } ?>

        <?php if (count($pageData) > 0 && $pageData['extraTranslations']) { ?>

            <?php if (count($pageData['extraTranslations']) > 0) { ?>
                <?php foreach($pageData['extraTranslations'] as $exLabel => $translations) { ?>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Extra Fields / <?php echo htmlentities($exLabel) ?></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-times"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">

                                <?php if (count($langs) > 0) { ?>

                                    <fieldset>
                                        <?php foreach($langs as $langData) { ?>
                                            <div class="control-group">
                                                <!-- Label -->
                                        <span title="<?php echo htmlspecialchars($langData['lLangDesc']) ?>"
                                              class="flag-icon flag-icon-<?php echo htmlspecialchars(strtolower($langData['lLangAbbv'])) ?>"></span>
                                                <label class="control-label" for="label"><?php echo htmlspecialchars($langData['lLangAbbv']) ?></label>
                                                <div class="controls">
                                                    <input type="text" id="label" disabled
                                                           value="<?php echo (isset($translations[$langData['lLangAbbv']])) ?
                                                               htmlentities($translations[$langData['lLangAbbv']]) : ''?>"
                                                           name="lang_<?php echo htmlspecialchars($langData['LID']) ?>" placeholder=""
                                                           maxlength="255" class="full_field form-control">
                                                </div>
                                            </div>

                                        <?php } ?>

                                    </fieldset>

                                <?php } else {?>
                                    No languages defined!
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>

        <?php } ?>

    </div>

</div>
