
<div class="right_col" role="main">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Settings</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-times"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form class="form-horizontal" action='' method="POST">
                        <input type="hidden" name="CSRF_TOKEN" value="<?php echo $token ?>" />
                        <fieldset>
                            <div class="control-group">
                                <!-- Label -->
                                <label class="control-label" for="mainLang">Main Language</label>
                                <div class="controls">
                                    <select title="Languages" name="mainLang" class="fields-combo-langs full_field form-control">
                                        <option>Choose Language</option>
                                        <?php if (isset($langs) && count($langs) > 0) { ?>
                                            <?php foreach ($langs as $langData) { ?>
                                                <option value="<?php echo $langData['LID'] ?>"
                                                    <?php echo (isset($settings['mainLangID']) &&
                                                        $langData['LID'] === $settings['mainLangID']) ? 'selected' : ''?>
                                                ><?php echo htmlentities($langData['lLangAbbv']) ?>
                                                </option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                    <p class="help-block">The main language is used as a basis for automatic translations from
                                    various apis.</p>
                                </div>
                            </div>

                            <div class="control-group">
                                <!-- APIS -->
                                <label class="control-label" for="transAPI">Translator API</label>
                                <div class="controls">
                                    <select title="Translator API" name="transAPI" class="full_field form-control">
                                        <option>Choose API</option>
                                        <?php if (isset($services) && count($services) > 0) { ?>
                                            <?php foreach ($services as $APIData) { ?>
                                                <option value="<?php echo $APIData['SID'] ?>"
                                                    <?php echo (isset($settings['chosenSID']) &&
                                                        $APIData['SID'] === $settings['chosenSID']) ? 'selected' : ''?>
                                                ><?php echo htmlentities($APIData['sName']) ?>
                                                </option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                    <p class="help-block">Use automatic translation API to ease your website translation
                                        process.</p>
                                </div>
                            </div>

                            <div class="control-group">
                                <!-- APIS -->
                                <label class="control-label" for="massTransType">Mass translation type</label>
                                <div class="controls">
                                    <select title="Mass translation type" name="massTransType" class="full_field form-control">
                                        <option>Choose Type</option>
                                        <option value="fillempty"
                                                <?php echo (isset($settings['transType']) && $settings['transType'] == 'fillempty') ? 'selected' : '' ?>
                                                title="Fill empty translates only those fields that are left untranslated"
                                        >Fill empty</option>
                                        <option value="overwrite"
                                                <?php echo (isset($settings['transType']) && $settings['transType'] == 'overwrite') ? 'selected' : '' ?>
                                                title="Overwrite the already filled fields with the language translation"
                                        >Overwrite</option>
                                    </select>
                                    <p class="help-block">Use automatic translation API to ease your website translation
                                        process.</p>
                                </div>
                            </div>

                            <br />

                            <div class="control-group">
                                <!-- Button -->
                                <div class="controls">
                                    <button class="btn btn-success">Save</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>

        <?php if ($bingIndex !== false) { ?>

            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Bing translator API Settings</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-times"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form class="form-horizontal" action='serviceUpdate.php' method="POST">
                            <fieldset>

                                <div class="control-group">
                                    <!-- APIS -->
                                    <label class="control-label" for="apikey">Api Key</label>
                                    <div class="controls">
                                        <input type="hidden" name="SID" value="<?php echo $services[$bingIndex]['SID'] ?>" />
                                        <input title="API Key" type="text" class="full_field form-control" name="api_key"
                                               value="<?php echo htmlentities($services[$bingIndex]['sAPIKey']) ?>">
                                        <p class="help-block">The API Key</p>
                                    </div>
                                </div>

                                <br />

                                <div class="control-group">
                                    <!-- Button -->
                                    <div class="controls">
                                        <button class="btn btn-success">Save</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>

        <?php } ?>

    </div>

</div>
